/** 
* @file
* @ingroup group_gcda
*/

#include "slave_socket.h"
#include "nscl.h"
#include "wordorder.h"
#include "Compat.h"

StatusManager GCDADupSlaveModuleControlBlock::sm;

in_addr_t GCDADupSlaveTransportReader::saddress_s;
int GCDADupSlaveTransportReader::sport_pool_start_s ;
in_addr_t GCDADupSlaveTransportReader::saddress_m;
int GCDADupSlaveTransportReader::sport_pool_start_m ;

StatusManager GCDADupSlaveMCBreader::sm;



GCDADupSlaveTransportReader::GCDADupSlaveTransportReader (int num){
    
    slave_port = sport_pool_start_s + num;
    
    //network init
    
    signal (SIGPIPE, SIG_IGN);  // make sure signal broken pipe ignored for this thread
    
    memset(&slave_ip4, 0, sizeof(slave_ip4));
    slave_ip4.sin_family = AF_INET;
    slave_ip4.sin_port   = htons ((unsigned short)slave_port);
    slave_ip4.sin_addr.s_addr = saddress_s;
    
    sm.InfoMessage(Compat::Form("(ADADup::NetLoad): Loading Network Reader, address: %s:%d", inet_ntoa (slave_ip4.sin_addr), slave_port)); 
    
    master_port = sport_pool_start_m + num;
    // store master ip address for auth proporses
    memset(&master_ip4, 0,sizeof(master_ip4));
    master_ip4.sin_family = AF_INET;
    master_ip4.sin_port   = htons ((unsigned short)master_port);
    master_ip4.sin_addr.s_addr = saddress_m;
    
    s = -1;
    ms = -1;
    socket_open = false;
    msocket_open = false;
    accepted = false;
    last_accepted_time = TimeStamp::current_time() ;
    last_readed_time = last_accepted_time;
    
    
}

int GCDADupSlaveTransportReader::reader_close (){
    if (msocket_open) {
        //(void) shutdown( ms, 2 ); //read_&_write shutdown
        sm.InfoMessage(Compat::Form("(ADADup::Run): Disconnected from master, address: %s:%d", inet_ntoa (master_ip4.sin_addr), master_port)); 
        (void) close (ms);
    }
    msocket_open = false;
    accepted = false;
    if (socket_open) {
        sm.InfoMessage(Compat::Form("(ADADup::Run): Shutdown Network Reader Listener, address: %s:%d", inet_ntoa (slave_ip4.sin_addr), slave_port)); 
        //(void) shutdown( s, 2 ); //read_&_write shutdown
        (void) close (s);
    }
    socket_open = false;
    return TN_SUCCESS;
}

int GCDADupSlaveTransportReader::reader_close_accept (){
    if (msocket_open) {
        sm.InfoMessage(Compat::Form("(ADADup::Run): Disconnected from master, address: %s:%d", inet_ntoa (master_ip4.sin_addr), master_port)); 
       (void) close (ms);
    }
    msocket_open = false;
    accepted = false;
    return TN_SUCCESS;
}

GCDADupSlaveTransportReader::~GCDADupSlaveTransportReader (){

    (void) reader_close ();
}


int GCDADupSlaveTransportReader::load (const string& sa,int sp, const string& ma,int mp){
    

struct hostent *hp;
string ip = sa;
if (isalpha (ip[0])) {
    hp = gethostbyname (ip.c_str ());// probe name server by name
    if (hp == NULL) {
        sm.ErrorMessage("(ADADup::NetLoad): Bad slave IP address.");
        return (TN_FAILURE);
    }
    memcpy ((char*)&saddress_s, hp->h_addr, hp->h_length);
}
else {
    saddress_s = inet_addr (ip.c_str ());// convert digital IP to binary format
}

if ( saddress_s == -1 ) {
    sm.ErrorMessage("(ADADup::NetLoad): Bad slave IP address.");
    return (TN_FAILURE);
}
sport_pool_start_s = sp;

string ip2 = ma;
if (isalpha (ip2[0])) {
    hp = gethostbyname (ip2.c_str ());// probe name server by name
    if (hp == NULL) {
        sm.ErrorMessage("(ADADup::NetLoad): Bad master IP address.");
        return (TN_FAILURE);
    }
    memcpy ((char*)&saddress_m, hp->h_addr, hp->h_length);
}
else {
    saddress_m = inet_addr (ip2.c_str ());// convert digital IP to binary format
}

if ( saddress_m == -1 ) {
    sm.ErrorMessage("(ADADup::NetLoad): Bad master IP address.");
    return (TN_FAILURE);
}
sport_pool_start_m = mp;

return TN_SUCCESS;

}


int set_nonblocking_mode (int s){
    int flags;
    if ((flags = fcntl (s, F_GETFL, 0)) < 0) { 
        return (TN_FAILURE);
    }
    if (fcntl (s, F_SETFL, flags | O_NONBLOCK) < 0){
        return (TN_FAILURE);
    }
    return (TN_SUCCESS) ;
}

int set_blocking_mode (int s){
    int flags;
    if ((flags = fcntl (s, F_GETFL, 0)) < 0) {
        return (TN_FAILURE);
    }
    if (fcntl (s, F_SETFL, flags & (~O_NONBLOCK)) < 0){
        return (TN_FAILURE);
    }
    return (TN_SUCCESS) ;
}

void GCDADupSlaveTransportReader::set_timeout_checktime (TimeStamp& time ){
    time = TimeStamp::current_time();
}

int GCDADupSlaveTransportReader::check_timeout (TimeStamp& time, double  interval ){
    // check if enough time left since last attempt to read
    TimeStamp curr_time = TimeStamp::current_time();
    double diff = (double) Duration(curr_time - time );
    double dperiod = (double) interval ;
    if (diff < dperiod){
        return(TN_FAILURE);
    }
    time = curr_time;
    
    return(TN_SUCCESS);
}



int  GCDADupSlaveTransportReader::reader_init () {
    
    int ret ;
    ret = socket ( AF_INET, SOCK_STREAM, 0 );
    if ( ret == -1 ) {
        return (TN_FAILURE);
    }
    // After that moment we already have socket opened
    // so we have to be sure in closing it
    s = ret ;
    socket_open = true;
    
    //clear previous errors
    int valopt =0 ;
    socklen_t optlen = sizeof(int);
    (void) getsockopt (s, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &optlen);
    
    //trying to bind while socket in TIME_WAIT state;  see rfc793
    int tries = NET_BIND_TRIES;
    bool binded = false;
    do {
        if ( ! no_signal ) { break; }
        valopt = 1;
        if ( setsockopt ( s, SOL_SOCKET, SO_REUSEADDR, (char *)&valopt, sizeof(int) ) == -1 ) {
                    sm.ErrorMessage("(ADADup::Run): Error making socket Reusable.");
        }
        if ( bind ( s, (struct sockaddr *) &slave_ip4, sizeof(slave_ip4)  ) == -1 )  {
            if (errno == EADDRINUSE ) {
                sm.InfoMessage(Compat::Form("(ADADup::Run): Binding to slave network interface, address: %s:%d", inet_ntoa (slave_ip4.sin_addr), slave_port)); 
                reader_wait (24, 0); // 2*MSL time/10 = 240/10=24 sec;
                sm.InfoMessage(Compat::Form("(ADADup::Run): Making socket re-usable,  address: %s:%d", inet_ntoa (slave_ip4.sin_addr), slave_port)); 
                valopt = 1;
                if ( setsockopt ( s, SOL_SOCKET, SO_REUSEADDR, (char *)&valopt, sizeof(int) ) == -1 ) {
                    sm.ErrorMessage("(ADADup::Run): Error making socket Reusable.");
                }
            } else {
                // other error
                binded = false;
                break;
            }
        }  else {
            binded = true; // no errors on bind
            break;
        }
    } while ( tries-- > 0);
    
    if ( ! binded ) {
        sm.ErrorMessage(Compat::Form("(ADADup::Run):Can not bind to slave network interface, address: %s:%d, errno: %d", inet_ntoa (slave_ip4.sin_addr), slave_port, errno)); 
        return (TN_FAILURE);
    }
    
    if ( listen ( s, 5 ) == -1 ) {
        sm.ErrorMessage(Compat::Form("(ADADup::Run):Can not listen to on slave network interface, address: %s:%d, errno: %d", inet_ntoa (slave_ip4.sin_addr), slave_port, errno)); 
        return (TN_FAILURE);
    }
    
    //make non-blocking operation for accept
    if ( set_nonblocking_mode (s) == TN_FAILURE  ) {
        return (TN_FAILURE);
    }
    sm.InfoMessage(Compat::Form("(ADADup::Run): Listen to slave network interface, address: %s:%d", inet_ntoa (slave_ip4.sin_addr), slave_port)); 
    set_timeout_checktime (last_accepted_time);
    
    return TN_SUCCESS;
    
}

int  GCDADupSlaveTransportReader::try_to_accept () {
    // Accept_ incoming connection
    int trusted = 0;                   /* 1 if client is trusted */
    int ret;
    socklen_t masterlen = sizeof(master_ip4);
    
    if ( ! socket_open ) {
        (void) reader_init (); // we know that we are able to bound  from init stage
    }
    ret = accept ( s, (struct sockaddr *)&master_ip4, &masterlen );
    if ( ret == -1 ) {
        return  (TN_FAILURE) ;
    }
    ms = ret;
    msocket_open = true;
    
    // Accept data only from trusted client
    if ( master_ip4.sin_addr.s_addr == saddress_m ){
        master_port = ntohs (master_ip4.sin_port);
        trusted = 1;
    }
    if ( ! trusted ) {
        sm.InfoMessage(Compat::Form("(ADADup::Run): Unauthorized attempt rejected , from address: %s", inet_ntoa (master_ip4.sin_addr))); 
        (void) reader_close_accept ();
        msocket_open = false;
        return TN_FAILURE;
    }
    
    if ( set_nonblocking_mode (ms) == TN_FAILURE  ) {
        return (TN_FAILURE);
    }
    
    accepted = true;
    set_timeout_checktime (last_readed_time);
    sm.InfoMessage(Compat::Form("(ADADup::Run): Accepted connection, from address: %s", inet_ntoa (master_ip4.sin_addr))); 
    
    return (TN_SUCCESS);
}



int  GCDADupSlaveTransportReader::try_to_read (){
    timeval tv;
    fd_set fdset;
    socklen_t optlen;
    int valopt;
    
    int res;
    
    // setup non-blocking select for read
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    
    FD_ZERO (&fdset);
    FD_SET (ms, &fdset);
    res = select (ms+1,  &fdset, NULL, NULL, &tv);
    if (res == -1){ //&& (errno != EINTR)) {
        return (TN_FAILURE);
        //throw Error("Error connecting in select ");
    }
    if (res == 0) {
        // select return  0 : time left and no any
        return (TN_NODATA);
    }
    if (res > 0) {
        if (FD_ISSET (ms, &fdset)) {
            // Socket selected for read
            optlen = sizeof(int);
            if (getsockopt (ms, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &optlen) < 0) {
                return (TN_FAILURE);
                //throw Error("Was Error connecting in select ");
            }
            switch ( valopt ) {
                case 0:
                    //return (TN_SUCCESS);
                    break;
#ifdef EHOSTUNREACH
                case EHOSTUNREACH:
#endif
#ifdef EHOSTDOWN
                case EHOSTDOWN:
#endif
#ifdef ETIMEDOUT
                case ETIMEDOUT:
#endif
                case ECONNREFUSED:
                    return (TN_NOTVALID);
#ifdef EALREADY
                case EALREADY:
#endif
#ifdef EAGAIN
                case EAGAIN:
#endif
                case EINPROGRESS:
                    return (TN_NODATA);
                default:
                    //throw Error ("Some Error return  while connecting in select ");
                    return (TN_FAILURE);
            }
            // looks like OK
            return (TN_SUCCESS);
        }
    }
    
    return (TN_FAILURE);
}


void GCDADupSlaveTransportReader::reader_wait (int sec, int msec ) {
    struct timespec sleeptime, slepttime;
    sleeptime.tv_sec = sec;
    sleeptime.tv_nsec = 1000000 * msec;  //1/1000 sec
    if ( LOG_VERBOSE_LEVEL > 3 ) {
        sm.InfoMessage(Compat::Form("(ADADup::Run): Reader sleeping [%lu:%lu]", sleeptime.tv_sec, sleeptime.tv_nsec ) ); 
    }
    nanosleep (&sleeptime, &slepttime);
    
}

int  GCDADupSlaveTransportReader::raw_read (int &bytes_left, char* &ptr ) {
    
    
    while (  bytes_left > 0 ) {
        if ( ! no_signal ) { break; }
        int status = try_to_read () ;
        switch  ( status ) {
            case TN_SUCCESS:
                int ret;
                ret = recv ( ms, ptr , bytes_left, 0 );
                if ( ret == 0 ) {
                    if ( LOG_VERBOSE_LEVEL > 0 ) {
                        sm.ErrorMessage(Compat::Form("(ADADup::Run):Socket error: last time received %d bytes.", ret));
                    }
                    return (TN_NODATA);
                }
                if (ret == -1) {
                    switch (errno) {
                        case EWOULDBLOCK :
                            //we should wait until network ready
                            reader_wait (0, NET_WOULDBLOCK_WAIT);
                        case EINTR :
                            break;
                        default :
                            if ( LOG_VERBOSE_LEVEL > 0 ) {
                                sm.ErrorMessage (Compat::Form ("(ADADup::Run):Socket error, errno: %d", errno ));
                            }
                            return (TN_FAILURE);
                    }
                } else {
                    ptr += ret;
                    bytes_left -= ret;
                }
                break;
            case  TN_FAILURE :
                return (TN_FAILURE);
            case TN_NODATA:
                if (check_timeout (last_readed_time, 2 * NET_HEARTBEAT_INTERVAL ) == TN_SUCCESS ) {
                    //no activity - we should disconnect
                    if ( LOG_VERBOSE_LEVEL > 2 ) {
                        sm.InfoMessage("(ADADup::Run): Network: Read interval expired (no heart beat) ");
                    }

                    return (TN_NODATA);
                }
                reader_wait (0, NET_WOULDBLOCK_WAIT);
                break;
            default :
                reader_wait (0, NET_WOULDBLOCK_WAIT);
        }
    }
    //if we here we read expected bytes
    return (TN_SUCCESS);
}


int  GCDADupSlaveTransportReader::read_data () {
    if ( ! accepted ) {
        if ( try_to_accept ()== TN_FAILURE) {
            if (check_timeout (last_accepted_time, 2 * NET_CONNECT_TIMEOUT  ) == TN_SUCCESS ) {
                //noone has connect  for long time
                (void) reader_close (); //full close ;
                reader_wait (NET_CONNECT_WAIT, 0);
                return (TN_FAILURE);
            }
            (void) reader_close_accept ();
            reader_wait (NET_CONNECT_WAIT, 0);
        }
#ifdef NETWORK_BOOST        
        if (accepted) 
            (void ) send_ack (); //helps to boost communication speed 
#endif
    }
#ifdef FLOATING_BUFFER_SIZE   
    if (accepted ){
        //create data area
        memset(mess.area, 0, NET_MAX_BUFFER_SIZE );
        int bytes_left = serialized_header_size;//sizeof(struct msg_header_struct);
        int rest_bytes_left = NET_BUFFER_SIZE * serialized_size ;
        char * ptr = (char*) &mess.area;
        char * rest_ptr = (char*) &mess.message.first_msg_char;
        
        int rest_status;
        int status = raw_read ( bytes_left, ptr );

        switch  ( status ) {
            case TN_NODATA:
            case TN_FAILURE:
                (void) reader_close_accept ();
                reader_wait (NET_CONNECT_WAIT, 0);
                return (TN_FAILURE);
                break;
            case TN_SUCCESS:
                //finally we have got a packet
                set_timeout_checktime (last_readed_time);
                last_accepted_time = last_readed_time;
                
                         #ifdef SEND_GET_ACK
                        if ( LOG_VERBOSE_LEVEL > 2 ) {
                            sm.InfoMessage("(ADADup::Run): Network: Sending ACK");
                        }
                        if ( send_ack() != TN_SUCCESS ) {
                            if ( LOG_VERBOSE_LEVEL > 2 ) {
                                sm.ErrorMessage("(ADADup::Run): Network: error sending ACK");
                            }
                        }
                        if ( LOG_VERBOSE_LEVEL > 2 ) {
                            sm.InfoMessage("(ADADup::Run): Network: ACK has been sent");
                        }
                        #endif               
                                 
                //1. validate info (correct packet ???)
                if (mess.message.header.reserved != 'C') {
		    sm.InfoMessage("(ADADup::read_data): res-char mismatch");
		    return (TN_FAILURE); // drop wrong packet
		} 
                //2. translate info (byte order)  and validaite
                wordorder_slave (mess.message.header.num);
                if ((mess.message.header.num < 0) || (mess.message.header.num > NET_BUFFER_SIZE )) {
		    sm.InfoMessage("(ADADup::read_data): header num mismatch");
                    return (TN_FAILURE);  // drop wrong packet
                } 
                wordorder_slave (mess.message.header.sample_size);
                if ((mess.message.header.sample_size != serialized_size )) {
		    sm.InfoMessage("(ADADup::read_data): size mismatch");
                    return (TN_FAILURE);  // drop wrong packet
                } 
                //3. read the rest of the packet

                rest_bytes_left= mess.message.header.num * mess.message.header.sample_size;
                rest_status = raw_read( rest_bytes_left, rest_ptr );
                
                switch  ( rest_status ) {
                    case TN_NODATA:
                    case TN_FAILURE:
                        (void) reader_close_accept();
                        reader_wait(NET_CONNECT_WAIT, 0);
                        return (TN_FAILURE);
                        break;
                    case TN_SUCCESS:
                        //finally we have got a packet
                        set_timeout_checktime(last_readed_time);
                        last_accepted_time = last_readed_time;
                        



                        return (TN_SUCCESS);
                        break;
                    default :
                        return (TN_FAILURE);
                }

                
                return (TN_SUCCESS);
                break;
            default : 
                return (TN_FAILURE);
        }
        return (TN_FAILURE);
    }
#else
    if (accepted ){
        //create data area
        memset(mess.area, 0, NET_MAX_BUFFER_SIZE );
        int bytes_left = network_packet_size ;
        char * ptr = (char*) &mess.area;
        
        int status = raw_read ( bytes_left, ptr );

        switch  ( status ) {
            case TN_NODATA:
            case TN_FAILURE:
                (void) reader_close_accept ();
                reader_wait (NET_CONNECT_WAIT, 0);
                return (TN_FAILURE);
            case TN_SUCCESS:
                //finally we have got a packet
                set_timeout_checktime (last_readed_time);
                last_accepted_time = last_readed_time;

                #ifdef SEND_GET_ACK
                if ( LOG_VERBOSE_LEVEL > 2 ) {
                    sm.InfoMessage("(ADADup::Run): Network: Sending ACK");
                }
                if ( send_ack() != TN_SUCCESS ) {
                    if ( LOG_VERBOSE_LEVEL > 2 ) {
                        sm.ErrorMessage("(ADADup::Run): Network: error sending ACK");
                    }
                    //(void)send_ack(); //no re-send nessesary
                }
                if ( LOG_VERBOSE_LEVEL > 2 ) {
                    sm.InfoMessage("(ADADup::Run): Network: ACK has been sent");
                }
                #endif

                
                //1. validate info (correct packet ???)
                if (mess.message.header.reserved != 'C') {
		    sm.InfoMessage("(ADADup::read_data): res-char mismatch");
		    return (TN_FAILURE); // drop wrong packet
		} 
                
                //2. translate info (byte order) and validaite 
                wordorder_slave (mess.message.header.num);
                if ((mess.message.header.num < 0) || (mess.message.header.num > NET_BUFFER_SIZE )) {
		    sm.InfoMessage("(ADADup::read_data): header num mismatch");
                    return (TN_FAILURE);
                } // drop wrong packet
                wordorder_slave (mess.message.header.sample_size);
                if ((mess.message.header.sample_size != serialized_size )) {
		    sm.InfoMessage("(ADADup::read_data): size mismatch");
                    return (TN_FAILURE);  // drop wrong packet
                } 


                return (TN_SUCCESS);
            default :
                return (TN_FAILURE);
        }
        return (TN_FAILURE);
    }
#endif    
    return (TN_NODATA);
    
}

int  GCDADupSlaveTransportReader::read_heartbeat () {
    if ( mess.message.header.num == 0 ) {
        if ( LOG_VERBOSE_LEVEL > 1 ) {
            sm.InfoMessage("(ADADup::Run): HeartBeat message received.");
        }
        return (TN_SUCCESS);
    }
    return (TN_FAILURE);
}

int  GCDADupSlaveTransportReader::send_ack ()  {
    timeval tv;
    fd_set fdset;
    socklen_t optlen;
    int valopt;
    int res;
    
    char msg[4]= {'A', 'C', 'K', 0}, *ptr = msg;
    int bytes_left = 4;
    while (  bytes_left > 0 ) {
        if ( ! no_signal ) { break; }
        
        // setup non-blocking select for write
        tv.tv_sec = 0;
        tv.tv_usec = 0;
        
        FD_ZERO (&fdset);
        FD_SET (ms, &fdset);
        res = select (ms+1, NULL, &fdset, NULL, &tv);
        if (res == -1) {//&& (errno != EINTR)) {
            return (TN_FAILURE);
        }
        if (res == 0) {
            return (TN_FAILURE);
        }
        if (res > 0) {
            if (FD_ISSET (ms, &fdset)) {
                // Socket selected for read
                optlen = sizeof(int);
                if (getsockopt (ms, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &optlen) < 0) {
                    return (TN_FAILURE);
                }
                // Check the value returned...
                if (valopt) {
                    return (TN_FAILURE);
                }
                
                int ret;
                ret = send ( ms, ptr , bytes_left, 0 );
                if (ret == 0 ) {
                    if ( LOG_VERBOSE_LEVEL > 0 ) {
                        sm.ErrorMessage(Compat::Form("(ADADup::Run):Socket error: ACK: last time sent %d bytes.", ret));
                    }
                    return (TN_FAILURE);
                } 
                if (ret == -1) {
                    switch (errno) {
                        case EWOULDBLOCK :
                            reader_wait (0, NET_WOULDBLOCK_WAIT);
                            continue;
                        default :
                            if ( LOG_VERBOSE_LEVEL > 0 ) {
                                sm.ErrorMessage(Compat::Form("(ADADup::Run):Socket error sending ACK, errno: %d", errno ));
                            }
                            return (TN_FAILURE);
                    }
                } else {
                    ptr += ret;
                    bytes_left -= ret;
                }
            }
        }
    }
    //ok no more errors, all above
    return (TN_SUCCESS);
    
}






GCDADupSlaveModuleControlBlock::GCDADupSlaveModuleControlBlock (Channel chn){
    channel = chn;
    strcpy(tmp_loc, chn.location);
    mapLC((char*)chn.network, tmp_loc, ASCII);

    reader = NULL;
    
    writer = NULL;
    TimeStamp zero = TimeStamp::current_time();
    nexttime = zero;
    start_timestamp= zero;
    nsamples_extracted = 0;
    nsamples_processed = 0;
    
    write_size = GCDA_SLAVE_OUTSIZE ;
    memset(data_buff, 0, GCDA_SLAVE_OUTSIZE *sizeof(GCDA_data_type));
    
    
}

GCDADupSlaveModuleControlBlock::~GCDADupSlaveModuleControlBlock (){
    if(writer)
        delete writer;
    
}


/*
void GCDADupSlaveModuleControlBlock::write_timeseries ( sock_msg_packet &mess ){
    
    int i, n;
    struct timeval tm, tn;
    int num = mess.message.header.num  ;
    char* buff_ptr = &mess.message.first_msg[0];
    struct msg_struct *msg_ptr= reinterpret_cast<msg_struct*> (buff_ptr);
    
    for ( i = 0; i< num; i++ ) {
        writer->Put_Sample (msg_ptr[i].time, msg_ptr[i].data);

    }

}
*/
void  GCDADupSlaveModuleControlBlock::pack_data ( msg_struct &data , int n ){
    if ( n < write_size) {
        time_buff[n] = TimeStamp(LEAPSECOND_TIME, data.time);
        data_buff[n] = data.data;
    }
}

int  GCDADupSlaveModuleControlBlock::write_data (int  num ){
    int ret = TN_SUCCESS ;
    for (int i = 0; i< num ; i++  ) {
        if (  writer->Put_Sample (time_buff[i], data_buff[i] ) == TN_FAILURE )
            ret = TN_FAILURE;
    }     
    return (ret);
    
}


int  GCDADupSlaveModuleControlBlock::write_by_packet (int  num ){
    int ret = TN_SUCCESS ;
    //check if no gaps  
    int i;
    int start = 0;

    double interval_sec = double(usec_per_sample )/double( USECS_PER_SECOND);
    Duration sample_interval (interval_sec);
    
    for (i = 1; i< num ; i++  ) {
        TimeStamp t1(time_buff[i-1]);
        TimeStamp t2(time_buff[i]);
        Duration d = t1-t2;
        if ( (TimeStamp(time_buff[i-1]) - TimeStamp(time_buff[i])) > sample_interval ) {
             if (  writer->Put_Samples (time_buff[start], i - start , &data_buff[start] ) == TN_FAILURE )  ret = TN_FAILURE;
             start = i;
        }
    }  
    //write packet
    if (  writer->Put_Samples (time_buff[start], num - start , &data_buff[start] ) == TN_FAILURE )  ret = TN_FAILURE;

    return (ret);
}


