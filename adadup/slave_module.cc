/** 
* @file
* @ingroup group_gcda
*/

#include <iostream>
#include <ctime>
#include <cmath>
#ifdef __SUNPRO_CC
#include <math.h>
#endif
#include <map>
#include "slave_module.h"
#include "Duration.h"
#include "Compat.h"

using namespace std;
int no_signal = 1;

extern "C" {
    static void (*sig_wrapper_old)(int);
    void sig_handler_wrapper(int i) {
        if (no_signal) {
            printf("Received Signal %d\n", i);
            no_signal = 0;
            if (sig_wrapper_old) sig_wrapper_old (i); 
        }
    }
}


char GCDADupSlaveTSPModuleManager::master_mashine_ip_address[MAXSTR];
int  GCDADupSlaveTSPModuleManager::master_mashine_ip_port;
char GCDADupSlaveTSPModuleManager::slave_mashine_ip_address[MAXSTR];
int  GCDADupSlaveTSPModuleManager::slave_mashine_ip_port;
bool GCDADupSlaveTSPModuleManager::all_channels_dup;


GCDADupSlaveTSPModuleManager::GCDADupSlaveTSPModuleManager ():Application(){
    outda = NULL;
    network_reader = NULL;
}

void GCDADupSlaveTSPModuleManager::cleanup (){
    
    std::cout << "Cleaning up."<<std::endl;
    if(outda) 
    {
        delete outda;
	outda = NULL;
    }
    
    for(GCDADupSlaveModuleControlBlockList::iterator mit = modlist.begin ();mit!=modlist.end ();mit++){
        GCDADupSlaveModuleControlBlock* mcb = (*mit);
        delete mcb;
    }
    modlist.clear ();
    modmap.clear();
   
   if ( network_reader ) {
        delete network_reader;
	network_reader = NULL;
    }
    
   if (lock && lock->isValid()) {
        lock->unlock();
    }
}


void GCDADupSlaveTSPModuleManager::stop (){

    cleanup ();
}

int GCDADupSlaveTSPModuleManager::ParseConfiguration(const char *tag, const char *value)
{
    try {
        if (strcasecmp (tag, "USE") == 0) {
            if (strlen (value) > 10) {
                std::cout << "Error ADADup:ParseConfiguration(): Use field too long files or database"<<std::endl;
                return(TN_FAILURE);
            } else if (strcasecmp (value, "DATABASE")==0) {
                use_db=true;
            } else if (strcasecmp (value, "FILES")==0) {
                use_db=false;
            } else {
                std::cout << "Error ADADup:ParseConfiguration(): Use field error:  Only options are files or database"<<std::endl;
                return(TN_FAILURE);
            }
        } else if (strcasecmp (tag, "DBPROGNAME") == 0) {
            if (strlen (value) > MAXSTR) {
                std::cout << "Error ADADup:ParseConfiguration(): dbprogname field too long"<<std::endl;
                return(TN_FAILURE);
            }
            strcpy (dbprogname, value);
        } else if (strcasecmp (tag, "ADAKEYNAME") == 0) {
            if (strlen (value) > GCDA_KEY_NAME_LEN) {
                std::cout << "Error ADADup:ParseConfiguration(): adakeyname field too long"<<std::endl;
                return(TN_FAILURE);
            }
            strcpy (gcda_key_name, value);
        } else if (strcasecmp (tag, "LOCKFILE") == 0) {
            if (strlen (value) > MAXSTR) {
                std::cout << "Error ADADup:ParseConfiguration(): lockfile field too long"<<std::endl;
                return(TN_FAILURE);
            }
            strcpy (lock_file_name, value);
        } else if (strcasecmp (tag, "ADAFILE") == 0) {
            if (strlen (value) > MAXSTR) {
                std::cout << "Error ADADup:ParseConfiguration(): adafile field too long"<<std::endl;
                return(TN_FAILURE);
            }
            //strcpy (gcdafile, value);
        } else if (strcasecmp (tag, "CHANNELSFILE") == 0) {
            if (strlen (value) > MAXSTR) {
                std::cout << "Error ADADup:ParseConfiguration(): channelsfile field too long"<<std::endl;
                return(TN_FAILURE);
            }
            //strcpy (channelsfile, value);
        } else if (strcasecmp (tag, "DBSERVICE") == 0) {
            if (strlen (value) > MAXSTR) {
                std::cout << "Error ADADup:ParseConfiguration(): DBservice field too long"<<std::endl;
                return(TN_FAILURE);
            }
            strcpy (dbservice, value);
        } else if (strcasecmp (tag, "DBUSER") == 0) {
            if (strlen (value) > MAXSTR) {
                std::cout << "Error ADADup:ParseConfiguration(): DBuser field too long"<<std::endl;
                return(TN_FAILURE);
            }
            strcpy (dbuser, value);
        } else if (strcasecmp (tag, "DBPASSWD") == 0) {
            if (strlen (value) > MAXSTR) {
                std::cout << "Error ADADup:ParseConfiguration(): DBpasswd field too long"<<std::endl;
                return(TN_FAILURE);
            }
            strcpy (dbpass, value);
        } else if (strcasecmp (tag, "MASTER_IP_ADDRESS") == 0) {
            if (strlen (value) > MAXSTR) {
                std::cout << "Error ADADup:ParseConfiguration(): Master_IP_Address field too long"<<std::endl;
                return(TN_FAILURE);
            }
            strcpy (master_mashine_ip_address, value);
        } else if (strcasecmp (tag, "MASTER_IP_PORT") == 0) {
            master_mashine_ip_port=atoi (value);
        } else if (strcasecmp (tag, "SLAVE_IP_ADDRESS") == 0) {
            if (strlen (value) > MAXSTR) {
                std::cout << "Error ADADup:ParseConfiguration(): Slave_IP_Address field too long"<<std::endl;
                return(TN_FAILURE);
            }
            strcpy (slave_mashine_ip_address, value);
        } else if (strcasecmp (tag, "Slave_IP_PORT") == 0) {
            slave_mashine_ip_port=atoi (value);
        } else if (strcasecmp (tag, "DBMAXCONNECTIONRETRIES") == 0) {
            dbretries=atoi (value);
        } else if (strcasecmp (tag, "DBCONNECTIONRETRYINTERVAL") == 0) {
            dbinterval=atoi (value);
        } else {
            std::cout << "Error (ADADup::ParseConfiguration): Unrecognized config line" << std::endl;
            return(TN_FAILURE);
        }
    }
    catch (...) {
        std::cout << "Error (ADADup::ParseConfiguration): Unknown type parsing error." << std::endl;
        return(TN_FAILURE);
    }
    return(TN_SUCCESS);
}     


unsigned int usec_from_rate(float rate ) {
    unsigned int ret ;
    ret =  (unsigned int) lround((1.0/rate) * USECS_PER_SECOND);
return ret;
}



int GCDADupSlaveTSPModuleManager::init_by_db() {
    key_t outda_key;
    
    char tmp_loc[3];
    ChannelConfigList configchans;
    ChannelConfigList::iterator clp;
    ChannelConfigList::iterator clpnext;
    sm.InfoMessage("(ADADup::Startup): configuring using database");
    
    if (strcmp(gcda_key_name, "") ==0) {
        sm.ErrorMessage("(ADADup::InitByDB): gcda_key_name not given");
        return(TN_FAILURE);
    }
    
    // first get the channels configured for this progname
    ChannelReaderDB cdb(dbservice, dbuser, dbpass);
    if (!cdb) {
        sm.ErrorMessage("ADADup::InitByDB() failed to connect to database");
        return TN_FAILURE;
    }
    cdb.SetRetryInterval(dbinterval);
    cdb.SetNumRetries(dbretries);
    
    sm.InfoMessage(Compat::Form("Loading list of configured channels from database for program: %s", dbprogname));
    
    // Get the configured channels from the database
    if (cdb.GetChannels(dbprogname, configchans) != TN_SUCCESS) {
        sm.ErrorMessage("(ADADup::InitByDB): Unable to retrieve list of configured channels");
        return(TN_FAILURE);
    }
    
    if (configchans.size() == 0) {
        sm.ErrorMessage("(ADADup::InitByDB): No configured channels found in database");
        return(TN_FAILURE);
    } else {
        sm.InfoMessage(Compat::Form("Retrieved %d channels from database",
        configchans.size()));
    }
    
    // Initialize the GCDA
    outda = new Generic_CDA<GCDA_data_type>(std::string(gcda_key_name), 0);
    if (outda == NULL) {
        sm.ErrorMessage("(ADADup::InitByDB): Unable to allocate ADA");
        return(TN_FAILURE);
    }
    
    outda_key = outda->Get_Memory_Key();
    clp = configchans.begin();
    while (clp != configchans.end()) {
        
        strcpy(tmp_loc, (*clp).first.location);
        mapLC((char*)(*clp).first.network, tmp_loc, ASCII);
        Channel chnl = (*clp).first;
        
        GCDADupSlaveModuleControlBlock* cb =
        (GCDADupSlaveModuleControlBlock*) new GCDADupSlaveModuleControlBlock(chnl);
        
        if( cb == NULL ){
            sm.ErrorMessage(Compat::Form("(ADADup::InitByDB): %s %s.%s.%s.%s",
            (char*) "No memory to create an internal module for channel",
            (char*) (*clp).first.network, (char*) (*clp).first.station, (char*)(*clp).first.channel, tmp_loc));
            return(TN_FAILURE);
        }
        
        
        cb->writer = new Data_Channel_Writer<GCDA_data_type>(outda_key);
        if( cb->writer == NULL){
            delete cb;
            sm.ErrorMessage(Compat::Form("(ADADup::InitByDB): %s %s.%s.%s.%s",
            (char*) "No memory to create a <Data_Channel_Writer> for channel",
            (char*) (*clp).first.network, (char*) (*clp).first.station, (char*)(*clp).first.channel, tmp_loc));
            return(TN_FAILURE);
            
        }
        
        if( cb->writer->Initialize_Channel(outda->Start_of_Memory_Area(),
        (char*)chnl.network, (char*)chnl.station, (char*)chnl.channel, (char*)chnl.location) == TN_FAILURE){
            delete cb;
            sm.InfoMessage(Compat::Form("(ADADup::InitByDB): %s %s.%s.%s.%s",
            (char*) "WARNING: Unable to initialize gcda for channel",
            (char*) (*clp).first.network, (char*) (*clp).first.station, (char*)(*clp).first.channel, tmp_loc));
            clp++;
            continue;
        }
        
        cb->usec_per_sample = usec_from_rate(cb->writer->Rate_in_Samples_per_Second());
        
        modlist.push_back(cb);
        
        sm.InfoMessage(Compat::Form("(ADADup::InitByDB): Initialized internal module for %s.%s.%s.%s",
        (*clp).first.network, (*clp).first.station, (*clp).first.channel, tmp_loc ));
        
        clp++;
    } // end of while loop
    
    sm.InfoMessage(Compat::Form("(ADADup::InitByDB): Initialized %d GCDA writers.", modlist.size()));
    
    if( modlist.size() < 1 ) {
        sm.ErrorMessage("(ADADup::InitByDB): Fatal Error: No GCDA writers created.");
        return(TN_FAILURE);
    }
    
    return TN_SUCCESS;
}




int GCDADupSlaveTSPModuleManager::Startup() 
{
    int res;
    nscl nscl_key;
    //sm.LogConsole(replevel);
    
    sm.InfoMessage("(ADADup::Startup): Starting...");
    // attempt to lock the file
    lock = new LockFile(lock_file_name);
    if (lock && lock->isValid()) {
        if(lock->lock() == -1) {
	    sm.ErrorMessage("(ADADup::Startup): Unable to lock LockFile, another instance of ADADup is running");
	    return(TN_FAILURE);
        }
    } else {
	sm.ErrorMessage("(ADADup::Startup): Unable to open/create LockFile");
	return(TN_FAILURE);
    }
    
    GCDADupSlaveModuleControlBlock::load(sm);
        
    res = init_by_db();
    if (res == TN_FAILURE) {
        sm.ErrorMessage("(ADADup::Startup): Unable to initialize internal module.");
        cleanup();
        return(TN_FAILURE);
    }
    
    //make a map here 
    
    for (GCDADupSlaveModuleControlBlockList::iterator mit = modlist.begin ();mit!=modlist.end ();mit++){
        GCDADupSlaveModuleControlBlock* mcb = (*mit);
        memset(&nscl_key, 0, sizeof(nscl));
        (void)strncpy (nscl_key.network, mcb->channel.network, MAX_CHARS_IN_NETWORK_STRING);
        (void)strncpy (nscl_key.station, mcb->channel.station, MAX_CHARS_IN_STATION_STRING);
        (void)strncpy (nscl_key.channel, mcb->channel.channel, MAX_CHARS_IN_CHANNEL_STRING);
        (void)strncpy (nscl_key.location, mcb->channel.location, MAX_CHARS_IN_LOCATION_STRING);
        modmap.insert (map<nscl, GCDADupSlaveModuleControlBlock*, ltnscl>::value_type (nscl_key, mcb));
    }
    
  
    
    
    GCDADupSlaveMCBreader :: load(sm);
    res = GCDADupSlaveTransportReader::load (std::string(slave_mashine_ip_address), slave_mashine_ip_port,
					     std::string(master_mashine_ip_address), master_mashine_ip_port);
    if (res == TN_FAILURE) {
        sm.ErrorMessage("(ADADup::Startup): Unable to initialize network module.");
        cleanup();
        return(TN_FAILURE);
    }

    network_reader = (GCDADupSlaveMCBreader*) new GCDADupSlaveTransportReader ( 0 );
    if( network_reader == NULL ){
        sm.ErrorMessage("(ADADup::Startup): No memory to create network reader.");
        cleanup();
        return(TN_FAILURE);
    }
    
    sig_wrapper_old = 
    signal (SIGHUP, sig_handler_wrapper);
    signal (SIGINT, sig_handler_wrapper);
    signal (SIGQUIT, sig_handler_wrapper);
    //    signal (SIGILL, sig_handler_wrapper);
    //    signal (SIGTRAP, sig_handler_wrapper);
    //    signal (SIGFPE, sig_handler_wrapper);
    //    signal (SIGKILL, sig_handler_wrapper);
    //    signal (SIGBUS, sig_handler_wrapper);
    //    signal (SIGSEGV, sig_handler_wrapper);
    //    signal (SIGSYS, sig_handler_wrapper);
    signal (SIGTERM, sig_handler_wrapper);
    signal (SIGPIPE, SIG_IGN);
    TimeStamp now = TimeStamp::current_time();
    SetNextWakeup (now);
    
    
    return TN_SUCCESS;
}


int GCDADupSlaveTSPModuleManager::Run() {
    
    sm.InfoMessage("(ADADup::Run): Slave Running...");
    
    map<nscl, GCDADupSlaveModuleControlBlock*, ltnscl>::iterator it;
    GCDADupSlaveMCBreader *rcb = network_reader;
    
    char tmp_loc[3];
    
    
    struct timespec sleeptime, slepttime;
    struct timeval temptime;
    TimeStamp loop_start_time, loop_end_time;
    double diff, loop_period = (double) GCDA_WRITE_INTIME_PERIOD;
    
    bool was_activity = false;
    bool found = false ;
    int status;
    
    while(no_signal){
        was_activity = false;
        
        loop_start_time = TimeStamp::current_time();
        if ( LOG_VERBOSE_LEVEL > 2 ) {
            sm.InfoMessage("(ADADup::Run): Waiting for network data");
        }
        //thread-level  read
        status = rcb->read_data();

        switch ( status ) {
            case TN_FAILURE:
                was_activity = true;
                break;
            case  TN_SUCCESS :
                was_activity = true;
                //check for heartbeat packet;
                if ( rcb->read_heartbeat() != TN_SUCCESS ) {
                    
                    char* buff_ptr = &rcb->mess.message.first_msg_char;
                    struct nscl chan;
                    int num = rcb->mess.message.header.num;
                    uint32_t sample_siz = rcb->mess.message.header.sample_size;
                    size_t ii;
                    
                    int i   = 0;
                    while ( i < num ) {
                        if ( ! no_signal ) { break; }
                        
                        //deserialization from network
                        struct msg_struct m;
                        bool serialized_ok = false;
                        do {
                            if ( ! no_signal ) { break; }
                            if ( ! (i < num) ) { break; }
                            
                            m.deserialize(ii, buff_ptr + sample_siz * i);
                            serialized_ok = true;
                            if (ii!=sample_siz) {
                                if ( LOG_VERBOSE_LEVEL > 0 ) {
                                    strcpy(tmp_loc, (char*)m.channel.location);
                                    (void)mapLC((char*)m.channel.network, tmp_loc, ASCII);
                                    sm.ErrorMessage(Compat::Form("(ADADup::Run): Could not write %d samples to channel %s.%s.%s.%s,  Wrong Sample Size.", 1,
                                            (char*)m.channel.network, (char*)m.channel.station, (char*)m.channel.channel, tmp_loc ));
                                }
                                serialized_ok = false;
                            }
                            wordorder_slave(m.time);
                            TimeStamp test_time(LEAPSECOND_TIME, m.time);
                            if (! test_time ) {
                                if ( LOG_VERBOSE_LEVEL > 0 ) {
                                    strcpy(tmp_loc, (char*)m.channel.location);
                                    (void)mapLC((char*)m.channel.network, tmp_loc, ASCII);
                                    sm.InfoMessage(Compat::Form("(ADADup::Run): Could not write %d samples to channel %s.%s.%s.%s, Wrong time: %e ", 1,
                                            (char*)m.channel.network, (char*)m.channel.station, (char*)m.channel.channel, tmp_loc , m.time));
                                }
                                serialized_ok = false;
                            }
                            if ( ! serialized_ok ) i++;
                        }  while ( ! serialized_ok  );
                        
                        if (serialized_ok) {
                            m.data.wordorder_slave();
                            
                            (void) mapLC(&m.channel, MEMORY);
                            chan = m.channel;
                            it = modmap.find(m.channel);
                            if ( it != modmap.end()) {
                                bool same_channel;
                                GCDADupSlaveModuleControlBlock* mcb = (*it).second;
                                int j = 0;
                                
                                do {
                                    if ( ! no_signal ) { break; }
                                    
                                    mcb->pack_data(  m, j );
				    
                                    i++ ; j++;
                                    same_channel  = false ;
                                    
                                    if ( i < num ) {
                                        //deserialize && check constrains
                                        serialized_ok = false;
                                        do {
                                            if ( ! no_signal ) { break; }
                                            if ( ! (i < num) ) { break; }
                                            
                                            m.deserialize(ii, buff_ptr + sample_siz * i);
                                            serialized_ok = true;
                                            if (ii!=sample_siz) {
                                                if ( LOG_VERBOSE_LEVEL > 0 ) {
                                                    strcpy(tmp_loc, (char*)m.channel.location);
                                                    (void)mapLC((char*)m.channel.network, tmp_loc, ASCII);
                                                    sm.ErrorMessage(Compat::Form("(ADADup::Run): Could not write %d samples to channel %s.%s.%s.%s,  Wrong Sample Size.", 1,
                                                            (char*)m.channel.network, (char*)m.channel.station, (char*)m.channel.channel, tmp_loc ));
                                                }
                                                serialized_ok = false;
                                            }
                                            wordorder_slave(m.time);
                                            TimeStamp test_time = TimeStamp(LEAPSECOND_TIME, m.time);
                                            if (! test_time ) {
                                                if ( LOG_VERBOSE_LEVEL > 0 ) {
                                                    strcpy(tmp_loc, (char*)m.channel.location);
                                                    (void)mapLC((char*)m.channel.network, tmp_loc, ASCII);
                                                    sm.InfoMessage(Compat::Form("(ADADup::Run): Could not write %d samples to channel %s.%s.%s.%s, Wrong time: %e ", 1,
                                                            (char*)m.channel.network, (char*)m.channel.station, (char*)m.channel.channel, tmp_loc , m.time));
                                                }
                                                serialized_ok = false;
                                            }
                                            if ( ! serialized_ok ) i++;
                                            
                                        }  while ( ! serialized_ok );
                                        
                                        if (serialized_ok) {
                                            m.data.wordorder_slave();
                                            (void) mapLC(&m.channel, MEMORY);
                                            same_channel =
                                                    (strcmp(m.channel.network, chan.network) == 0) &&
                                                    (strcmp(m.channel.station, chan.station) == 0) &&
                                                    (strcmp(m.channel.channel, chan.channel) == 0) &&
                                                    (strcmp(m.channel.location, chan.location) == 0) ;
                                        }
                                    } // if i < num
                                    
                                } while ( same_channel );
                                
                                int ret = mcb->write_by_packet( j );
                                if ( LOG_VERBOSE_LEVEL > 1 ) {
                                    TimeStamp ts( mcb->time_buff[0]);
                                    TimeStamp te( mcb->time_buff[(j<mcb->write_size)?j-1:mcb->write_size-1]);
                                    
                                    sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, StartTime: %04d/%02d/%02d %02d:%02d:%02d.%03d, EndTime: %04d/%02d/%02d %02d:%02d:%02d.%03d, Samples: %d, GCDA write: %s",
                                            (char*)mcb->channel.network, (char*)mcb->channel.station, (char*)mcb->channel.channel, (char*)mcb->tmp_loc,
                                            ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
                                            te.year(), te.month_of_year(), te.day_of_month(), te.hour_of_day(), te.minute_of_hour(), te.second_of_minute(), te.milliseconds(),
                                            j, (ret == TN_SUCCESS)?(char*)"OK":(char*)"FAIL" ));
                                }
                                
                            } else { //channel not found
                                if ( LOG_VERBOSE_LEVEL > 0 ) {
                                    TimeStamp ts(LEAPSECOND_TIME, m.time);
                                    strcpy(tmp_loc, (char*)m.channel.location);
                                    (void)mapLC((char*)m.channel.network, tmp_loc, ASCII);
                                    sm.InfoMessage(Compat::Form("(ADADup::Run): Could not write %d samples to channel %s.%s.%s.%s, Mapping Error.", 1,
                                            (char*)m.channel.network, (char*)m.channel.station, (char*)m.channel.channel, tmp_loc ));
                                }
                                i++ ;
                            } //channel found
                        } // serialized_ok
                    } //while i < num
                }//if !heartbeat
                break;
            default: 
                ;
        }
        //let us give CPU busyless time back
        if ( ! was_activity ) {
            loop_end_time = TimeStamp::current_time();
            diff = (double) Duration(loop_end_time - loop_start_time);
            if (diff < loop_period){
                temptime = Duration( loop_period - diff ).tv();
                sleeptime.tv_sec = temptime.tv_sec;
                sleeptime.tv_nsec = temptime.tv_usec * 1000;
                if  (nanosleep(&sleeptime, &slepttime) == -1 ) {
                    sm.ErrorMessage(Compat::Form("(ADADup::Run):  Nanosleep fails, errno: %d ", errno ));
                }
            }
        }
        
    } //while(no_signal)
    sm.InfoMessage("(ADADup::Run): Slave exited");
    
    return(TN_SUCCESS);
}

