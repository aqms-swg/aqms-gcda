/** 
* @file
* @ingroup group_gcda
*/

#include <cmath>
#ifdef __SUNPRO_CC
#include <math.h>
#endif
#include <ctime>
#include "master_module.h"
#include "Duration.h"
#include "Compat.h"

using namespace std;
int no_signal = 1;
int MAX_SECONDS_BEHIND_REALTIME = GCDA_MAX_SECONDS_BEHIND_REALTIME ;


extern "C" {
    static void (*sig_wrapper_old)(int) ;
    void sig_handler_wrapper(int i) {
        if (no_signal) {
            printf("Received Signal %d\n", i);
            no_signal = 0;
            if (sig_wrapper_old) sig_wrapper_old(SIGINT);
        }
    }
}


char GCDADupMasterTSPModuleManager::slave_mashine_ip_address[MAXSTR];
int  GCDADupMasterTSPModuleManager::slave_mashine_ip_port;
int  GCDADupMasterTSPModuleManager::start_times_mode;
char GCDADupMasterTSPModuleManager::start_times_dir[PATH_MAX];
char GCDADupMasterTSPModuleManager::start_times_file[PATH_MAX];
char GCDADupMasterTSPModuleManager::dbprogname[MAXSTR];
char GCDADupMasterTSPModuleManager::dbservice[MAXSTR];
char GCDADupMasterTSPModuleManager::dbuser[MAXSTR];
char GCDADupMasterTSPModuleManager::dbpass[MAXSTR];
int  GCDADupMasterTSPModuleManager::dbinterval;
int  GCDADupMasterTSPModuleManager::dbretries;
bool GCDADupMasterTSPModuleManager::use_db;
char GCDADupMasterTSPModuleManager::lock_file_name[MAXSTR];
LockFile* GCDADupMasterTSPModuleManager::lock;
int GCDADupMasterTSPModuleManager::gcda_read_mode = 0;


GCDADupMasterTSPModuleManager::GCDADupMasterTSPModuleManager ():Application(){
    inda = NULL;
    network_writer = NULL;
    strcpy (start_times_dir, (char*)DEFAULT_STARTPOSITION_DIR);
    strcpy (start_times_file, (char*)DEFAULT_STARTPOSITION_FILE);
}

void GCDADupMasterTSPModuleManager::cleanup (){
    
    cout << "Cleaning up."<<endl;
    if ( network_writer ) {
        delete network_writer;
    }
    
    for(GCDADupMasterModuleControlBlockList::iterator mit = modlist.begin ();mit!=modlist.end ();mit++){
        GCDADupMasterModuleControlBlock* mcb = (*mit);
        delete mcb;
    }
    modlist.clear ();
    
    if(inda) {
        delete inda;
    }
    
    if (lock && lock->isValid()) {
        lock->unlock();
    }
 
}

void GCDADupMasterTSPModuleManager::stop (){
    //Unload modules//
    
    cleanup ();
}

int GCDADupMasterTSPModuleManager::ParseConfiguration(const char *tag, const char *value)
{
    try {
        if (strcasecmp (tag, "USE") == 0) {
            if (strlen (value) > 10) {
                cout << "Error ADADup:ParseConfiguration(): Use field too long files or database"<<endl;
                return(TN_FAILURE);
            } else if (strcasecmp (value, "DATABASE")==0) {
                use_db=true;
            } else if (strcasecmp (value, "FILES")==0) {
                use_db=false;
            } else {
                cout << "Error ADADup:ParseConfiguration(): Use field error:  Only options are files or database"<<endl;
                return(TN_FAILURE);
            }
        } else if (strcasecmp (tag, "DBPROGNAME") == 0) {
            if (strlen (value) > MAXSTR) {
                cout << "Error ADADup:ParseConfiguration(): dbprogname field too long"<<endl;
                return(TN_FAILURE);
            }
            strcpy (dbprogname, value);
        } else if (strcasecmp (tag, "ADAKEYNAME") == 0) {
            if (strlen (value) > GCDA_KEY_NAME_LEN) {
                cout << "Error ADADup:ParseConfiguration(): adakeyname field too long"<<endl;
                return(TN_FAILURE);
            }
            strcpy (gcda_key_name, value);
        } else if (strcasecmp (tag, "LOCKFILE") == 0) {
            if (strlen (value) > MAXSTR) {
                cout << "Error ADADup:ParseConfiguration(): lockfile field too long"<<endl;
                return(TN_FAILURE);
            }
            strcpy (lock_file_name, value);
        } else if (strcasecmp (tag, "ADAFILE") == 0) {
            if (strlen (value) > MAXSTR) {
                cout << "Error ADADup:ParseConfiguration(): adafile field too long"<<endl;
                return(TN_FAILURE);
            }
            //strcpy (gcdafile, value);
        } else if (strcasecmp (tag, "CHANNELSFILE") == 0) {
            if (strlen (value) > MAXSTR) {
                cout << "Error ADADup:ParseConfiguration(): channelsfile field too long"<<endl;
                return(TN_FAILURE);
            }
            //strcpy (channelsfile, value);
        } else if (strcasecmp (tag, "DBSERVICE") == 0) {
            if (strlen (value) > MAXSTR) {
                cout << "Error ADADup:ParseConfiguration(): DBservice field too long"<<endl;
                return(TN_FAILURE);
            }
            strcpy (dbservice, value);
        } else if (strcasecmp (tag, "DBUSER") == 0) {
            if (strlen (value) > MAXSTR) {
                cout << "Error ADADup:ParseConfiguration(): DBuser field too long"<<endl;
                return(TN_FAILURE);
            }
            strcpy (dbuser, value);
        } else if (strcasecmp (tag, "DBPASSWD") == 0) {
            if (strlen (value) > MAXSTR) {
                cout << "Error ADADup:ParseConfiguration(): DBpasswd field too long"<<endl;
                return(TN_FAILURE);
            }
            strcpy (dbpass, value);
        } else if (strcasecmp (tag, "SLAVE_IP_ADDRESS") == 0) {
            if (strlen (value) > MAXSTR) {
                cout << "Error ADADup:ParseConfiguration(): Slave_IP_Address field too long"<<endl;
                return(TN_FAILURE);
            }
            strcpy (slave_mashine_ip_address, value);
        } else if (strcasecmp (tag, "SLAVE_IP_PORT") == 0) {
            slave_mashine_ip_port=atoi (value);
        } else if (strcasecmp (tag, "START_TIMES_DIR") == 0) {
            if (strlen (value) > MAXSTR) {
                cout << "Error ADADup:ParseConfiguration(): Start_Times_Dir field too long"<<endl;
                return(TN_FAILURE);
            }
            strcpy (start_times_dir, value);
        } else if (strcasecmp (tag, "START_TIMES_FILE") == 0) {
            if (strlen (value) > MAXSTR) {
                cout << "Error ADADup:ParseConfiguration(): Start_Times_File field too long"<<endl;
                return(TN_FAILURE);
            }
            strcpy (start_times_file, value);
        } else if (strcasecmp (tag, "START_TIMES_MODE") == 0) {
            start_times_mode=atoi (value);
        } else if (strcasecmp (tag, "GCDA_READ_MODE") == 0) {
            gcda_read_mode=atoi (value);
        } else if (strcasecmp (tag, "MAX_SECONDS_BEHIND_REALTIME") == 0) {
            MAX_SECONDS_BEHIND_REALTIME=atoi (value);
        } else if (strcasecmp (tag, "DBMAXCONNECTIONRETRIES") == 0) {
            dbretries=atoi (value);
        } else if (strcasecmp (tag, "DBCONNECTIONRETRYINTERVAL") == 0) {
            dbinterval=atoi (value);
        } else {
            cout << "Error (ADADup::ParseConfiguration): Unrecognized config line" << endl;
            return(TN_FAILURE);
        }
    }
    catch (...) {
        cout << "Error (ADADup::ParseConfiguration): Unknown type parsing error." << endl;
        return(TN_FAILURE);
    }
    return(TN_SUCCESS);
}     



int GCDADupMasterTSPModuleManager::Startup() 
{
    int res;
    //sm.LogConsole(replevel);
    
    sm.InfoMessage("(ADADup::Startup): Starting...");
    // attempt to lock the file
    lock = new LockFile(lock_file_name);
    if (lock && lock->isValid()) {
        if(lock->lock() == -1) {
	    sm.ErrorMessage("(ADADup::Startup): Unable to lock LockFile, another instance of ADADup is running");
	    return(TN_FAILURE);
        }
    } else {
	sm.ErrorMessage("(ADADup::Startup): Unable to open/create LockFile");
	return(TN_FAILURE);
    }
    
    GCDADupMasterModuleControlBlock::load(sm);
    GCDADupMasterModuleControlBlock::load(start_times_dir, start_times_file);
    
    res = init_by_db();
    if (res == TN_FAILURE) {
        sm.ErrorMessage("(ADADup::Startup): Unable to initialize internal module.");
        cleanup();
        return(TN_FAILURE);
    }
    
    
    GCDADupMasterMCBwriter :: load(sm);
    string s ((char*) slave_mashine_ip_address);
    res = GCDADupMasterTransportWriter:: load (s , slave_mashine_ip_port);
    if (res == TN_FAILURE) {
        sm.ErrorMessage("(ADADup::Startup): Unable to initialize network module.");
        cleanup();
        return(TN_FAILURE);
    }

    network_writer = (GCDADupMasterMCBwriter*) new GCDADupMasterTransportWriter ( 0 );
    if( network_writer == NULL ){
        sm.ErrorMessage("(ADADup::Startup): No memory to create network writer.");
        cleanup();
        return(TN_FAILURE);
    }
    sig_wrapper_old = signal (SIGTERM, sig_handler_wrapper);
    signal (SIGPIPE, SIG_IGN);  //because no cms
    signal (SIGHUP, sig_handler_wrapper);
    signal (SIGINT, sig_handler_wrapper);
    signal (SIGQUIT, sig_handler_wrapper);
    signal (SIGTERM, sig_handler_wrapper);
    // These really shouldn't be caught:
    //    signal (SIGILL, sig_handler_wrapper);
    //    signal (SIGTRAP, sig_handler_wrapper);
    //    signal (SIGFPE, sig_handler_wrapper);
    //    signal (SIGKILL, sig_handler_wrapper);
    //    signal (SIGBUS, sig_handler_wrapper);
    //    signal (SIGSEGV, sig_handler_wrapper);
    //    signal (SIGSYS, sig_handler_wrapper);
    
    TimeStamp t = TimeStamp::current_time();
    SetNextWakeup (t);
    return TN_SUCCESS;
}

unsigned int usec_from_rate(float rate ) {
    unsigned int ret ;
    ret =  (unsigned int) lround((1.0/rate) * USECS_PER_SECOND);

return ret;
}

int GCDADupMasterTSPModuleManager::init_by_db() {
    key_t inda_key;
    
    char tmp_loc[3];
    ChannelConfigList configchans;
    ChannelConfigList::iterator clp;
    ChannelConfigList::iterator clpnext;
    sm.InfoMessage("(ADADup::Startup): configuring using database");
    
    if (strcmp(gcda_key_name, "") ==0) {
        sm.ErrorMessage("(ADADup::InitByDB): gcda_key_name not given");
        return(TN_FAILURE);
    }
    
    // first get the channels configured for this progname
    ChannelReaderDB cdb(dbservice, dbuser, dbpass);
    if (!cdb) {
        sm.ErrorMessage("ADADup::InitByDB() failed to connect to database");
        return TN_FAILURE;
    }
    cdb.SetRetryInterval(dbinterval);
    cdb.SetNumRetries(dbretries);
    
    sm.InfoMessage(Compat::Form("Loading list of configured channels from database for program: %s", dbprogname));
    
    // Get the configured channels from the database
    if (cdb.GetChannels(dbprogname, configchans) != TN_SUCCESS) {
        sm.ErrorMessage("(ADADup::InitByDB): Unable to retrieve list of configured channels");
        return(TN_FAILURE);
    }
    
    if (configchans.size() == 0) {
        sm.ErrorMessage("(ADADup::InitByDB): No configured channels found in database");
        return(TN_FAILURE);
    } else {
        sm.InfoMessage(Compat::Form("Retrieved %d channels from database",
        configchans.size()));
    }
    
    // Initialize the GCDA
    inda = new Generic_CDA<GCDA_data_type>(string(gcda_key_name), 0);
    if (inda == NULL) {
        sm.ErrorMessage("(ADADup::InitByDB): Unable to allocate ADA");
        return(TN_FAILURE);
    }

#ifdef STARTPOSITION_MMAP    
    //Initialize StartPosition
    GCDADupMasterModuleControlBlock::SP_data.load(configchans);
    if ( ! GCDADupMasterModuleControlBlock::SP_data ) {
       sm.ErrorMessage("(ADADup::ChanLoad): Start position: Initialisation: FAILED.");
    }
        
#endif
    
    inda_key = inda->Get_Memory_Key();
    clp = configchans.begin();
    while (clp != configchans.end()) {
        
        strcpy(tmp_loc, (*clp).first.location);
        mapLC((char*)(*clp).first.network, tmp_loc, ASCII);
        Channel chnl = (*clp).first;
        
        GCDADupMasterModuleControlBlock* cb =
        (GCDADupMasterModuleControlBlock*) new GCDADupMasterModuleControlBlock(chnl, start_times_mode);
        
        if( cb == NULL ){
            sm.ErrorMessage(Compat::Form("(ADADup::InitByDB): %s %s.%s.%s.%s",
            (char*) "No memory to create an internal module for channel",
            (char*) (*clp).first.network, (char*) (*clp).first.station, (char*)(*clp).first.channel, tmp_loc));
            return(TN_FAILURE);
        }
        
        
        cb->reader = new Data_Channel_Reader<GCDA_data_type>(inda_key);
        if( cb->reader == NULL){
            delete cb;
            sm.ErrorMessage(Compat::Form("(ADADup::InitByDB): %s %s.%s.%s.%s",
            (char*) "No memory to create a <Data_Channel_Reader> for channel",
            (char*) (*clp).first.network, (char*) (*clp).first.station, (char*)(*clp).first.channel, tmp_loc));
            return(TN_FAILURE);
            
        }
        
        if( cb->reader->Initialize_Channel(inda->Start_of_Memory_Area(),
        (char*)chnl.network, (char*)chnl.station, (char*)chnl.channel, (char*)chnl.location) == TN_FAILURE){
            delete cb;
            sm.InfoMessage(Compat::Form("(ADADup::InitByDB): %s %s.%s.%s.%s",
            (char*) "WARNING: Unable to initialize gcda for channel",
            (char*) (*clp).first.network, (char*) (*clp).first.station, (char*)(*clp).first.channel, tmp_loc));
            clp++;
            continue;
        }
        
        //we have to use <Channel_Monitor> because <Channel_Reader> has a bug:
        //does not lock gcda when return "time_of_newest_sample_in_cda()"
        cb->monitor = new Data_Channel_Monitor<GCDA_data_type>(inda_key);
        if( cb->monitor == NULL ) {
            delete cb;
            sm.ErrorMessage(Compat::Form("(ADADup::InitByDB): %s %s.%s.%s.%s",
            (char*) "No memory to create a <Data_Channel_Monitor> for channel",
            (char*) (*clp).first.network, (char*) (*clp).first.station, (char*)(*clp).first.channel, tmp_loc));
            return(TN_FAILURE);
        }
        if(cb->monitor->Initialize_Channel(inda->Start_of_Memory_Area(),
        (char*)chnl.network, (char*)chnl.station, (char*)chnl.channel, (char*)chnl.location) == TN_FAILURE){
            delete cb;
            sm.InfoMessage(Compat::Form("(ADADup::InitByDB): %s %s.%s.%s.%s",
            (char*) "WARNING: Unable to initialize monitor gcda for channel",
            (char*) (*clp).first.network, (char*) (*clp).first.station, (char*)(*clp).first.channel, tmp_loc));
            clp++;
            continue;
        }
        
        TimeStamp start_time = TimeStamp::current_time();;
        switch  ( start_times_mode ) {
            case 0 :
                start_time = cb->monitor->time_of_oldest_sample_in_cda();
                break;
            case 1:
                start_time = TimeStamp( cb->start_position_time);
                break;
            case 2 :
                start_time = cb->monitor->time_of_newest_sample_in_cda();
                break;
            default :
                start_time = TimeStamp( cb->last_access_time);
        }
        
        (void) cb->reader->Set_Time_of_Last_Sample_Read(start_time);
        cb->last_retrieved_sample_time = start_time;
        TimeStamp ts(start_time);
        sm.InfoMessage(Compat::Form("(ADADup::InitByDB): Time_of_Last_Sample_Read is set to %04d/%02d/%02d %02d:%02d:%02d.%03d for channel: %s.%s.%s.%s",
        ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
        (char*) (*clp).first.network, (char*) (*clp).first.station, (char*)(*clp).first.channel, tmp_loc));
        
        cb->usec_per_sample = usec_from_rate(cb->reader->Rate_in_Samples_per_Second());
        
        modlist.push_back(cb);
        
        sm.InfoMessage(Compat::Form("(ADADup::InitByDB): Initialized internal module for %s.%s.%s.%s",
        (*clp).first.network, (*clp).first.station, (*clp).first.channel, tmp_loc ));
        
        clp++;
    } // end of while loop
    
    sm.InfoMessage(Compat::Form("(ADADup::InitByDB): Initialized %d internal modules.", modlist.size()));
    
    if( modlist.size() < 1 ) {
        sm.ErrorMessage("(ADADup::InitByDB): Fatal Error: No internal modules created.");
        return(TN_FAILURE);
    }
    
    return TN_SUCCESS;
}

int GCDADupMasterTSPModuleManager::Run() {
    
    sm.InfoMessage("(ADADup::Run): Master Running...");
    
    GCDADupMasterModuleControlBlockList::iterator mit;
    GCDADupMasterMCBwriter * wcb = network_writer;
    
    
    struct timespec sleeptime, slepttime;
    struct timeval temptime ;
    TimeStamp loop_start_time = TimeStamp::current_time();;
    TimeStamp loop_end_time = TimeStamp::current_time();;
    double diff, loop_period = (double) GCDA_READ_INTIME_PERIOD;
    
    int mod_size = modlist.size();
    bool was_activity;
    int read_res;
    int pack_ret;

    while(no_signal){
        
        was_activity = false;
        loop_start_time = TimeStamp::current_time();
        
        GCDADupMasterModuleControlBlock* mcb;
        
        for(mit = modlist.begin();mit!=modlist.end();mit++) {
            mcb = (*mit);
            if (!no_signal) break;
            
            read_res = TN_FALSE;
            switch (gcda_read_mode) {
                case 1: 
                    read_res = mcb->read_by_packet_all();
                    break;
                case 2: 
                    read_res = mcb->read_by_packet_follow_rt() ;
                    break;
                default :
                    read_res = mcb->read_by_sample();   
            }
            
            if ( read_res == TN_TRUE ) {
                was_activity = true;
                
                while ( mcb->is_data_to_pack() == TN_TRUE ) {
                    
                    if (!no_signal) break;
                    
                    unsigned int packed  = 0;
                    unsigned int maxnum = mcb->nsamples_extracted;
                    unsigned int num = mcb->nsamples_extracted - mcb->nsamples_packed ;
                    unsigned int idx = mcb->nsamples_packed + 1;
                    if (mcb->mode) {
                        //we are sure that before this time data has sent
                        (void)mcb->update_start_position(mcb->times_buff[idx]);
                    }
                    pack_ret = TN_BEGIN ;
                    pack_ret = wcb->pack_data( packed , num, mcb->channel, &mcb->data_buff[idx], &mcb->times_buff[idx] );
                    mcb->nsamples_packed += packed ;
                    
                    if ( packed )
                        if ( LOG_VERBOSE_LEVEL > 1 ) {
                            TimeStamp ts(mcb->times_buff[idx]);
                            TimeStamp te(mcb->times_buff[(idx+packed<maxnum)?idx+packed-1:maxnum]);
                            
                            sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, StartTime: %04d/%02d/%02d %02d:%02d:%02d.%03d, EndTime: %04d/%02d/%02d %02d:%02d:%02d.%03d, Samples: %3d, Packed: OK.",
                            (char*)mcb->channel.network, (char*)mcb->channel.station, (char*)mcb->channel.channel, (char*)mcb->tmp_loc,
                            ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
                            te.year(), te.month_of_year(), te.day_of_month(), te.hour_of_day(), te.minute_of_hour(), te.second_of_minute(), te.milliseconds(),
                            packed ));
                            
                        }
                    
                    switch ( pack_ret ) {
                        case TN_END:
                            if (wcb->write_data()==TN_SUCCESS) {
                                if ( LOG_VERBOSE_LEVEL > 1 ) {
                                   sm.InfoMessage(Compat::Form("(ADADup::Run): Network: Send, Samples: %3d, Status: OK", wcb->get_npacket() ));
                                }
                            } else  {
                                if ( LOG_VERBOSE_LEVEL > 1 ) {
                                   sm.InfoMessage(Compat::Form("(ADADup::Run): Network: Send, Samples: %3d, Status: FAIL", wcb->get_npacket() ));
                                }
                            }
                            wcb->clear_pack(); 
                            break;
                        case TN_BEGIN:
                            ;//something wrong
                        default:
                            ; //go to pack more the next channel
                    }//switch
                } //while is data
            } //read_timeseries
        }//for
        //regardless of activity we need to send all data remaining in buffer   
        //this will send a heartbeat if no data at all
        if (no_signal) {  
            switch (wcb->write_data_rest()) {
                case TN_SUCCESS:
                    if ( LOG_VERBOSE_LEVEL > 1 ) {
                        sm.InfoMessage(Compat::Form("(ADADup::Run):  Network: Send, Samples: %3d, Status: OK", wcb->get_npacket() ));
                    }
                    wcb->clear_pack();
                    break;
                case TN_FAILURE:
                    if ( LOG_VERBOSE_LEVEL > 1 ) {
                        sm.InfoMessage(Compat::Form("(ADADup::Run): Network: Send, Samples: %3d, Status: FAIL", wcb->get_npacket() ));
                    }
                    wcb->clear_pack();
                    break;
                case TN_NODATA:
                    ; //heartbeat time not exceed yet, we can try to fill up the buffer more  
                default: 
                    ; // go to the hext iteration
            }
        }
        //let us give CPU busyless time back
        if (! was_activity) {
            loop_end_time = TimeStamp::current_time();
            diff = (double) Duration(loop_end_time - loop_start_time);
            if (diff < loop_period){

                temptime = Duration ( loop_period - diff ).tv(); 
                sleeptime.tv_sec = temptime.tv_sec;
                sleeptime.tv_nsec = temptime.tv_usec * 1000;
                if  (nanosleep(&sleeptime, &slepttime) == -1 ) {
                    sm.ErrorMessage(Compat::Form("(ADADup::Run):  Nanosleep fails, errno: %d ", errno ));
                }
            }
        }
    
    }//while
    sm.InfoMessage("(ADADup::Run): Master exit on signal");
    //explicit signal send to master program 
    if (sig_wrapper_old) sig_wrapper_old (SIGINT);
    
    return(TN_SUCCESS);
    
}

int GCDADupMasterTSPModuleManager::Shutdown(){
    
            //stop();
            
            return TN_SUCCESS;
}
