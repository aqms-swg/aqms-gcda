/** 
* @file
* @ingroup group_gcda
* @brief Header file for slave_socket.C
*/

#ifndef __GCDADupSlaveTSPModule_h
#define __GCDADupSlaveTSPModule_h
#include <sys/time.h>
#include <string>
#include <cstring>
#include <cerrno>
#include <csignal>

#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "project_globals.h"
#include "dwriter.h"
#include "Duration.h"
#include "StatusManager.h"



class GCDADupSlaveMCBreader {
    protected: 
        static StatusManager sm;
        size_t serialized_size;
        size_t serialized_header_size;
        size_t network_packet_size;
    public:
    sock_msg_packet mess;
    public:
        static void load (StatusManager& s){sm = s;};
        GCDADupSlaveMCBreader() {
            msg_struct test_struct;
            msg_header_struct test_header_struct;
            test_struct.serialize(serialized_size, mess.area);
	    // following looks wrong: except for here, msg_header_struct
	    // is never serialized explicitly.
	    // Instead is is serialized implicitly by the definition of
	    // sock_msg_struct and sock_msg_socket. PNL 2012/03/02
            // test_header_struct.serialize(serialized_header_size, mess.area);
	    // So we just use sizeof msg_header_struct:
	    serialized_header_size = sizeof(msg_header_struct);
            network_packet_size = NET_BUFFER_SIZE * serialized_size + serialized_header_size; //sizeof(msg_header_struct);
        };
        virtual ~GCDADupSlaveMCBreader() {};
        virtual int reader_init()=0 ;
        virtual int reader_close()=0 ;
        virtual int read_data()=0;
        virtual int read_heartbeat()=0;
     
};


class GCDADupSlaveTransportReader: public GCDADupSlaveMCBreader {
        static in_addr_t saddress_s;
        static int sport_pool_start_s ;

        static in_addr_t saddress_m;
        static int sport_pool_start_m ;

        int s;                          // slave socket number (attach to)
        int ms;                         // master socket (work with)
        
        int slave_port;
        struct sockaddr_in slave_ip4;   // Server socket address structure
        int master_port;
        struct sockaddr_in master_ip4;   // Server socket address structure
        bool socket_open;
        bool msocket_open;
        bool accepted;
        TimeStamp last_readed_time;
        TimeStamp last_accepted_time;
    
    public:
        GCDADupSlaveTransportReader(int);
        ~GCDADupSlaveTransportReader();
        static int load(const string&,int,const string&,int);
        int reader_init () ;
        int reader_close ();
        int reader_close_accept ();
        int try_to_accept();
        int try_to_read() ;
        int read_data ();
        int raw_read (int&,char*&);
        int send_ack () ;
        void reader_wait(int,int);
        int read_heartbeat();
        void set_timeout_checktime(TimeStamp&);
        int check_timeout (TimeStamp&, double );

        
};










class GCDADupSlaveModuleControlBlock{
            protected:
                static StatusManager sm;
            
            public:
                
                Channel channel;
                char tmp_loc [3];
                
                static void load (StatusManager& s){sm = s;};
                GCDADupSlaveModuleControlBlock(Channel);
                ~GCDADupSlaveModuleControlBlock();
                
                Data_Channel_Writer<GCDA_data_type> *writer;
                GCDADupSlaveMCBreader *reader;

                TimeStamp nexttime;
                TimeStamp start_timestamp;
                unsigned int nsamples_extracted;
                unsigned int nsamples_processed;
                unsigned int usec_per_sample;
 
                int write_size;
                
                struct TimeStamp time_buff [ GCDA_SLAVE_OUTSIZE ];
                GCDA_data_type data_buff [ GCDA_SLAVE_OUTSIZE ];
                
                
                //Timeseries Processing helper functions//
               
                //void write_timeseries(sock_msg_packet &  );
                void pack_data( msg_struct & , int );
                int write_data( int );
                int write_by_packet( int );
};


#endif
