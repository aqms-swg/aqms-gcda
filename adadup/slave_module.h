/** 
* @file
* @ingroup group_gcda
* @brief Header file for slave_module.C
*/

#ifndef __GCDADupSlaveTSPModuleManager_h
#define __GCDADupSlaveTSPModuleManager_h

#include "gcda.h"
#include "project_globals.h"
#include "slave_socket.h"
#include "ChannelReaderDB.h"
#include "Application.h"   
#include "LockFile.h"
#include "chan.h"
#include "findchancfg.h"
#include "chancfg.h"
#include "nscl.h"



typedef vector<GCDADupSlaveModuleControlBlock*> GCDADupSlaveModuleControlBlockList;
struct ltnscl {
    bool operator() (const nscl &c1, const nscl &c2) const {
         if (strcmp(c1.network, c2.network) < 0) {
         return(TN_TRUE);
         }
         if ((strcmp(c1.network, c2.network) == 0) &&
         (strcmp(c1.station, c2.station) < 0)) {
         return(TN_TRUE);
         }
        
        
         if ((strcmp(c1.network, c2.network) == 0) &&
         (strcmp(c1.station, c2.station) == 0) &&
         (strcmp(c1.location, c2.location) < 0)) {
         return(TN_TRUE);
         }
        
         if ((strcmp(c1.network, c2.network) == 0) &&
         (strcmp(c1.station, c2.station) == 0) &&
         (strcmp(c1.location, c2.location) == 0) &&
         (strcmp(c1.channel, c2.channel) < 0)) {
         return(TN_TRUE);
         }
        
         return(TN_FALSE);
    }
};

class GCDADupSlaveTSPModuleManager : public Application{
    private:
        //config variables
        static char master_mashine_ip_address[MAXSTR];
        static int  master_mashine_ip_port;
        static char slave_mashine_ip_address[MAXSTR];
        static int  slave_mashine_ip_port;
        
        char gcda_key_name[GCDA_KEY_NAME_LEN];
        static bool all_channels_dup;
        
        char dbprogname[MAXSTR];
        char dbservice[MAXSTR];
        char dbuser[MAXSTR];
        char dbpass[MAXSTR];
        int  dbinterval;
        int  dbretries;
        int  checklist;
        
        
        
        bool use_db;
        char lock_file_name[MAXSTR];
        LockFile *lock;
        
        //internal variable
        Generic_CDA<GCDA_data_type> *outda;
        GCDADupSlaveMCBreader *    network_reader;
        GCDADupSlaveModuleControlBlockList modlist;
        map<nscl, GCDADupSlaveModuleControlBlock*, ltnscl> modmap;
        
        int number_of_channels_found;
        struct channel_identifier_type channels[MAX_CHANNELS_IN_NETWORK];
        
        void init();
        int init_by_db();
        void cleanup();
        void stop();
        
        
        public:
            
            
        GCDADupSlaveTSPModuleManager();
        ~GCDADupSlaveTSPModuleManager(){stop();};
        int ParseConfiguration(const char *tag, const char *value);
        int Startup();
        int Run();
        int Shutdown(){stop();return TN_SUCCESS;};

};

#endif

