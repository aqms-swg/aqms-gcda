/** 
* @file
* @ingroup group_gcda
*/

#ifndef __ProjectGlobals_h
#define __ProjectGlobals_h




/* This configuration for  ADA 
   we read ADA sample by sample and send over network
   we use ONE network socket to serve 10000 channels
   we use Network buffer size for about 50 samples 
 */

#define     VERSION             "2.1.0 16-May-2014"  
#define     LOG_VERBOSE_LEVEL          1                            //debug verbose: 0- no msg, 1-normal, 2- tons of msg 3-gap finder
#define     LOG_DEFAULT_FILENAME      "log_gcdadup"       
#define     DEFAULT_STARTPOSITION_DIR "startposition"       
#define     DEFAULT_STARTPOSITION_FILE "times.mmap"       
#define     GCDA_DATA_PERIOD          5                             //data refresh period in sec 
#define     GCDA_NUMBER_OF_CHANNELS   1000                          //this value is used ONLY for estimate optimal buffer size
#define     GCDA_MAX_SECONDS_BEHIND_REALTIME 180

#define     GCDA_READ_INTIME_PERIOD   (GCDA_DATA_PERIOD / 2)        //I think a value close to half of period is OK to keep data fresh
#define     GCDA_WRITE_INTIME_PERIOD  GCDA_READ_INTIME_PERIOD                 
#define     GCDA_MASTER_INSIZE        (GCDA_NUMBER_OF_CHANNELS / GCDA_DATA_PERIOD / 2) 
#define     GCDA_SLAVE_OUTSIZE        GCDA_MASTER_INSIZE                  
#define     SEND_GET_ACK              1
#define     NET_NON_BLOCKING_CONNECT  0
#define     NET_BUFFER_SIZE           (GCDA_NUMBER_OF_CHANNELS / GCDA_DATA_PERIOD / 2)
#define     NET_WOULDBLOCK_WAIT       100  // time in milliseconds
#define     NET_CONNECT_WAIT          1                 
#define     NET_ACK_TIMEOUT           3.0               
#define     NET_HEARTBEAT_INTERVAL    (2 * NET_ACK_TIMEOUT + 0.01)             
#define     NET_CONNECT_TIMEOUT       (2 * NET_HEARTBEAT_INTERVAL + 0.01)      
#define     NET_BIND_TRIES            40                


#include "data_types.h"
#include "RetCodes.h"
#include "Channel.h"
#include <list>

using namespace std;
extern int no_signal;
extern int MAX_SECONDS_BEHIND_REALTIME;



//Pet Lombard has changed struct timeval to double 
//and noticed that type double require aligment 8 for SPARC platform
//
//alignment by 8 works for SPARC (V8, Generic) and X86
//SPARC V9 require alingment 16 for lter_socket.cc:136: error: format ‘%u’ expects type ‘unsigned int*’, but argument 3 has type ‘__time_t*’
//ng double 
//const int MSG_STRUCT_ALLIGNMENT_ADJUST_BY_8 = 8 +
//    ((sizeof(nscl) + sizeof(struct timeval)+ sizeof(GCDA_data_type))%8);

#pragma pack(16)
//
// the nework packet structure 
// 
struct msg_header_struct {            // A header for a network packet    
    char          reserved;		     // filled with 'C' for selfcheck reason (version: A-1, B-2, C-3)
//  uint32_t      seq_num;            // sequence number 
    uint32_t      num;                // number of samples in packet [32 bit]
    uint32_t      sample_size;        // size of one msg_struct   [32 bit] 
    
    void serialize(size_t& size, char* buff) {
        char* ptr = buff;
        size = 0;
        size += ::serialize (reserved, ptr );
        size += ::serialize (num, ptr );
        size += ::serialize (sample_size, ptr );
    }
    void deserialize(size_t& size, char* buff) {
        char* ptr = buff;
        size = 0;
        size += ::deserialize (reserved, ptr );
        size += ::deserialize (num, ptr );
        size += ::deserialize (sample_size, ptr );
    }
};

struct msg_struct {                  // ONE sample struct    
    nscl              channel;       // channel name for the sample (sizeof=16)
    double            time;       // time for the sample (sizeof=8)
    GCDA_data_type       data;       // the sample (sizeof=bad_sizeof)
    //char alignment[MSG_STRUCT_ALLIGNMENT_ADJUST_BY_8]; // just for alligment  
    
    void serialize(size_t& size, char* buff) {
        char* ptr = buff;
        size = 0;
        
        size += ::serialize (channel, ptr );
        size += ::serialize (time, ptr );
        
        size_t s;
        data.serialize (s, ptr); 
        size += s;

    }
    void deserialize(size_t& size, char* buff) {
        char* ptr = buff;
        size = 0;
        
        size += ::deserialize (channel, ptr );
        size += ::deserialize (time, ptr );
        
        size_t s;
        data.deserialize (s, ptr); 
        size += s;
        
    }
};
#pragma pack()


struct sock_msg_struct {                         // structure for acssess to socket packet
    struct msg_header_struct  header;            // header  
    char   first_msg_char;                       // a pointer to a first sample in the packet
    //...                                       
};

const int NET_MAX_BUFFER_SIZE = NET_BUFFER_SIZE * sizeof(struct msg_struct) + sizeof(struct msg_header_struct);
union sock_msg_packet {                      // socket's packet
    char   area [NET_MAX_BUFFER_SIZE];       // real socket's packet size
    struct sock_msg_struct message;          // struct to get access to data
};


#endif

