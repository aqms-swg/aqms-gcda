/** 
* @file
* @ingroup group_gcda
* @brief Header file for master_module.cc
*/

#ifndef __GCDADupMasterTSPModuleManager_h
#define __GCDADupMasterTSPModuleManager_h

#include "gcda.h"
#include "project_globals.h"
#include "master_socket.h"
#include "ChannelReaderDB.h"
#include "Application.h"   
#include "LockFile.h"
#include "chan.h"
#include "findchancfg.h"
#include "chancfg.h"
#include "nscl.h"


typedef vector<GCDADupMasterModuleControlBlock*> GCDADupMasterModuleControlBlockList;
class GCDADupMasterTSPModuleManager : public Application{
    
    private:
        
        static char slave_mashine_ip_address[MAXSTR];
        static int  slave_mashine_ip_port;
        
        char gcda_key_name[GCDA_KEY_NAME_LEN];
        
        
        static bool all_channels_dup;
        
        static char dbprogname[MAXSTR];
        static char dbservice[MAXSTR];
        static char dbuser[MAXSTR];
        static char dbpass[MAXSTR];
        static int  dbinterval;
        static int  dbretries;
        
        
        static int  start_times_mode;
        static char start_times_dir[PATH_MAX];
        static char start_times_file[PATH_MAX];
        
        static bool use_db;
        static char lock_file_name[MAXSTR];
        static LockFile *lock;
        
        static int gcda_read_mode;
        
        int  checklist;

        
        //internal variable
        Generic_CDA<GCDA_data_type> *inda;
        GCDADupMasterMCBwriter *                network_writer;
        GCDADupMasterModuleControlBlockList     modlist;
        int number_of_channels_found;
        struct channel_identifier_type channels[MAX_CHANNELS_IN_NETWORK];

        
        void init();
        int init_by_db();
        void cleanup ();
        void stop();
        //void GetChannels (string , gcdadupProperties, ChannelConfigList& ) throw (Error);
        
    public:
        
        GCDADupMasterTSPModuleManager ();
        ~GCDADupMasterTSPModuleManager (){stop();};
        int ParseConfiguration(const char *tag, const char *value);
        int Startup();
        int Run();
        int Shutdown();
        
        
        
};

#endif

