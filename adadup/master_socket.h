/** 
* @file
* @ingroup group_gcda
* @brief Header file for master_socker.cc
*/

#ifndef __GCDADupMasterTSPModule_h
#define __GCDADupMasterTSPModule_h
#include <sys/time.h>
#include <string>
#include <map>
#include <cstring>
#include <cerrno>
#include <csignal>
#include <sys/types.h>
#include <sys/mman.h>

#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>


#include "project_globals.h"
#include "dreader.h"
#include "dcmon.h"
#include "ChannelReaderDB.h"
#include "StatusManager.h"



class GCDADupMasterMCBwriter {
    protected: 
        static StatusManager sm;
        sock_msg_packet mess;
        size_t max_packet_size;
        unsigned int npacket;
        size_t serialized_size;
        size_t serialized_header_size;
        size_t network_packet_size;
    public:
        
        
        
        static void load (StatusManager& stat_man) {sm = stat_man;};
        GCDADupMasterMCBwriter() {
            max_packet_size =  ( NET_MAX_BUFFER_SIZE - sizeof(msg_header_struct) ) / sizeof (msg_struct);
            //test_size 
            msg_struct test_struct;
            msg_header_struct test_header_struct;
            test_struct.serialize(serialized_size, mess.area);
            test_struct.serialize(serialized_size, mess.area);
	    // following looks wrong: except for here, msg_header_struct
	    // is never serialized explicitly.
	    // Instead is is serialized implicitly by the definition of
	    // sock_msg_struct and sock_msg_socket. PNL 2012/03/02
            // test_header_struct.serialize(serialized_header_size, mess.area);
	    // So we just use sizeof msg_header_struct:
	    serialized_header_size = sizeof(msg_header_struct);
            network_packet_size = NET_BUFFER_SIZE * serialized_size + serialized_header_size; //sizeof(msg_header_struct);
            clear_pack();
        };
        int pack_data( unsigned & , unsigned int , Channel&, GCDA_data_type*  , TimeStamp *);
        void pack_header() {
            mess.message.header.reserved = 'C'; // signature
            mess.message.header.num = npacket ;
            wordorder_master(mess.message.header.num);
            mess.message.header.sample_size = serialized_size ;
            wordorder_master(mess.message.header.sample_size);
        }
        void clear_pack(){
            memset(mess.area, 0, NET_MAX_BUFFER_SIZE );
            npacket = 0;
        };
        unsigned int get_npacket(){return npacket;};
        void print_pack();
        virtual ~GCDADupMasterMCBwriter() {};
        virtual int writer_init()=0 ;
        virtual int writer_close()=0 ;
        virtual int write_data()=0;
        virtual int write_data_rest()=0;
        virtual int write_heartbeat()=0;
        virtual void writer_wait (unsigned int, unsigned int) = 0;
};



class GCDADupMasterTransportWriter: public GCDADupMasterMCBwriter {
            
        
        int s;                          // socket number
        int port;
        struct sockaddr_in slave_ip4;   // Server socket address structure
        bool socket_open;
        bool connected;
        bool postphoned;
        TimeStamp last_sent_time;
        TimeStamp last_heartbeat_time;
        TimeStamp start_connect_time;
#ifdef SEND_GET_ACK        
        TimeStamp start_ackwait_time;
#endif
    protected:
        
        static in_addr_t saddress;
        static int sport_pool_start ;

            
    public:
        
        static int load(string&,int);
        
        GCDADupMasterTransportWriter(int);
        ~GCDADupMasterTransportWriter();
        //static gcdadupProperties properties;
        int writer_init () ;
        int writer_connect () ;
        int writer_close () ;
        int try_to_connect() ;
        int try_to_write(int,int) ;
        int write_data();
        int write_data_rest();
        int write_data_from_buffer();
        int raw_write (int & , char* );
        void writer_wait (unsigned int, unsigned int) ;
        int wait_for_ack() ;
        int write_heartbeat();
        void set_timeout_checktime(TimeStamp&);
        int check_timeout (TimeStamp&, double );

        
};

//human-readble file
typedef char disk_map [80];

struct ltnscl {
    bool operator() (const nscl &c1, const nscl &c2) const {
         if (strcmp(c1.network, c2.network) < 0) {
         return(TN_TRUE);
         }
         if ((strcmp(c1.network, c2.network) == 0) &&
         (strcmp(c1.station, c2.station) < 0)) {
         return(TN_TRUE);
         }
         if ((strcmp(c1.network, c2.network) == 0) &&
         (strcmp(c1.station, c2.station) == 0) &&
         (strcmp(c1.location, c2.location) < 0)) {
         return(TN_TRUE);
         }
         if ((strcmp(c1.network, c2.network) == 0) &&
         (strcmp(c1.station, c2.station) == 0) &&
         (strcmp(c1.location, c2.location) == 0) &&
         (strcmp(c1.channel, c2.channel) < 0)) {
         return(TN_TRUE);
         }
        
         return(TN_FALSE);
    }
};



class GCDADupMasterModuleControlBlock{
            protected:
                
                
            public:
                static StatusManager sm;
                Channel channel;
                char tmp_loc [3];
                
                
        
                static int in_size;
                
                static void load (StatusManager& s){sm = s;};
                static int load (char *, char*);
                GCDADupMasterModuleControlBlock(Channel,int);
                ~GCDADupMasterModuleControlBlock();
                
                //Data reader and writer//
                
                Data_Channel_Reader<GCDA_data_type> *reader;
                //we have to have monitor as far as we want to use 
                //time_of_newest_data_in_cda &time_of_oldsest_data_in_cda()
                //dreader.h has a BUG with not locking shared memory when retriving this values
                Data_Channel_Monitor<GCDA_data_type> *monitor;
                GCDADupMasterMCBwriter *writer;
                
                //Variables to keep track of buffers//
                TimeStamp last_sucessfull_sent_time;
                
                TimeStamp last_gcda_read_time;
                
                TimeStamp last_access_time;
                TimeStamp last_retrieved_sample_time;
                
                TimeStamp start_timestamp;
                TimeStamp newest_sample_time;
                TimeStamp start_position_time;
                
                bool mode;
                static char  start_position_dir [PATH_MAX];
                static char  start_position_file [PATH_MAX];
                char  start_position_file_path [PATH_MAX];
                static int gcda_read_mode;
                
                static class SP { //startpositoon
                    bool initialized;
                    unsigned int len;
                    disk_map* timemap;
                    public:
                        SP();
                        ~SP();
                        void load(ChannelConfigList& );
                        void shutdown();
                        int set_time (char *, const struct nscl& , const struct TimeStamp&  );
                        int set_time (char *, const struct Channel& , const struct TimeStamp& );
                        int get_time (char *, const struct nscl&, struct TimeStamp&  ); 
                        int get_time (char *, const struct Channel&, struct TimeStamp& ); 
                        char * initialize_position (const struct Channel& );
                        void rawncpy(char* , char*, int );
                        bool operator ! ();
                } SP_data ;
                char * SP_channel_ptr;

                unsigned int nsamples_extracted;
                unsigned int nsamples_processed;
                unsigned int usec_per_sample;
                //double dura_1_sample;
                
                unsigned int nsamples_packed;
                
                bool last_read_gap;
                
                int read_size;
                
                //Timeseries data//
                struct TimeStamp times_buff[GCDA_MASTER_INSIZE];
                GCDA_data_type data_buff[GCDA_MASTER_INSIZE];
                
                
                //Timeseries Processing helper functions//
                static int(GCDADupMasterModuleControlBlock::*gcda_read_function )();
                int read_by_sample();
                int read_by_packet_follow_rt();
                int read_by_packet_all();
                int update_start_position(TimeStamp&);
                int is_data_to_pack ();
                
};





#endif
