/** 
* @file
* @ingroup group_gcda
*/
// 
// File:   test.cc
// Author: ncss
//
// Created on June 13, 2007, 9:44 AM
//


/*
template <typename T>
class mmap_allocator {
    public:
        
        typedef T                 value_type;
        typedef value_type*       pointer;
        typedef const value_type* const_pointer;
        typedef value_type&       reference;
        typedef const value_type& const_reference;
        typedef std::size_t       size_type;
        typedef std::ptrdiff_t    difference_type;

        //rebind
        template <typename U>
        struct rebind {
            typedef mmap_allocator <U> other;
        };
        
        pointer address(reference value ) const {
            return &value;
        }
        const_pointer address(const_reference value) const {
            return &value;
        }
        
        mmap_allocator() throw () {}
        
        mmap_allocator( mmap_allocator const  &) throw () {}
        
        template <typename U>
        mmap_allocator(mmap_allocator <U> const &) throw () {}
        
        ~mmap_allocator() throw () {}
        
        size_type max_size() const throw() {
            return ::std::numeric_limits <size_type>::max() / sizeof(T);
        }
        
        pointer allocate(size_type num, void *  hint = 0) {
            pointer p = (pointer) ::mmap(hint, num, PROT_READ | PROT_WRITE, MAP_ANON, -1, 0);
            int * val = (int *)p;
            if(val && *val == -1)
                p = NULL;
            return p;
        }
        
        void construct(pointer p, const_reference value) {
            new((void *)p) T(value);  //placement new
        }
        
        void destroy(pointer p) {
            p->~T();
        }
        
        void deallocate(pointer p, size_type num) {
            ::munmap((caddr_t) p, num);
        }
        
};


template <typename T1, typename T2>
bool operator == (mmap_allocator <T1> const &, mmap_allocator <T2> const &) throw () {
    return true;
}

template <typename T1, typename T2>
bool operator != (mmap_allocator <T1> const &, mmap_allocator <T2> const &) throw () {
    return false;
}

*/
/*
template <class T>
class mmap_allocator {
    public:
        typedef T                 value_type;
        typedef value_type*       pointer;
        typedef const value_type* const_pointer;
        typedef value_type&       reference;
        typedef const value_type& const_reference;
        typedef std::size_t       size_type;
        typedef std::ptrdiff_t    difference_type;
        
        template <class U>
        struct rebind { typedef mmap_allocator<U> other; };
        
        mmap_allocator() throw () {}
        mmap_allocator(const mmap_allocator&) throw () {}
        template <class U>
        mmap_allocator(const mmap_allocator<U>&) throw () {}
        ~mmap_allocator() {}
        
        pointer address(reference x) const { return &x; }
        const_pointer address(const_reference x) const {
            return x;
        }
        //hint is a (int*) 
        pointer allocate(size_type n, mmap_allocator<void>::const_pointer hint  = 0);
        
        void deallocate(pointer p, size_type) { 
            ::munmap((caddr_t) p, num);
        } 
        
        size_type max_size() const {
            return ::std::numeric_limits <size_type>::max() / sizeof(T);
        }
        
        void construct(pointer p, const_reference x) {
            new ( p ) value_type(x); //palcement new 
        }
        
        void destroy(pointer p) { p->~value_type(); }
        
        private:
            void operator=(const mmap_allocator&);
};

template<> class mmap_allocator<void> {
    typedef void        value_type;
    typedef void*       pointer;
    typedef const void* const_pointer;
    
    template <class U>
    struct rebind { typedef mmap_allocator<U> other; };
};


template <class T>
inline bool operator==(const mmap_allocator<T>&, const mmap_allocator<T>&) {
    return true;
}

template <class T>
inline bool operator!=(const mmap_allocator<T>&, const mmap_allocator<T>&) {
    return false;
}

typedef mmap_allocator<pair<const nscl, struct timeval>, int > mmap_alloc;
typedef map <nscl, struct timeval, ltnscl, mmap_alloc > disk_map;


*/

#include <stdlib.h>
#include <iostream>
#include <strings.h>

#include "data_types.h"
#include "project_globals.h"
#include "TimeStamp.h" 
#include "Duration.h" 

using namespace std;

int 
main(int argc, char** argv) {
    timeval t ={0,0};
    TimeStamp zero (UNIX_TIME, t);
    TimeStamp temptime = zero + Duration((double)3.23);
    struct timespec sleeptime, slepttime;
    sleeptime.tv_sec = temptime.seconds(UNIX_TIME);
    sleeptime.tv_nsec = temptime.u_seconds()*1000;

    char buff[1000];
    bzero (buff, 1000);
    
    char buff2[1000];
    bzero (buff2, 1000);
    
    unsigned int i, j;
    
    //first seria of tests
    GCDA_data_type a, b;
    
    
    
    a.peak_acceleration.samples_after_start_time = 2;
    a.peak_acceleration.data_point =1.0;
    a.peak_acceleration.quality =8.0;
    
    a.peak_velocity.samples_after_start_time = 1;
    a.peak_velocity.data_point =1.0;
    a.peak_velocity.quality =1.0;
    
    a.flags = 1234;

    a.serialize(i, buff); 
    
    for (int k =0 ; k <1000; k++) {
        buff2[k] = buff[k];
    }
    
    b.deserialize (j, buff2);
    
    
    //second seria of tests 
    msg_struct s1, s2, r1, r2;    
    strcpy(s1.channel.network, (const char*) "BK"); 
    strcpy(s1.channel.station, (const char*) "BKS"); 
    strcpy(s1.channel.channel, (const char*) "HHZ"); 
    strcpy(s1.channel.location, (const char*) "--"); 
    s1.time = 123456789.777;
    s1.data = a;
    
    s2= s1;
    strcpy(s2.channel.network, (const char*) "GG"); 
    s1.serialize(i, buff);
    s2.serialize(i, buff+i);
    for (int k =0 ; k <1000; k++) {
        buff2[k] = buff[k];
    }
    r1.deserialize(j, buff2);
    r2.deserialize(j, buff2+j);
    return (EXIT_SUCCESS);
}

