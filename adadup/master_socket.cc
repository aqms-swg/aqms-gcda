/** 
* @file
* @ingroup group_gcda
*/

#include <iterator>
#include "master_socket.h"
#include "TimeStamp.h"
#include "Duration.h"
#include "seismic.h"
#include "wordorder.h"
#include "Compat.h"




// Static Vars

int GCDADupMasterModuleControlBlock::in_size =  GCDA_MASTER_INSIZE ;
class StatusManager GCDADupMasterModuleControlBlock::sm;
char  GCDADupMasterModuleControlBlock::start_position_dir [PATH_MAX];
char  GCDADupMasterModuleControlBlock::start_position_file [PATH_MAX];
class GCDADupMasterModuleControlBlock::SP  GCDADupMasterModuleControlBlock::SP_data;
int GCDADupMasterModuleControlBlock::gcda_read_mode = 0;
int (GCDADupMasterModuleControlBlock::* GCDADupMasterModuleControlBlock::gcda_read_function)() = &GCDADupMasterModuleControlBlock::read_by_sample;

class StatusManager GCDADupMasterMCBwriter::sm;
in_addr_t GCDADupMasterTransportWriter::saddress;
int GCDADupMasterTransportWriter::sport_pool_start ;


bool operator == (const struct nscl& c1, const struct nscl& c2) {
    if ((strcmp(c1.network, c2.network) == 0) && 
	(strcmp(c1.station, c2.station) == 0) && 
	(strcmp(c1.channel, c2.channel) == 0) &&
	(strcmp(c1.location, c2.location) == 0)) 
      return(true);
    return (false);
}

struct nscl c2n (const struct Channel& chan){
    struct nscl nscl_key;
    memset(&nscl_key, 0, sizeof(struct nscl));
    
    (void)strncpy(nscl_key.network, chan.network, MAX_CHARS_IN_NETWORK_STRING);
    (void)strncpy(nscl_key.station, chan.station, MAX_CHARS_IN_STATION_STRING);
    (void)strncpy(nscl_key.channel, chan.channel, MAX_CHARS_IN_CHANNEL_STRING);
    (void)strncpy(nscl_key.location, chan.location, MAX_CHARS_IN_LOCATION_STRING);
    return (nscl_key);
}


GCDADupMasterModuleControlBlock::SP::SP(){
    timemap = NULL;
    len = 0;
    initialized = false;
}

GCDADupMasterModuleControlBlock::SP::~SP(){
    shutdown();
}

void GCDADupMasterModuleControlBlock::SP::rawncpy(char* ptr, char* str, int n ){
    char buff [BUFSIZ];
    (void)memset(buff, ' ', n );
    (void)strncpy(buff, str, strlen (str) ); 
    buff[n-1]= '\n';
    (void)strncpy(ptr, buff, n ); 
}

void GCDADupMasterModuleControlBlock::SP::load(ChannelConfigList& cl){
    
    int fd = 0;
    len = sizeof ( disk_map ) * cl.size() ;

    TimeStamp tv = TimeStamp::current_time();
    
    if (len > 0 ) {
        if ((fd = open( (char*)start_position_file, O_RDWR, 0)) == -1 ) {
            try {
                if ((fd = open( (char*)start_position_file, O_RDWR | O_CREAT |O_TRUNC, 0644 )) != -1 ) {
                    // file the file with zeroes because of compability
                    if ( LOG_VERBOSE_LEVEL > 0 ) {
                        sm.InfoMessage(Compat::Form("(ADADup::ChanLoad): Start position: Create new map, Filename:%s",
                        (char*)&start_position_file ));
                    }
                    for (ChannelConfigList::iterator i = cl.begin(); i!= cl.end(); ++i) {
                        Channel chan = (*i).first ;
                        disk_map s;
                        char buff [BUFSIZ];
                        (void)memset(buff, ' ', sizeof (disk_map) );
                        sprintf((char*)s, "%s.%s.%s.%s:%u.%u%c", chan.network, chan.station, chan.channel, chan.location,
                        (uint32_t)tv.seconds(LEAPSECOND_TIME), tv.u_seconds(), '\0');
                        (void)strncpy(buff, s, strlen(s) );
                        buff[sizeof (disk_map)-1]= '\n';
                        if ( write(fd, buff, sizeof (disk_map)) != sizeof (disk_map) ) 
                            throw  ((int)errno);
                    }
                } else throw ((int)errno);
            } catch (int e) {
                if (fd > 0 ) (void ) close (fd); 
                sm.ErrorMessage(Compat::Form("(ADADup::ChanLoad): Start position: Error creating file: %s, Errno: %d ",
                (char*)&start_position_file, e ));
            }
        }
        
        if ( fd > 0 )  {
            //check filesize and append
            bool good_size = true;
            int wret;
            off_t fsize = lseek(fd, 0 , SEEK_END);
            if (fsize < len) {
                disk_map buff;
                (void)memset(buff, ' ', sizeof (disk_map) );
                for (; fsize < len ; fsize+= sizeof(disk_map) )
                    if (wret = write(fd, buff, sizeof(disk_map)  ) != sizeof (disk_map)  )
                        good_size = false;
            }

            void* ptr;
            if ( good_size )
            if (  (ptr = mmap( NULL, len , PROT_READ | PROT_WRITE, MAP_SHARED, fd , 0)  ) != MAP_FAILED ) {
                timemap =  reinterpret_cast<disk_map*> (ptr);
                //now check if we have all data in right status
                bool a_channel_not_found = false;
                for (ChannelConfigList::iterator i = cl.begin(); i!= cl.end(); ++i) {
                    Channel chan = (*i).first ;
                    
                    disk_map temp_buff;
                    disk_map * temp_ptr = timemap;
                    char * area = (char*)timemap;

                    sprintf( (char*)temp_buff, "%s.%s.%s.%s:%c", chan.network, chan.station, chan.channel, chan.location, '\0');
                    bool found = false ;
                    while ( (char*)temp_ptr < (area+len) ) {
                        if ( ! strncmp((char*)temp_ptr, (char*)temp_buff, strlen((char*)temp_buff)) ) {
                            struct timeval tv_tmp;
                            tv_tmp.tv_sec = (uint32_t) tv.seconds(LEAPSECOND_TIME);
                            tv_tmp.tv_usec = tv.u_seconds();
                            char *p, *pp  = strchr((char*)temp_ptr, ':' );
                            if ( sscanf( pp ,":%lu.%lu", &tv_tmp.tv_sec, &tv_tmp.tv_usec) == 2 ) {
                                disk_map buf;
                                sprintf(buf , "%u.%u%c", (unsigned) tv_tmp.tv_sec, (unsigned) tv_tmp.tv_usec, '\0');
                                if ( ( p = ( (*i).second ).config ) ) strcpy (p,(char*)buf);
                                found = true ;
                            }
                            break;  
                        }
                        temp_ptr++;
                    }
                    if (! found ) {
                        a_channel_not_found = true;
                        if ( LOG_VERBOSE_LEVEL > 1 ) {
                            TimeStamp ts(tv);
                            sm.InfoMessage(Compat::Form("(ADADup::ChanLoad): %s %s.%s.%s.%s, Set Start Time: %04d/%02d/%02d %02d:%02d:%02d.%03d ",
                            (char*) "Start position: Channel: ",
                            (char*) chan.network, (char*) chan.station, chan.channel, 
                            (char*) mapLC((char*) chan.network,(char*) chan.location,ASCII),
                            ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds() ) );
                        }
                        disk_map buf;
                        sprintf(buf , "%u.%u%c", (uint32_t)tv.seconds(LEAPSECOND_TIME), tv.u_seconds(), '\0');
                        char * p;
                        if ( ( p = ( (*i).second ).config ) ) strcpy(p, buf);
                    }
                }
                
                if (a_channel_not_found) {
                    //re-create file
                    disk_map * temp_ptr = timemap;
                    if ( LOG_VERBOSE_LEVEL > 1 ) {
                        sm.InfoMessage(Compat::Form("(ADADup::ChanLoad): Start position: Re-Create new map, Filename:%s", 
                        (char*)&start_position_file ));
                    }
                    for (ChannelConfigList::iterator i = cl.begin(); i!= cl.end(); ++i) {
                        Channel chan = (*i).first ;
                        ChannelConfig cfg =(*i).second ;
                        
                        disk_map s;
                        sprintf((char*)s, "%s.%s.%s.%s:%s%c",
                        chan.network, chan.station, chan.channel, chan.location, cfg.config, '\0'); 
                        
                        rawncpy((char*)temp_ptr,(char*) s, sizeof (disk_map) ); 
                        temp_ptr++;
                    }
                }
                initialized = true; 
            }
            
            if (fd >0 ) (void) close(fd);
        }
    }
    
}

bool GCDADupMasterModuleControlBlock::SP::operator!(){
    return (!initialized);
}

void GCDADupMasterModuleControlBlock::SP::shutdown(){
    if (initialized ) if ( timemap && (timemap != MAP_FAILED) ) {
        (void) ::munmap ( (char*) timemap , len); 
    }
    initialized = false;
}


int GCDADupMasterModuleControlBlock::SP::set_time (char *ptr ,const struct nscl& nscl_key, const struct TimeStamp& tv) {
    if (initialized ) {
        if ( ptr ) {
            disk_map temp_buff;
            sprintf( (char*)temp_buff, "%s.%s.%s.%s:%c", 
            nscl_key.network, nscl_key.station, nscl_key.channel, nscl_key.location, '\0');
            if ( ! strncmp((char*)ptr, (char*)temp_buff, strlen((char*)temp_buff)) ) {
                uint32_t m = tv.seconds(LEAPSECOND_TIME);
                sprintf((char*)temp_buff, "%s.%s.%s.%s:%u.%u%c",
                nscl_key.network, nscl_key.station, nscl_key.channel, nscl_key.location, (uint32_t) tv.seconds(LEAPSECOND_TIME), tv.u_seconds(), '\0');
                rawncpy((char*)ptr, (char*) temp_buff, sizeof (disk_map) );
                return (TN_SUCCESS);
            }        
        }
    }
    return (TN_FAILURE);
}

int GCDADupMasterModuleControlBlock::SP::set_time (char *ptr, const struct Channel& chan, const struct TimeStamp& tv) { 

    struct nscl nscl_key = c2n (chan) ;
    return ( set_time(ptr, nscl_key, tv) );
}


int  GCDADupMasterModuleControlBlock::SP::get_time (char *ptr ,const struct nscl& nscl_key, struct TimeStamp& time) {
    
    if (initialized ) {
        if ( ptr ) {
            struct timeval tv_tmp;
            disk_map temp_buff;
            sprintf( (char*)temp_buff, "%s.%s.%s.%s:%c",
            nscl_key.network, nscl_key.station, nscl_key.channel, nscl_key.location, '\0');
            if ( ! strncmp((char*)ptr, (char*)temp_buff, strlen((char*)temp_buff)) ) {
                char *p = strchr((char*)ptr, ':' );
                if ( sscanf( p , ":%lu.%lu", &tv_tmp.tv_sec, &tv_tmp.tv_usec) == 2 ) {
                    time = TimeStamp(LEAPSECOND_TIME,tv_tmp);
                    return (TN_SUCCESS);
                }
            }
        }
    }
    return (TN_FAILURE);
}


int GCDADupMasterModuleControlBlock::SP::get_time (char *ptr ,const struct Channel& chan, struct TimeStamp& time) { 
    
    struct nscl nscl_key = c2n (chan) ;
    return ( get_time(ptr, nscl_key, time) );
} 

//return pointer to position for specified channel in mapfile
char*  GCDADupMasterModuleControlBlock::SP::initialize_position (const struct Channel& chan) { 
    
    char * ret = NULL ;
    disk_map temp_buff;
    disk_map * temp_ptr = timemap;
    char * area = (char*)timemap;
    
    sprintf( (char*)temp_buff, "%s.%s.%s.%s:%c", chan.network, chan.station, chan.channel, chan.location, '\0');
    bool found = false ;
    while ( (char*)temp_ptr < (area+len) ) {
        if ( ! strncmp((char*)temp_ptr, (char*)temp_buff, strlen((char*)temp_buff)) ) {
            ret = (char*)temp_ptr ;
            break;
        }
        temp_ptr++ ;
    } 
    return ( ret ) ;
}
    
int set_nonblocking_mode (int s){
    int flags;

    if ((flags = fcntl (s, F_GETFL, 0)) < 0) {
        return (TN_FAILURE);
    }
#ifdef O_NONBLOCK
    flags |= (O_NONBLOCK) ;// POSIX
#else // O_NONBLOCK
#ifdef F_NDELAY
    flags |= (F_NDELAY);// BSD
#endif // F_NDELAY
#endif // !O_NONBLOCK
if (fcntl (s, F_SETFL, flags) < 0){
    return (TN_FAILURE);
}

return (TN_SUCCESS) ;
}

int set_blocking_mode (int s){
    int flags;

    if ((flags = fcntl (s, F_GETFL, 0)) < 0) {
        return (TN_FAILURE);
    }
#ifdef O_NONBLOCK
    flags &= ( ~ O_NONBLOCK); // POSIX
#else // O_NONBLOCK
#ifdef F_NDELAY
    flags &= F_NDELAY;// BSD
#endif // F_NDELAY
#endif // !O_NONBLOCK
if (fcntl (s, F_SETFL, flags ) < 0){
    return (TN_FAILURE);
}

return (TN_SUCCESS) ;
}

GCDADupMasterTransportWriter::GCDADupMasterTransportWriter (int num){
    
    
    
    // we increase port number for each thread
    // - so we will have one connection per thread
    //
    // each connection will have same IP Addr and different ports
    // starting from start_port_pool
    
    port = sport_pool_start + num;
    
    signal (SIGPIPE, SIG_IGN);  // make sure signal broken pipe ignored
    memset(&slave_ip4, 0,sizeof(slave_ip4));
    slave_ip4.sin_family = AF_INET;
    slave_ip4.sin_port   = htons ((unsigned short)port);
    slave_ip4.sin_addr.s_addr = saddress;
    
    sm.InfoMessage(Compat::Form("(ADADup::NetLoad): Loading Network Writer, address: %s:%d", inet_ntoa (slave_ip4.sin_addr),  port)); 
    
    socket_open = false;
    connected = false;
    postphoned = false;

    last_sent_time = TimeStamp::current_time();
    last_heartbeat_time = last_sent_time;
    start_connect_time = last_sent_time;
#ifdef SEND_GET_ACK        
    start_ackwait_time = last_sent_time;
#endif    
}

GCDADupMasterTransportWriter::~GCDADupMasterTransportWriter (){
    (void) writer_close ();
}

int GCDADupMasterTransportWriter::writer_close (){
    
    if (socket_open) {
        sm.InfoMessage(Compat::Form("(ADADup::Run): Shutdown Network Writer, address: %s:%d", inet_ntoa (slave_ip4.sin_addr),  port));
        (void) close (s);
    }
    socket_open = false;
    connected = false;
    postphoned = false;
    return TN_SUCCESS;
}


int GCDADupMasterTransportWriter::load (string& a,int p ){
    //static function  : we can use thread_unsafe  calls
    
    
    struct hostent *hp;
    //string ip = properties.getSlave_mashine_ip_address ();
    string ip = a;
    
    if (isalpha (ip[0])) {
        hp = gethostbyname (ip.c_str ());// probe name server by name
        if (hp == NULL) {
            sm.ErrorMessage("(ADADup::NetLoad): Bad slave IP address.");
            return (TN_FAILURE);
        }
        memcpy ((char*)&saddress, hp->h_addr, hp->h_length);
    }
    else {
        saddress = inet_addr (ip.c_str ());// convert digital IP to binary format
    }
    //saddress = inet_addr ( ip.c_str ());
    if ( saddress == -1 ) {
        sm.ErrorMessage("(ADADup::NetLoad): Bad slave IP address.");
        return (TN_FAILURE);
    }
    //sport_pool_start = properties.getSlave_mashine_ip_port ();
    sport_pool_start = p;
    return (TN_SUCCESS) ;
    
}

void GCDADupMasterTransportWriter::set_timeout_checktime (TimeStamp& time ){
    time = TimeStamp::current_time();
}

int GCDADupMasterTransportWriter::check_timeout (TimeStamp& time, double  interval ){
    // check if enough time left since last attempt to read
    // if time not exceed return TN_FAILURE
    TimeStamp curr_time = TimeStamp::current_time();
    double diff = (double) Duration (curr_time - time );
    double dperiod = (double) interval ;
    if (diff < dperiod){
        return(TN_FAILURE);
    }
    time = curr_time;
    
    return(TN_SUCCESS);
}


int  GCDADupMasterTransportWriter::writer_init () {
    
    if ( socket_open ) {
        return (TN_SUCCESS);
    }
    
    s = socket ( AF_INET, SOCK_STREAM, 0 );
    if ( s == -1 ) {
        return (TN_FAILURE);
    }
    
    // After that moment we already have socket opened
    // so we have to be sure in closing it
    socket_open = true;
    
#ifdef NON_BLOCKING_CONNECT
    if ( set_nonblocking_mode (s) == TN_FAILURE  ) {
        return (TN_FAILURE);
    }
#endif
    
    set_timeout_checktime (start_connect_time);
    
    return (TN_SUCCESS);
    
} // writer_init

int  GCDADupMasterTransportWriter::writer_connect () {
    
    connected = false;
    postphoned = false;
    
    int res = connect ( s, (struct sockaddr *) &slave_ip4, sizeof(slave_ip4) );
    if ( res == 0) {
         if ( LOG_VERBOSE_LEVEL > 0 ) {
             sm.InfoMessage (Compat::Form ("(ADADup::Run): Connected to slave GCDA: %s:%d",inet_ntoa (slave_ip4.sin_addr),port));
         }
        connected = true;
        (void)set_nonblocking_mode (s);
        set_timeout_checktime (last_heartbeat_time);
        return (TN_SUCCESS);
    }

    // some errors, check if we are in progress
    switch (errno) {
#ifdef EALREADY
        case EALREADY:
#endif
#ifdef EAGAIN
        case EAGAIN:
#endif
        case EINPROGRESS:
            // we are in progress
            // we asre waiting for actual connect - but this function should return true
            // we check for actual connect in select, later
            postphoned = true;
            if ( LOG_VERBOSE_LEVEL > 0 ) {
                sm.InfoMessage (Compat::Form ("(ADADup::Run): Connection postphoned to slave GCDA: %s:%d", inet_ntoa (slave_ip4.sin_addr), port));
            }
            break;
        case ECONNREFUSED:
            writer_wait (NET_CONNECT_WAIT, 0);
            if ( LOG_VERBOSE_LEVEL > 0 ) {
                sm.ErrorMessage (Compat::Form ("(ADADup::Run):Connection refused from Slave GCDA: %s:%d", inet_ntoa (slave_ip4.sin_addr), port));
            }
            break;
        case EEXIST:
            if ( LOG_VERBOSE_LEVEL > 1 ) {
                sm.InfoMessage ("(ADADup::Run):Switching into blocking mode.");
            }
            if (set_blocking_mode (s) == TN_FAILURE )  {
                if ( LOG_VERBOSE_LEVEL > 1 ) {
                    sm.InfoMessage ("(ADADup::Run):Problems to switch in block mode.");
                }
            }
            break;
        default:
            writer_wait (NET_CONNECT_WAIT, 0);
            if ( LOG_VERBOSE_LEVEL > 0 ) {
                sm.ErrorMessage (Compat::Form ("(ADADup::Run):Can not connect to slave GCDA: %s:%d", inet_ntoa (slave_ip4.sin_addr), port));
            }
    }
    if (postphoned) {
        (void)set_nonblocking_mode (s);
        return (TN_SUCCESS);
    }
    
    return (TN_FAILURE);
}


int  GCDADupMasterTransportWriter::try_to_write (int sec, int usec) {
    timeval tv;
    fd_set fdset;
    socklen_t optlen;
    int valopt;
    int res;
    
    // setup non-blocking select for write
    tv.tv_sec = sec;
    tv.tv_usec = usec;
    
    FD_ZERO (&fdset);
    FD_SET (s, &fdset);
    res = select (s+1, NULL, &fdset, NULL, &tv);
    if ( res == -1 )  { //((res == -1) && (errno != EINTR))
        //throw Error ("Error write  in select ");
        return (TN_FAILURE);
    }
    if (res == 0) {
        // select return  0 : not ready to write yet in this time
        return (TN_NODATA);
    }
    if (res > 0) {
        if (FD_ISSET (s, &fdset)) {
            // Socket selected for write
            optlen = sizeof(int);
            if (getsockopt (s, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &optlen) < 0) {
                //throw Error ("Was Error during write  in select ");
                return (TN_FAILURE);
            }
            switch ( valopt ) {
                case 0:
                    //return (TN_SUCCESS);
                    break;
#ifdef EHOSTUNREACH
                case EHOSTUNREACH:
#endif
#ifdef EHOSTDOWN
                case EHOSTDOWN:
#endif
#ifdef ETIMEDOUT
                case ETIMEDOUT:
#endif
                case ECONNREFUSED:
                    return (TN_NOTVALID);
#ifdef EALREADY
                case EALREADY:
#endif
#ifdef EAGAIN
                case EAGAIN:
#endif
                case EINPROGRESS:
                    return (TN_NODATA);
                default:
                    return (TN_FAILURE);
            }
            // looks like OK
            return (TN_SUCCESS);
        }
    }
    return (TN_FAILURE);
}

int  GCDADupMasterTransportWriter::try_to_connect (){
    int result;
    
    if (  connected) {
        return TN_SUCCESS;
    }
    
    if (  postphoned ) {
        result = try_to_write (0, 0);
        switch ( result ) {
            case TN_SUCCESS:
                if ( LOG_VERBOSE_LEVEL > 0 ) {
                    sm.InfoMessage (Compat::Form ("(ADADup::Run): Connected to slave GCDA: %s:%d", inet_ntoa (slave_ip4.sin_addr), port));
                }
                postphoned = false;
                connected = true;
                set_timeout_checktime (last_heartbeat_time);
                //return (TN_SUCCESS);
                break;
            case TN_NODATA:
            case TN_NOTVALID:
                if (check_timeout (start_connect_time, NET_CONNECT_TIMEOUT ) == TN_SUCCESS) {
                    return (TN_FAILURE);
                }
                //return (TN_SUCCESS);
                break;
            default :
                return (TN_FAILURE);
        }
        // even if we still not connected - we expect would write in next time
        // but we need to return seccess i order to avoid disconnent
        return (TN_SUCCESS);
    }
    
    if ( writer_init () == TN_FAILURE ) {
        return (TN_FAILURE);
    }
    
    if ( ( result = writer_connect () ) == TN_FAILURE ) {
        if (check_timeout (start_connect_time, NET_CONNECT_TIMEOUT ) == TN_SUCCESS) {
            return (TN_FAILURE);
        }
    }
    //all done. we connected OK
    return (TN_SUCCESS);
}

void GCDADupMasterTransportWriter::writer_wait (unsigned int sec, unsigned int msec) {
    struct timespec sleeptime, slepttime;
    sleeptime.tv_sec = sec;
    sleeptime.tv_nsec = 1000000 * msec;  //1/100 sec
    if ( LOG_VERBOSE_LEVEL > 3 ) {
        sm.InfoMessage(Compat::Form("(ADADup::Run): Writer sleeping [%u:%u]", sleeptime.tv_sec, sleeptime.tv_nsec ) );
    }
    nanosleep (&sleeptime, &slepttime);
}

int GCDADupMasterTransportWriter::raw_write ( int& bytes_left, char* ptr) {
    while (  bytes_left > 0 ) {
        if ( ! no_signal ) { break; }
        int status = try_to_write (0, 0); //non block try
        switch (status ) {
            case TN_SUCCESS :
                int ret; 
                ret = send ( s, ptr , bytes_left, 0 );
                if (ret == 0 ) {
                    if ( LOG_VERBOSE_LEVEL > 0 ) {
                        sm.ErrorMessage (Compat::Form ("(ADADup::Run):Socket error: last time sent %d bytes.", ret));
                    }
                    return (TN_FAILURE);
                }
                if (ret == -1) {
                    switch (errno) {
                        case EWOULDBLOCK :
                            writer_wait (0, NET_WOULDBLOCK_WAIT);
                        case EINTR:
                            continue;
                        default :
                            if ( LOG_VERBOSE_LEVEL > 0 ) {
                                sm.ErrorMessage (Compat::Form ("(ADADup::Run):Socket error, errno: %d", errno ));
                            }
                            return (TN_FAILURE);
                    }
                } else  {
                    ptr += ret;
                    bytes_left -= ret;
                }
                break;
            case TN_FAILURE:
                return (TN_FAILURE);
            default:
                //we should wait until network ready
                writer_wait (0, NET_WOULDBLOCK_WAIT);
        }
    }
    return (TN_SUCCESS);
}

int GCDADupMasterTransportWriter::write_data( ) {
    
    int ret = TN_FAILURE;
    if ( LOG_VERBOSE_LEVEL > 1 ) {
        sm.InfoMessage("(ADADup::Run): Network: Start sending...");
    }

    while ( (ret = write_data_from_buffer() ) != TN_SUCCESS ) {
        if ( ! no_signal ) { break; }
        
        if (check_timeout( last_sent_time, double(MAX_SECONDS_BEHIND_REALTIME) ) == TN_SUCCESS ) {
            //time to send has exceed
            // we need to give up
            if ( LOG_VERBOSE_LEVEL > 2 ) {
                sm.InfoMessage(Compat::Form ("(ADADup::Run): Network: Time to send left, max_sec_behind_rt: %d", MAX_SECONDS_BEHIND_REALTIME));
            }
            ret = TN_FAILURE;
            break;
        }
        writer_wait(0, NET_WOULDBLOCK_WAIT);
            if ( LOG_VERBOSE_LEVEL > 2 ) {
                sm.InfoMessage("(ADADup::Run): Network: Re-try");
            }

    }
    if ( LOG_VERBOSE_LEVEL > 1 ) {
        sm.InfoMessage("(ADADup::Run): Network: Stop sending...");
    }
    //make sure to restart the heartbeat's timer
    set_timeout_checktime  ( last_heartbeat_time );
    
    return (ret);
}

int GCDADupMasterTransportWriter::write_data_rest() {

#ifndef FLOATING_BUFFER_SIZE     
    //if not floating_buffer 
    if (check_timeout ( last_heartbeat_time, NET_HEARTBEAT_INTERVAL ) == TN_FAILURE ) {
        return (TN_NODATA);
    }
    if ( LOG_VERBOSE_LEVEL > 1 ) {
        sm.InfoMessage("(ADADup::Run): Network: Start sending the rest of the data ...");
    }
#endif        
    return ( write_data() );
}
    
 
int GCDADupMasterTransportWriter::write_data_from_buffer () {
    
    if ( ! connected) {
        writer_wait (0, NET_WOULDBLOCK_WAIT); // wait a little before trying to connect
        if ( try_to_connect () == TN_FAILURE) {
            (void) writer_close ();
            writer_wait (NET_CONNECT_WAIT, 0);
        }
    }
    
    if ( connected  ) {
        //we assume we already have a valid packet
#ifdef FLOATING_BUFFER_SIZE  
        int bytes_left = npacket * serialized_size + serialized_header_size;
#else        
        int bytes_left = network_packet_size; //sizeof(sock_msg_packet);
#endif
        // explicite header write befor use
        pack_header();
        char * ptr = (char*) &mess.area;
        if ( LOG_VERBOSE_LEVEL > 2 ) {
            sm.InfoMessage("(ADADup::Run): Network: Start Raw Write");
        }
        if ( raw_write ( bytes_left, ptr) == TN_FAILURE)  {
            (void) writer_close ();
            writer_wait (NET_CONNECT_WAIT, 0);
            return (TN_FAILURE);
        }
        
        if ( bytes_left == 0 ) {
            
#ifdef SEND_GET_ACK
            if ( LOG_VERBOSE_LEVEL > 2 ) {
                sm.InfoMessage("(ADADup::Run): Network: Start Waiting for ACK");
            }

            if (wait_for_ack () == TN_FAILURE)  {
                if ( LOG_VERBOSE_LEVEL > 0 ) {
                   sm.ErrorMessage ("(ADADup::Run): No ACK received.");
                }
                (void) writer_close ();
                writer_wait (NET_CONNECT_WAIT, 0);
                return (TN_FAILURE);
            }
            if ( LOG_VERBOSE_LEVEL > 2 ) {
                sm.InfoMessage("(ADADup::Run): Network: Received ACK");
            }
#endif
            //make sure to restart the last_sent timer
            set_timeout_checktime  ( last_sent_time );
            return (TN_SUCCESS);
        }
    }
    
    return (TN_FAILURE);
}

int GCDADupMasterTransportWriter::write_heartbeat ( ) {
    
    if (check_timeout (last_heartbeat_time, NET_HEARTBEAT_INTERVAL ) == TN_FAILURE ) {
        return (TN_FAILURE);
    }
    
    return ( write_data() );
}


int GCDADupMasterTransportWriter::wait_for_ack ()  {
    timeval tv;
    fd_set fdset;
    socklen_t optlen;
    int valopt;
    int res;
    
    char rmsg[4], msg[4]= {'A', 'C', 'K', 0};
    char  *ptr = rmsg, *standard = msg;
    int bytes_left = 4;
    set_timeout_checktime (start_ackwait_time);
    while (  bytes_left > 0 ) {
        //make sure we have exit from infinitive loop
        if ( ! no_signal ) { break; }
        
        tv.tv_sec =  0; //NET_ASK_TIMEOUT;   //wait up to 5sec
        tv.tv_usec = 0;
        
        FD_ZERO (&fdset);
        FD_SET (s, &fdset);
        //select socket for read
        res = select (s+1, &fdset, NULL, NULL, &tv);
        if ((res == -1) && (errno != EINTR)) {
            //throw Error ("Error in select ");
            return (TN_FAILURE);
        }
        if (res == 0) {  //time left and we got nothing
            if (check_timeout (start_ackwait_time, double(NET_ACK_TIMEOUT) ) == TN_SUCCESS ) {
                if ( LOG_VERBOSE_LEVEL > 2 ) {
                    sm.ErrorMessage(Compat::Form("(ADADup::Run): Network: Error getting ACK, timeout expired"));
                }
                return (TN_FAILURE);
            }
            writer_wait (0, NET_WOULDBLOCK_WAIT);
        }
        if (res > 0) {
            if (FD_ISSET (s, &fdset)) {
                // Socket selected for read
                optlen = sizeof(int);
                if (getsockopt (s, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &optlen) < 0) {
                    //throw Error ("Receiving ACK: Was Error connecting in select ");
                    return (TN_FAILURE);
                }
                // Check the value returned...
                if (valopt) {
                    //throw Error ("Receiving ACK: Returned Error connecting in select ");
                    if ( LOG_VERBOSE_LEVEL > 2 ) {
                        sm.ErrorMessage(Compat::Form ("(ADADup::Run): Network: Error getting ACK, opt %d", valopt));
                    }

                    return (TN_FAILURE);
                }
                
                int ret;
                ret =recv ( s, ptr , bytes_left, 0 );
                if (ret == 0 ) {  return (TN_FAILURE); } //throw Error ( "receive ACK(0) error:" ); }
                if (ret == -1) {
                    switch (errno) {
                        case EWOULDBLOCK : { writer_wait (0, NET_WOULDBLOCK_WAIT); continue;}
                        default          : { return (TN_FAILURE);  } //throw Error ( "receive ACK() error:" );
                    }
                }
                ptr += ret;
                bytes_left -= ret;
                
            } else {
                writer_wait (0, NET_WOULDBLOCK_WAIT); //continue with small wait
            }
        }
    }
    //ok no more errors
    if (strncmp (rmsg, standard, 3)== 0) {
        return (TN_SUCCESS);
    }
    return (TN_FAILURE);
    
    
    
    
}



//
int GCDADupMasterMCBwriter::pack_data (
    unsigned int &pnum , unsigned int num, Channel &channel, GCDA_data_type* data , TimeStamp * time ) {
    
    int ret = TN_FAILURE;
    if ( npacket < 0  )  {
        return (TN_BEGIN);
    }
    if ( npacket >= max_packet_size  )  {
        return (TN_END);
    }
    
    char* buff_ptr;
    size_t j;
    for (int i = 0 ; (i < num) && (npacket < max_packet_size); npacket++ , i++) {
        msg_struct m;
        m.time = time[i].ts_as_double(LEAPSECOND_TIME);
        m.data = data[i];
        m.channel = channel;

        //byte order
        wordorder_master(m.time);
        m.data.wordorder_master();
        
        //serialization
        buff_ptr = &mess.message.first_msg_char + npacket * serialized_size;
        m.serialize (j, buff_ptr );
        if (j != serialized_size ) ; // assert error later
        
        pnum = i + 1;
        ret = TN_SUCCESS;
    }
    pack_header();
    return (ret);
    
}

void GCDADupMasterMCBwriter::print_pack() {
    
    char* buff_ptr = &mess.message.first_msg_char;
    struct msg_struct *msg_ptr= reinterpret_cast<msg_struct*> (buff_ptr);
    double time;
    GCDA_data_type data;
    nscl channel ;
    char tmp_loc[3];
    
    for (int i = 0 ; i < npacket; i++) {
        time = msg_ptr[i].time ;
        data = msg_ptr[i].data ;
        channel =  msg_ptr[i].channel ;
        
        wordorder_slave(time);
        data.wordorder_slave();
        
        strcpy(tmp_loc, channel.location);
        mapLC((char*)channel.network, tmp_loc, ASCII);
        TimeStamp ts(LEAPSECOND_TIME, time);
        
        sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, Time: %04d/%02d/%02d %02d:%02d:%02d.%03d, ",
        (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
        ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds() ));
    }
    
}


GCDADupMasterModuleControlBlock::GCDADupMasterModuleControlBlock (Channel chn,int mode1){
    channel = chn;
    strcpy(tmp_loc, chn.location);
    mapLC((char*)chn.network, tmp_loc, ASCII);
    
    reader = NULL;
    monitor = NULL;
    SP_channel_ptr = NULL;
    
    TimeStamp zero = TimeStamp::current_time();
    last_retrieved_sample_time = zero;
    start_timestamp = zero;
    start_position_time= zero;
    mode = false;
    last_access_time = zero;
    nsamples_extracted = 0;
    nsamples_processed = 0;
    nsamples_packed =0;
    last_read_gap = true;
    usec_per_sample = 1; //assume at least small portion of sample
    
    // count read_size to fill up to Max-buffer_lenght
    read_size =  ( NET_MAX_BUFFER_SIZE - sizeof(msg_header_struct) ) / sizeof (msg_struct);
    //int n = int(channel.samprate);
    read_size = (read_size < in_size )? read_size : in_size;
    
    
    //Initialize input and output arrays//
    memset(data_buff, 0,in_size*sizeof(GCDA_data_type));
    
    
    //Initialize per-channel datastructures here//
    Channel chan (chn);
    
    sm.InfoMessage(Compat::Form("(ADADup::ChanLoad): Loading parameters, Channel: %s.%s.%s.%s, Samples in buff: %d ", 
    (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc, read_size )); 
    
    mode = false;
    start_position_time= TimeStamp::current_time();
    
    if (mode1 == 1) {
#ifdef STARTPOSITION_MMAP
        mode = true;
        TimeStamp tt = zero;
        SP_channel_ptr = GCDADupMasterModuleControlBlock::SP_data.initialize_position (chn); 
        if (GCDADupMasterModuleControlBlock::SP_data.get_time (SP_channel_ptr, chn, tt) == TN_SUCCESS )  {
            start_position_time = tt ;
        }
#else        
        mode = true;
        //open file and read last access time
        string fpath= start_position_dir;
        string fname;
        (void)fname.append ((char*)chan.network);
        fname.push_back ('.');
        (void)fname.append ((char*)chan.station);
        fname.push_back ('.');
        mapLC (chan.network, chan.location, ASCII);
        (void)fname.append ((char*)chan.location);
        fname.push_back ('.');
        (void)fname.append ((char*)chan.channel);
        
        char trail_char = '/' ;
        char trail = fpath[fpath.size ()-1];
        
        if (trail == trail_char ) {
            fpath.append (fname);
        } else {
            fpath.push_back (trail_char);
            fpath.append (fname);
        }
        memset(&start_position_file_path, 0,PATH_MAX);
        int length = fpath.size ();
        length = ( length <PATH_MAX )? length : PATH_MAX;
        strncpy ((char*)&start_position_file_path, (char*)fpath.c_str (), length);
        start_position_file_path[length] = 0;
        sm.InfoMessage(Compat::Form("(ADADup::ChanLoad): Start position set, Channel: %s.%s.%s.%s, filename: %s ", 
        (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc, (char*)&start_position_file_path )); 
    
        FILE *fp;
        if (fp = fopen ((char*)&start_position_file_path, "r")){
            try {
                uint32_t sec=0, usec=0;
                int res=fscanf (fp, "%u %u", &sec, &usec);
                if ((res==2) && (sec>0)) {
                    struct timeval tt;
                    tt.tv_sec = sec;
                    tt.tv_usec = usec;
                    
                    start_position_time = TimeStamp(LEAPSECOND_TIME, tt);
                }
            } catch (...) {
                start_position_time = TimeStamp::current_time();
            }
            fclose (fp);
        }
#endif        
    }
    
    
    
}

GCDADupMasterModuleControlBlock::~GCDADupMasterModuleControlBlock (){
    if(reader)
        delete reader;
    if(monitor)
        delete monitor;
    
}


int GCDADupMasterModuleControlBlock::is_data_to_pack () {
    
    if  ( (nsamples_extracted > 0) && 
          (nsamples_packed < nsamples_extracted) ) {
        return (TN_TRUE);
    } 
    return (TN_FALSE);
}


int GCDADupMasterModuleControlBlock::read_by_sample () {
    
    bool gap_was_found = false;
    int i, gap, res;
    
    nsamples_extracted = nsamples_packed = 0; //very important set
    
    for(i=1; i < read_size; i++) {  // we starts from 1 intensionnalyy
        if ( (res = reader->Get_Next_Sample(times_buff[i] , data_buff[i], gap) != TN_SUCCESS ) )
            break;
        nsamples_extracted ++ ;
        if (gap) gap_was_found = true;
    }
    
    if (nsamples_extracted >0 ) {
        if ( LOG_VERBOSE_LEVEL > 1 ) {
            
            TimeStamp ts(times_buff[1]);
            TimeStamp es(times_buff[nsamples_extracted]);
            
            sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, StartTime: %04d/%02d/%02d %02d:%02d:%02d.%03d, EndTime: %04d/%02d/%02d %02d:%02d:%02d.%03d, Samples: %3d, Gap %d, Read: OK ",
            (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
            ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
            es.year(), es.month_of_year(), es.day_of_month(), es.hour_of_day(), es.minute_of_hour(), es.second_of_minute(), es.milliseconds(),
            nsamples_extracted, gap
            ));
        }
        

        return (TN_TRUE);
    }
    
    return(TN_FALSE);
}


void normalize_tval (struct timeval& this_tval) {
    uint32_t secs_in_usec_field = this_tval.tv_usec / USECS_PER_SECOND;
    if (secs_in_usec_field > 0) {
        int usecs_remaining = this_tval.tv_usec % USECS_PER_SECOND;
        this_tval.tv_sec  = this_tval.tv_sec + secs_in_usec_field;
        this_tval.tv_usec    = usecs_remaining;
    }
}

int GCDADupMasterModuleControlBlock::read_by_packet_all() {
    
    int number_of_samples;
    bool gap_was_found = false;
    
    nsamples_extracted = nsamples_packed = 0; //very important set
    
    //Just call Get All samples before checking Oldest/Newest Time in CDA
    //we do not want unnessesery lock GCDA, while geting values above.
    //It affects on performance
    TimeStamp begin_timestamp(this->last_retrieved_sample_time);
    TimeStamp read_time (this->last_retrieved_sample_time);
    
    if ( reader->Get_All_Available_Samples(read_time, read_size,
    number_of_samples, times_buff , data_buff) == TN_SUCCESS ){
        //we started from previose position
        //so success if we got more then 1 sample (because 1st sample was retrieved last time )
        if (number_of_samples > 1 ) {
            //read_successful = true;
            //in this case we do not care continouse of data - by the way :most common case
            nsamples_extracted = number_of_samples -1;
            this->last_retrieved_sample_time = times_buff[number_of_samples - 1];

            gap_was_found = last_read_gap ;
            this->last_read_gap = false ;
            
            if ( LOG_VERBOSE_LEVEL > 1 ) {
                
                TimeStamp ts(times_buff[1]);
                TimeStamp es(times_buff[nsamples_extracted]);
                
                sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, StartTime: %04d/%02d/%02d %02d:%02d:%02d.%03d, EndTime: %04d/%02d/%02d %02d:%02d:%02d.%03d, Samples: %3d, Gap %d, Read: OK ",
                (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
                ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
                es.year(), es.month_of_year(), es.day_of_month(), es.hour_of_day(), es.minute_of_hour(), es.second_of_minute(), es.milliseconds(),
                nsamples_extracted, gap_was_found
                ));
            }

            return (TN_TRUE);
        }
        if ( number_of_samples  == 1 ) { // no new data - data has no gaps
            nsamples_extracted = 0;
        }
    } else {
        // we can not read from CDA for specified timestamp
        // that meens we out of GCDA range or gap:  lets check CDAtime boundaries
        
        // somthing wrong againg, may be time pointer went far ahead ? because of wrong config file
        begin_timestamp = TimeStamp(this->last_retrieved_sample_time);
        TimeStamp newest_gcda_timestamp = monitor->time_of_newest_sample_in_cda();
        if (begin_timestamp == newest_gcda_timestamp) {
            //we have no new data ; skip to next iteration
            ;
            
        } else {
            if (begin_timestamp > newest_gcda_timestamp) {
                
                //if we reach this  - something realy wrong with data (it was changed back)or
                // we've been sending wrong timestamped data or ...?
                this->last_retrieved_sample_time = newest_gcda_timestamp;
                this->last_read_gap = true ;
                if ( LOG_VERBOSE_LEVEL > 1 ) {
                    TimeStamp ts(begin_timestamp);
                    TimeStamp es(last_retrieved_sample_time);
                    
                    sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, Adjust Time: From: %04d/%02d/%02d %02d:%02d:%02d.%03d, To: %04d/%02d/%02d %02d:%02d:%02d.%03d, Rule: Move Back ",
                    (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
                    ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
                    es.year(), es.month_of_year(), es.day_of_month(), es.hour_of_day(), es.minute_of_hour(), es.second_of_minute(), es.milliseconds() ));
                }
                
            } else  {
                
                TimeStamp oldest_gcda_time = monitor->time_of_oldest_sample_in_cda();
                TimeStamp oldest_gcda_timestamp(oldest_gcda_time);
                
                if (begin_timestamp < oldest_gcda_timestamp) {
                    //we were not able to deliver data to slavegcda, time and data in mastergcda has gone away GCDA
                    this->last_retrieved_sample_time = oldest_gcda_time;
                    this->last_read_gap = true ;
                    if ( LOG_VERBOSE_LEVEL > 1 ) {
                        TimeStamp ts(begin_timestamp);
                        TimeStamp es(last_retrieved_sample_time);
                        
                        sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, Adjust Time: From: %04d/%02d/%02d %02d:%02d:%02d.%03d, To: %04d/%02d/%02d %02d:%02d:%02d.%03d, Rule: Data Left ",
                        (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
                        ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
                        es.year(), es.month_of_year(), es.day_of_month(), es.hour_of_day(), es.minute_of_hour(), es.second_of_minute(), es.milliseconds() ));
                    }
                    
                    //here we need to have big enough internal buffer in order we dont want to keep continuously loosing data
                    //because we are not able to read fast enough to get data from the end. (assuming GCDA_READ_INTIME_PERIOD too long)
                    
                }    else  {
                    //begin_timestamp>=oldest_gcda_timestamp
                    //other interesting case when we between boundaries in the middle
                    //but all data for requested time is invalid
                    //it can happen bacause our buffer small and gap so big
                    //move pointer ahead up to curr position + buffer lenght
//                    struct timeval t_int = {0, usec_per_sample * read_size};
//                    normalize_tval (t_int);
//                    begin_timestamp += Duration(t_int);
                     double buffsize_sec = double(usec_per_sample * read_size)/double( USECS_PER_SECOND);
                     begin_timestamp += Duration(buffsize_sec);
                  
                    if (begin_timestamp < newest_gcda_timestamp) {
                        this->last_retrieved_sample_time = begin_timestamp;
                    } else {
                        //all data  markered as bad
                        this->last_retrieved_sample_time = newest_gcda_timestamp;
                    }
                    this->last_read_gap = true;
                    if ( LOG_VERBOSE_LEVEL > 1 ) {
                        TimeStamp ts(begin_timestamp);
                        TimeStamp es(last_retrieved_sample_time);
                        
                        sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, Adjust Time: From: %04d/%02d/%02d %02d:%02d:%02d.%03d, To: %04d/%02d/%02d %02d:%02d:%02d.%03d, Rule: Long Gap ",
                        (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
                        ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
                        es.year(), es.month_of_year(), es.day_of_month(), es.hour_of_day(), es.minute_of_hour(), es.second_of_minute(), es.milliseconds() ));
                    }
                }
                
            }
        }
    }
    
    return(TN_FALSE);
}

int GCDADupMasterModuleControlBlock::read_by_packet_follow_rt() {
    
    int number_of_samples;
    bool gap_was_found = false;
    
    nsamples_extracted = nsamples_packed = 0; //very important set
    
    //Just call Get All samples before checking Oldest/Newest Time in CDA
    //we do not want unnessesery lock GCDA, while geting values above.
    //It affects on performance
    TimeStamp begin_timestamp(this->last_retrieved_sample_time);
    TimeStamp newest_gcda_timestamp = TimeStamp::current_time();;
    TimeStamp follow_rt_timestamp = TimeStamp::current_time();
    
    follow_rt_timestamp -= Duration(double(MAX_SECONDS_BEHIND_REALTIME));
    if (begin_timestamp < follow_rt_timestamp) {
        gap_was_found = true ;
        this->last_retrieved_sample_time = follow_rt_timestamp;
        this->last_read_gap = true;
        
        if ( LOG_VERBOSE_LEVEL > 2 ) {
            TimeStamp ts(begin_timestamp);
            TimeStamp es(last_retrieved_sample_time);
            
            sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, Adjust Time: From: %04d/%02d/%02d %02d:%02d:%02d.%03d, To: %04d/%02d/%02d %02d:%02d:%02d.%03d, Rule: Follow RT ",
                    (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
                    ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
                    es.year(), es.month_of_year(), es.day_of_month(), es.hour_of_day(), es.minute_of_hour(), es.second_of_minute(), es.milliseconds() ));
        }
    }

    TimeStamp read_time = this->last_retrieved_sample_time;
    if ( reader->Get_All_Available_Samples(read_time, read_size,
    number_of_samples, times_buff , data_buff) == TN_SUCCESS ){
        if (number_of_samples > 1 ) {
            nsamples_extracted = number_of_samples -1;
            this->last_retrieved_sample_time = times_buff[number_of_samples - 1];
            //check for gap here
            gap_was_found = last_read_gap ;
            this->last_read_gap = false ;
            //print here
            
            if ( LOG_VERBOSE_LEVEL > 1 ) {
                
                TimeStamp ts(times_buff[1]);
                TimeStamp es(times_buff[nsamples_extracted]);
                
                sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, StartTime: %04d/%02d/%02d %02d:%02d:%02d.%03d, EndTime: %04d/%02d/%02d %02d:%02d:%02d.%03d, Samples: %3d, Gap %d, Read: OK ",
                (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
                ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
                es.year(), es.month_of_year(), es.day_of_month(), es.hour_of_day(), es.minute_of_hour(), es.second_of_minute(), es.milliseconds(),
                nsamples_extracted, gap_was_found
                ));
            }
            
            return (TN_TRUE);
        }
        if ( number_of_samples  == 1 ) { // no new data - data has no gaps
            nsamples_extracted = 0;
        }
    } else {
        // we can not read from CDA for specified timestamp
        // that meens we out of GCDA range or gap:  lets check CDAtime boundaries
        
        // somthing wrong againg, may be time pointer went far ahead ? because of wrong config file
        TimeStamp newest_gcda_timestamp = monitor->time_of_newest_sample_in_cda();
        if (begin_timestamp == newest_gcda_timestamp) {
            ; //no new data
        } else {
            if (begin_timestamp > newest_gcda_timestamp) {
                if ( newest_gcda_timestamp < follow_rt_timestamp) {
                    ; //no new data 
                    //probably we have moved  time pointer with follow realtime rule .
                } else {
                    //if we reach this  - something realy wrong with data (it was changed back)or
                    // we've been sending wrong timestamped data or ...?
                    this->last_retrieved_sample_time = newest_gcda_timestamp;
                    this->last_read_gap = true;
                    if ( LOG_VERBOSE_LEVEL > 2 ) {
                        TimeStamp ts(begin_timestamp);
                        TimeStamp es(last_retrieved_sample_time);
                        
                        sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, Adjust Time: From: %04d/%02d/%02d %02d:%02d:%02d.%03d, To: %04d/%02d/%02d %02d:%02d:%02d.%03d, Rule: Move Back ",
                        (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
                        ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
                        es.year(), es.month_of_year(), es.day_of_month(), es.hour_of_day(), es.minute_of_hour(), es.second_of_minute(), es.milliseconds() ));
                    }
                }
            } else  {
                
                TimeStamp oldest_gcda_time = monitor->time_of_oldest_sample_in_cda();
                TimeStamp oldest_gcda_timestamp(oldest_gcda_time);
                
                if (begin_timestamp < oldest_gcda_timestamp) {
                    //we were not able to deliver data to slavegcda, time and data in mastergcda has gone away from GCDA
                    this->last_retrieved_sample_time = oldest_gcda_time;
                    this->last_read_gap = true;
                    if ( LOG_VERBOSE_LEVEL > 2 ) {
                        TimeStamp ts(begin_timestamp);
                        TimeStamp es(last_retrieved_sample_time);
                        
                        sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, Adjust Time: From: %04d/%02d/%02d %02d:%02d:%02d.%03d, To: %04d/%02d/%02d %02d:%02d:%02d.%03d, Rule: Data Left ",
                        (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
                        ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
                        es.year(), es.month_of_year(), es.day_of_month(), es.hour_of_day(), es.minute_of_hour(), es.second_of_minute(), es.milliseconds() ));
                    }
                    // !!! important
                    //here we need to have big enough internal buffer in order we dont want to keep continuously loosing data
                    //because we are not able to read fast enough to get data from the end. (assuming GCDA_READ_INTIME_PERIOD too long)
                }    else  {
                    //begin_timestamp>=oldest_gcda_timestamp
                    //other interesting case when we between boundaries in the middle
                    //but all data for requested time is invalid
                    //it can happen bacause our buffer small and gap so big
                    //move pointer ahead up to curr position + buffer lenght
//                    struct timeval t_int = {0, usec_per_sample * read_size};
//                    normalize_tval (t_int);
//                    begin_timestamp += Duration(t_int);
                    double buffsize_sec = double(usec_per_sample * read_size)/double( USECS_PER_SECOND);
                    begin_timestamp += Duration(buffsize_sec);
                    if (begin_timestamp < newest_gcda_timestamp) {
                        this->last_retrieved_sample_time = begin_timestamp;
                    } else {
                        //all data  markered as bad
                        this->last_retrieved_sample_time = newest_gcda_timestamp;
                    }
                    this->last_read_gap = true;
                    if ( LOG_VERBOSE_LEVEL > 1 ) {
                        TimeStamp ts(begin_timestamp);
                        TimeStamp es(last_retrieved_sample_time);
                        
                        sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s, Adjust Time: From: %04d/%02d/%02d %02d:%02d:%02d.%03d, To: %04d/%02d/%02d %02d:%02d:%02d.%03d, Rule: Long Gap ",
                        (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
                        ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds(),
                        es.year(), es.month_of_year(), es.day_of_month(), es.hour_of_day(), es.minute_of_hour(), es.second_of_minute(), es.milliseconds() ));
                    }
                    
                }
                
            }
        }
    }
    
    return(TN_FALSE);
}




int  GCDADupMasterModuleControlBlock::update_start_position (TimeStamp& time) {
#ifdef STARTPOSITION_MMAP
    int ret;
    if ( (ret = GCDADupMasterModuleControlBlock::SP_data.set_time( SP_channel_ptr, channel, time)) == TN_SUCCESS ) {
        if ( LOG_VERBOSE_LEVEL > 1 ) {
            TimeStamp ts(time);
            sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s,  NextTime: %04d/%02d/%02d %02d:%02d:%02d.%03d, StartPosition Update: OK ",
            (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
            ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds()  ));
        }
    } else {
        if ( LOG_VERBOSE_LEVEL > 1 ) {
            sm.ErrorMessage(Compat::Form("(ADADup::Run): StartPosition: Problem updating start position, Channel: %s.%s.%s.%s, Filename: %s ",
            (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc, (char*)&start_position_file ));
        }
    }

    return ( ret );
#else     
    FILE *fp = NULL;
    try {
        if (fp = fopen ((char*)&start_position_file_path, "w")){
            fprintf (fp, "%u %u\r", time.seconds(LEAPSECOND_TIME), time.u_seconds());
            fclose (fp);
            if ( LOG_VERBOSE_LEVEL > 1 ) {
                TimeStamp ts(time);
                sm.InfoMessage(Compat::Form("(ADADup::Run): Channel %s.%s.%s.%s,  NextTime: %04d/%02d/%02d %02d:%02d:%02d.%03d, StartPosition Update: OK ",
                (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc,
                ts.year(), ts.month_of_year(), ts.day_of_month(), ts.hour_of_day(), ts.minute_of_hour(), ts.second_of_minute(), ts.milliseconds()  ));
            }

        } else {
            if ( LOG_VERBOSE_LEVEL > 0 ) {
                sm.ErrorMessage(Compat::Form("(ADADup::Run): StartPosition: Problem open file for writing, Channel: %s.%s.%s.%s, Filename: %s ",
                (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc, (char*)&start_position_file_path ));
            }
           
            return TN_FAILURE;
        }
    } catch (...) {
        if (fp) fclose (fp);
        if ( LOG_VERBOSE_LEVEL > 0 ) {
            sm.ErrorMessage(Compat::Form("(ADADup::Run): StartPosition: Problem update file, Channel: %s.%s.%s.%s, Filename: %s ",
            (char*)channel.network, (char*)channel.station, (char*)channel.channel, tmp_loc, (char*)&start_position_file_path ));
        }
        return TN_FAILURE;
    }
    return TN_SUCCESS;
#endif    
}

int  GCDADupMasterModuleControlBlock::load (char * dir, char* file) {
    //TODO: check the dir exist,writable?
    strcpy(start_position_dir,dir);
    strcpy(start_position_file,file); 
    
    return TN_SUCCESS;
}


