/** 
* @file
* @ingroup group_gcda
*/
// 
// File:   coutlog.h
// Author: ncss
//
// Created on February 5, 2007, 5:36 PM
//


#ifndef _coutlog_H
#define	_coutlog_H

#include <cstdio>
#include <cerrno>
#include "TimeStamp.h"
using namespace std;




struct log_file {
    
    int initialized ;
    FILE *afile;
    FILE *lfile;
    TimeStamp curtime;
    char lfilename[PATH_MAX];
#ifdef LOG_DEFAULT_FILENAME
    char default_filename[FILENAME_MAX] =  (char*) LOG_DEFAULT_FILENAME ;
#else     
    char default_filename[FILENAME_MAX] = NULL;
#endif
  
#ifdef _PTHREAD_H
    pthread_mutex_t print_lock;
    
    void lock () { pthread_mutex_lock (&print_lock);  }
    void unlock () { pthread_mutex_unlock (&print_lock); }
#else
    void lock () {}
    void unlock () {}
#endif


    log_file () {
#ifdef _PTHREAD_H         
        pthread_mutex_init(&print_lock,NULL); 
#endif        
        initialized = 0;
        lfile = NULL;
        afile = NULL;
        curtime = TimeStamp (0, 0);
    }
    ~log_file () {
        if (initialized ) {
#ifdef _PTHREAD_H         
            (void)pthread_mutex_destroy(&print_lock); 
#endif
            if (lfile) {
                fclose (lfile);
            }
            if (afile) {
                fclose (afile);
            }
        }
        
    }
    
    
    
    
    int updateCurrentTimeDaily () {
        
        int retval;
        TimeStamp newtime;
        FILE *newfile;
        if ( ! initialized ) {
            return (0);
        } 
        
        newtime = TimeStamp::current_time ();
        if ((newtime.day_of_year () != curtime.day_of_year ())
        || (newtime.year () != curtime.year ())) {
            lock();
            curtime = newtime;
            // Create new log filename
            sprintf (lfilename, "%s_%04d%02d%02d.log", default_filename, newtime.year (),
            newtime.month_of_year (), newtime.day_of_month ());
            // Open new logfile
            newfile = freopen (lfilename, "a", afile);
            if (newfile == NULL) {
                cerr << "Error (Logfile): Error opening " << lfilename <<"error code: "<<errno<< endl;
            }
            lfile = newfile;
            fputs ("\n------------------------------------------\n", lfile);
            fprintf (lfile, "%02d/%02d/%d Change of Calendar Date",
            curtime.day_of_month (), curtime.month_of_year (),
            curtime.year ());
            retval = fputs ("\n------------------------------------------\n\n", lfile);
            
            if (retval == EOF) {
                cout << "Error (Logfile): Error writing to " << lfilename;
            }
            unlock();
        }
        return(0);
    }
    
    
    static int attach_cout (const char * filename) {
        lock();
        afile = stdout;
        strcpy (default_filename, filename);
        curtime = TimeStamp ();
        initialized = 1;
        unlock();
        (void)updateCurrentTimeDaily ();
        return (1);
    }
    
    int attach (const char * filename, FILE* f) {
        lock();
        afile = f;
        if (!afile) 
        strcpy (default_filename, filename);
        curtime = TimeStamp ();
        initialized = 1;
        unlock();
        (void)updateCurrentTimeDaily ();
        return (1);
    }
    
    FILE * newfile (const char * filename) {
        TimeStamp newtime = TimeStamp::current_time ();
        FILE *newfile;
        strcpy (default_filename, filename);
        
    }
    
    
};
static struct log_file cout_log_cb;




#endif	/* _coutlog_H */

