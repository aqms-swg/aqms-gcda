/** 
* @file
* @ingroup group_gcda
*/
#ifndef __wordorder_h
#define __wordorder_h


#include <sys/types.h>
#include <netinet/in.h>
#include <inttypes.h>

template <class T>
inline void wordorder_slave (T & n){
    if (sizeof (T) == 4 ) {
        uint32_t *p = reinterpret_cast<uint32_t*>(&n);  
        *p = ntohl( *p ) ;
        n = *(reinterpret_cast<T*> (p));        
    }    
    else if (sizeof (T) == 2 ) {
        uint16_t *p = reinterpret_cast<uint16_t*>(&n);  
        *p = ntohs( *p ) ;
        n = *(reinterpret_cast<T*> (p));
    }
    else if (ntohs(1)!=1) { //generic case
        T t;
        char *src = reinterpret_cast<char *>(&n);
        char *dst = reinterpret_cast<char *>(&t)+sizeof(T);
        do { *(--dst) = *(src++); } while(dst != reinterpret_cast<char *>(&t));
        n = t;  
    }
}

template <class T>
inline void wordorder_master (T & n){
    if (sizeof (T) == 4 ) {
        uint32_t *p = reinterpret_cast<uint32_t*>(&n);  
        *p = htonl( *p ) ;
        n = *(reinterpret_cast<T*> (p));        
    }    
    else if (sizeof (T) == 2 ) {
        uint16_t *p = reinterpret_cast<uint16_t*>(&n);  
        *p = htons( *p ) ;
        n = *(reinterpret_cast<T*> (p));
    }
    else if (htons(1)!=1) {  //generic case 
        T t;
        char *src = reinterpret_cast<char *>(&n);
        char *dst = reinterpret_cast<char *>(&t)+sizeof(T);
        do { *(--dst) = *(src++); } while(dst != reinterpret_cast<char *>(&t));
        n = t;  
    }
}

template <class T>
inline size_t serialize(T & n, char*& dst ){
    size_t i =0;
    char *src = reinterpret_cast<char *>(&n);
    for (i; i < sizeof(T); ++i ){ *(dst++) = *(src++);} 
    return i;
}

template <class T>
inline size_t deserialize(T & n, char*& src ){
    size_t i =0;
    char *dst = reinterpret_cast<char *>(&n);
    for (i; i < sizeof(T); ++i ){ *(dst++) = *(src++);} 
    return i;
}


#endif  
//__wordorder_h

