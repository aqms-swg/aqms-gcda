/** 
* @file
* @ingroup group_gcda
*/

#ifndef __DataTypes_h
#define __DataTypes_h

#include "AmplitudeADA.h"
#include "wordorder.h"

class GCDA_data_type:
    public amplitude_data_type 
{
 public:

    inline void wordorder_slave () {
	rt_wordorder_slave (peak_acceleration);
	rt_wordorder_slave (peak_velocity);
	rt_wordorder_slave (peak_displacement);
	rt_wordorder_slave (spectral_peak_0_3);
	rt_wordorder_slave (spectral_peak_1_0);
	rt_wordorder_slave (spectral_peak_3_0);
        //start_time
	//::wordorder_slave (peak_wa_amp.samples_after_timeval_peak);
        ::wordorder_slave (peak_wa_amp.samples_after_start_time);
	::wordorder_slave (peak_wa_amp.uncorrected_data_point);
	::wordorder_slave (peak_wa_amp.corrected_data_point);
	::wordorder_slave (peak_wa_amp.quality);
	::wordorder_slave (integral_of_velocity_squared);
	::wordorder_slave (energy_in_ergs);
	::wordorder_slave (ml100);
	::wordorder_slave (me100);
	::wordorder_slave (on_scale);
	::wordorder_slave (fullwin);
	::wordorder_slave (flags);
	::wordorder_slave (noise_variance);
	::wordorder_slave (lta);
	::wordorder_slave (lta_window);
	::wordorder_slave (snr);
    }
    inline void wordorder_master () {
	rt_wordorder_master (peak_acceleration);
	rt_wordorder_master (peak_velocity);
	rt_wordorder_master (peak_displacement);
	rt_wordorder_master (spectral_peak_0_3);
	rt_wordorder_master (spectral_peak_1_0);
	rt_wordorder_master (spectral_peak_3_0);
	//::wordorder_master (peak_wa_amp.samples_after_timeval_peak);
        ::wordorder_master (peak_wa_amp.samples_after_start_time);
	::wordorder_master (peak_wa_amp.uncorrected_data_point);
	::wordorder_master (peak_wa_amp.corrected_data_point);
	::wordorder_master (peak_wa_amp.quality);
	::wordorder_master (integral_of_velocity_squared);
	::wordorder_master (energy_in_ergs);
	::wordorder_master (ml100);
	::wordorder_master (me100);
	::wordorder_master (on_scale);
	::wordorder_master (fullwin);
	::wordorder_master (flags);
	::wordorder_master (noise_variance);
	::wordorder_master (lta);
	::wordorder_master (lta_window);
	::wordorder_master (snr);
    }

    inline void serialize (size_t& size, char* buff) {
        /*
            basiclly we don't need to do anything for A.D.A data type
            because it has all data has proper alignment
            but in general case: 
                memcpy(buff[0+0], 1st_member )
                memcpy(buff[0+sizeof(1st_member)], 2st_member )
                ...
        */
        
        
        char* ptr = buff;
        size = 0; 
        
        size += rt_serialize (peak_acceleration, ptr );
        size += rt_serialize (peak_velocity, ptr );
        size += rt_serialize (peak_displacement, ptr );
        size += rt_serialize (spectral_peak_0_3, ptr );
	size += rt_serialize (spectral_peak_1_0, ptr );
	size += rt_serialize (spectral_peak_3_0, ptr );
        
        //size += ::serialize (peak_wa_amp.samples_after_timeval_peak, ptr ); 
        size += ::serialize (peak_wa_amp.samples_after_start_time, ptr ); 
	size += ::serialize (peak_wa_amp.uncorrected_data_point, ptr ); 
	size += ::serialize (peak_wa_amp.corrected_data_point, ptr ); 
	size += ::serialize (peak_wa_amp.quality, ptr ); 
	size += ::serialize (integral_of_velocity_squared, ptr ); 
	size += ::serialize (energy_in_ergs, ptr ); 
	size += ::serialize (ml100, ptr ); 
	size += ::serialize (me100, ptr ); 
	size += ::serialize (on_scale, ptr ); 
	size += ::serialize (fullwin, ptr ); 
	size += ::serialize (flags, ptr ); 
	size += ::serialize (noise_variance, ptr ); 
	size += ::serialize (lta, ptr ); 
	size += ::serialize (lta_window, ptr ); 
	size += ::serialize (snr, ptr ); 
	

    }
    
    inline void deserialize (size_t& size, char* buff) {
        
        char* ptr = buff;
        size =0; 
        
        size += rt_deserialize (peak_acceleration, ptr );
        size += rt_deserialize (peak_velocity, ptr );
        size += rt_deserialize (peak_displacement, ptr );
        size += rt_deserialize (spectral_peak_0_3, ptr );
	size += rt_deserialize (spectral_peak_1_0, ptr );
	size += rt_deserialize (spectral_peak_3_0, ptr );
        //size += ::deserialize (peak_wa_amp.samples_after_start_time, ptr ); 
        size += ::deserialize (peak_wa_amp.samples_after_start_time, ptr ); 
	size += ::deserialize (peak_wa_amp.uncorrected_data_point, ptr ); 
	size += ::deserialize (peak_wa_amp.corrected_data_point, ptr ); 
	size += ::deserialize (peak_wa_amp.quality, ptr ); 
	size += ::deserialize (integral_of_velocity_squared, ptr ); 
	size += ::deserialize (energy_in_ergs, ptr ); 
	size += ::deserialize (ml100, ptr ); 
	size += ::deserialize (me100, ptr ); 
	size += ::deserialize (on_scale, ptr ); 
	size += ::deserialize (fullwin, ptr ); 
	size += ::deserialize (flags, ptr ); 
	size += ::deserialize (noise_variance, ptr ); 
	size += ::deserialize (lta, ptr ); 
	size += ::deserialize (lta_window, ptr ); 
	size += ::deserialize (snr, ptr ); 

    }
    
private:

     
    inline size_t rt_serialize (struct reading_type &d,  char*& ptr ) {
        size_t size = 0;
        //size += ::serialize (d.samples_after_start_time, ptr );
        size += ::serialize (d.samples_after_start_time, ptr );
        size += ::serialize (d.data_point, ptr );
        size += ::serialize (d.quality, ptr );
        return (size);
    }
    inline size_t rt_deserialize (struct reading_type &d,  char*& ptr ) {
        size_t size = 0;
        //size += ::deserialize (d.samples_after_start_time, ptr );
        size += ::deserialize (d.samples_after_start_time, ptr );
        size += ::deserialize (d.data_point, ptr );
        size += ::deserialize (d.quality, ptr );
        return (size);
    }
   
    inline void rt_wordorder_slave(struct reading_type &d) {
        //::wordorder_slave(d.samples_after_timeval_peak);
        ::wordorder_slave(d.samples_after_start_time);
        ::wordorder_slave(d.data_point);
        ::wordorder_slave(d.quality);
    }
    
    inline void rt_wordorder_master(struct reading_type &d) {
        //::wordorder_master(d.samples_after_timeval_peak);
        ::wordorder_master(d.samples_after_start_time);
        ::wordorder_master(d.data_point);
        ::wordorder_master(d.quality);
    }

};



#endif
    
