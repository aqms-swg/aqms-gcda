/** 
* @file
* @ingroup group_gcda
*/

#include <iostream>
#include <cstdio>
#include <cstring>
#include <csignal>
#include <unistd.h>
#include "project_globals.h"
#include "master_module.h"
#include "Runtime.h"
             


void usage (char *progname) {
    std::cout << std::endl << "usage: " << progname << " <config file>" << std::endl << std::endl;
    std::cout << progname << ":  Version v" << VERSION<< std::endl;
    
}


int main (int argc, char* argv[]){
    {
        GCDADupMasterTSPModuleManager GCDADupMaster ;
        
        if (argc != 2) {
            usage(argv[0]);
            return ( TN_FAILURE );
        }
        
        (void) GCDADupMaster.SetProgramName((char*)argv[0]);
        (void) GCDADupMaster.SetProgramVersion((char*)VERSION);
        std::cout << argv[0] << ":  Version v" << VERSION << std::endl;
        
        ArgList args;
        args.push_back(argv[1]);
        GCDADupMaster.SetArgs(args);
        
        Runtime run(&GCDADupMaster, argv[1] );
        
        if (!run) {
            std::cout << "Error (main): Unable to create runtime environment" << std::endl;
            return(1);
        }
        
        
        if (run.Run() != TN_SUCCESS) {
            std::cout << "Error (main): Run failure" << std::endl;
        }
    }
    std::cout << "Terminate program." << std::endl;
    return(TN_SUCCESS);
}
