Project Name : ADA DUP II
Original Author: Alexei Kireev
Description:
Creation Date: Jun 2007
Modification History:
Usage Notes: 

version 2.0.0:
-This project was derived from GCDA Dup  and RAD

version 2.0.3:
-added some features to improve speed while reading/writing GCDA: 
   write/read by packet mode 

version 2.0.4:
-added some features to improve network communication: 
   compiler flags (by default off):
  -DFLOATING_BUFFER_SIZE  :allow floating sizeof network. PL ask for it. AK belive this is useless.     
  -DNETWORK_BOOST	  :allow pre-cached ACK read. Improve transfer speed, but some data loses may occur on reconnect.
 
version 2.0.5:
-added some improvment to store start positions for channels: 
   compiler flags (by default off):
  -DSTARTPOSITION_MMAP    :uses direct memory access (pages) to store time pointers in file. 
   WARNING !!! Do not modifile times_map_file during the program runnig,  the standard behavior on it - SIGBUS.
   IN CASE of modify the file Master Program will shutdown.
   
version 2.2.0
-added pragma pack and alignment control 
 Pete Lombard has changed struct timeval to double 
 and noticed that type double require aligment 8 for SPARC platform
-Alignment by 8 works for SPARC (V8, Generic) and X86
 SPARC V9 require alingment 16 for long double 

################################

			
 