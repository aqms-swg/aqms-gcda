:no_entry_sign: **CAUTION, WORK IN PROGRESS** :no_entry_sign:

# Generic Continuous Data Area or GCDA

The gcda are a handful of small C++ programs that create, monitor, and destroy the Generic Continuous Data Areas known as Waveform Data Area (wda) and the Acceleration Data Area (ada). These include makeada, deleteada, makewda, deletewda, scang, accmon, adadup.