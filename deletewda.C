/** 
* @file
* @ingroup group_gcda
* @brief Destroys the WDA and exits.
*/
/***********************************************************/
/*
* File Name :
* 	deletewda.C
*
* Programmer:
* 	Paul Friberg
*
* Description:
* 	This is a top level driver. It deletes the wda
* 	then exits.
*	
*
* Creation Date:
* 	2 Feb 2004
*
*
* Usage Notes:
*
*
* Modification History:
* 	17-Jan-2013 - Ported to Linux
*/
/**********************************************************/
#include <iostream>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <cstdio>
#include <ctime>
#include <sys/time.h>
#include <cstring>
#include "RetCodes.h"
#include "gcda.h"
#include "dcmon.h"

int debug = TN_TRUE;

int main(int argc,char* argv[])
{

  char key_name[256];


  if (argc < 2)
  {
    std::cout << "USAGE : deletewda key_name" << std::endl;
    return(TN_FAILURE);
  }

  if (argv[1] != NULL)
  {
    strcpy(key_name,argv[1]);
    
  }
  else
  {
    std::cout << "Null configuration file name " << std::endl;
    return(TN_FAILURE);
  }

  Generic_CDA<int> dmap(std::string(key_name),0);
  dmap.Delete_Memory_Area();

  struct timeval outtime;
  int res = gettimeofday(&outtime,NULL);
  time_t mtime =  (time_t) outtime.tv_sec;
  std::cout << "Memory Map Deleted at : " << ctime(&mtime)  << std::endl;

  return(TN_SUCCESS);
}
