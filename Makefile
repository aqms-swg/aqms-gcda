########################################################################
#
# Makefile     : gcda_utils
#
# Author       : Paul Friberg
#
# Last Revised : November 10, 1999
# History      : 2021/01/15    AB    Removed targets makeeda, makecda
#
#
########################################################################

include $(DEVROOT)/shared/makefiles/Make.includes
BIN	= accmon intmon makeada makewda scang \
    deleteada deletewda adadata adastat wdadata wdastat \
    ew2wda

INCL	= $(RTSTDINCL) -I$(DEVROOT)/shared/include
LIBS	= -lgcda $(RTSTDLIBS_NOCMS)

SCANGOBJS = scang.o shand.o

########################################################################

all:$(BIN)

makeada: makeada.o
	$(CC) $(CFLAGS) makeada.o -o $@ $(LIBS)

deleteada: deleteada.o
	$(CC) $(CFLAGS) deleteada.o -o $@ $(LIBS)

deletewda: deletewda.o
	$(CC) $(CFLAGS) deletewda.o -o $@ $(LIBS)

makewda: makewda.o
	$(CC) $(CFLAGS) makewda.o -o $@ $(LIBS)

accmon: accmon.o shand.o
	$(CC) $(CFLAGS) accmon.o  shand.o -o $@ $(LIBS)

intmon: intmon.o
	$(CC) $(CFLAGS) intmon.o shand.o -o $@ $(LIBS)

scang: $(SCANGOBJS) scdef.h
	$(CC) $(CFLAGS) $(SCANGOBJS) -o $@ $(LIBS)

adadata: adadata.o
	${CC} ${CFLAGS} -o adadata adadata.o $(LIBS)

wdadata: wdadata.o
	${CC} ${CFLAGS} -o wdadata wdadata.o $(LIBS)

adastat: adastat.o
	${CC} ${CFLAGS} -o adastat adastat.o $(LIBS)

wdastat: wdastat.o
	${CC} ${CFLAGS} -o wdastat wdastat.o $(LIBS)

ew2wda:
	(cd ew2wda; make)


.C.o: 
	$(CC) $(CFLAGS) $(INCL)  $< -c 

clean:
	-rm -f *.o *~ core $(BIN)
	-rm -f -r ./SunWS_cache
