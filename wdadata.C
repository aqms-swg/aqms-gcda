/** 
* @file
* @ingroup group_gcda
*/

#include <iostream>

#include <string>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <unistd.h>
#include <fnmatch.h>

#include "TimeStamp.h"
#include "RetCodes.h"
#include "seismic.h"
#include "chan.h"
#include "gcda.h"
#include "dreader.h"
#include "nscl.h"

#include "qlib2.h"

#define	VERSION	"1.05.0 17-Jan-2013"
#define	MAX_OUTPUT_SAMPLES  1024000
#define WDA_KEYNAME "WDA_KEY"


const char *syntax[] = {
"%s version " VERSION,
"%s - retrieve data from WDA shared memory for station, channel, time.",
"%s  [-n] [-f date] [-t date | -s interval] [-a] -[l N] [-D j|j,|m|m,]",
"    [-r|-R] [-A wda_keyname] [-C] [-B] [-o output_file] [-h]",
"    stat.net.chan.loc ...",
"    where:",
"	-A ada_keyname	Specify an alternate wda_keyname file.",
"		default is " WDA_KEYNAME,
"	-n	    No data output.",
"	-f date	    From date - ignore data before this date.",
"	-t date	    To date - ignore data from this date on.",
"		    Date can be in formats:",
"			[19]yy/mm/dd/hh:mm:ss.ffff",
"			[19]yy/mm/dd.hh:mm:ss.ffff",
"			[19]yy/mm/dd,hh:mm:ss.ffff",
"			[19]yy.ddd,hh:mm:ss.ffff",
"			[19]yy,ddd,hh:mm:ss.ffff",
"	-s interval span interval.  Alternate way of specifying end time.",
"		    Interval can be an integer followed immediately by",
"		    s, m, h, or d for seconds, minutes, hours, or days.",
"	-a	    Output data in ascii.",
"	-l N	    Put N ascii data points per line.",
"	-r | R	    Output 1 line summary report for each channel.",
"	-Q	    If insufficient data, run qdata to get data.",
"	-B	    Dummy argument for compatability with qdata.",
"	-C	    Dummy argument for compatability with qdata.",
"	-o output_file",
"		    Output filename for data from this request.",
"	-h	    Print this syntax summary",
"	stat.net.chan.loc ...",
"		    A list of one or more fully-qualified channel names.",
NULL };

//**********************************************************************

typedef struct _Channel_id {
	char station[MAX_CHARS_IN_STATION_STRING];
	char network[MAX_CHARS_IN_NETWORK_STRING];
	char channel[MAX_CHARS_IN_CHANNEL_STRING];
	char location[MAX_CHARS_IN_LOCATION_STRING];
} Channel_id;

//  External variables.
char *cmdname;
static int no_output = 0;
static int ascii_output = 0;
int date_fmt = 0;
int points_per_line = 10;
int status_report = 0;

int discard_incomplete_data = 0;
int run_qdata = 0;
FILE *info = stdout;
char **stationv = NULL;
int nstation = 0;
char **channelv = NULL;
int nchannel = 0;
char **snclv = NULL;
int nsncl = 0;

char *wda_keyname = NULL;
char *pass_B = (char *)"";
char *pass_C = (char *)"";
char *pass_n = (char *)"";
char pass_o[MAXPATHLEN] = "";
char *outfile = (char *)NULL;
FILE *outfp = (FILE *)NULL;

/************************************************************************/
/*  print_syntax:							*/
/*	Print the syntax description of program.			*/
/************************************************************************/
void print_syntax(char *cmd)
{
    int i;
    for (i=0; syntax[i] != NULL; i++) {
	fprintf (info, syntax[i], cmd);
	fprintf (info, "\n");
    }
}

Channel_id parse_channel_id (char *channel_str)
{
    Channel_id channel_id = {"*", "*", "*", "*"};
    char *token;

    if (channel_str == NULL) return (channel_id);

    token = strtok (channel_str, ".");
    if (token == NULL) return (channel_id);
    strcpy (channel_id.station, token);

    channel_str = NULL;

    token = strtok (channel_str, ".");
    if (token == NULL) return (channel_id);
    strcpy (channel_id.network, token);

    token = strtok (channel_str, ".");
    if (token == NULL) return (channel_id);
    strcpy (channel_id.channel, token);

    token = strtok (channel_str, ".");
    if (token == NULL) return (channel_id);
    strcpy (channel_id.location, token);
    mapLC(channel_id.network, channel_id.location, MEMORY);
    
    return (channel_id);
}

/************************************************************************/
/*  upcase:								*/
/*	Convert a string to upper case.					*/
/************************************************************************/
char *upcase (char *str) {
    char *p = str;
    for (p=str; *p; p++) *p = toupper (*p);
    return (str);
}

/************************************************************************/
/*  get_and_output_data:						*/
/*	Get data from WDA for station/channel/time and output it.	*/
/************************************************************************/
int get_and_output_data (key_t &wda_key, char *wda_mem, char *station, 
			 char *channel, char *network, 
			 char *location, INT_TIME start_time, 
			 int duration, int *p_rate, 
			 INT_TIME *p_out_time, int *p_ns, int *retarray)
{
    int res;
    int seconds, usecs;    
    INT_TIME end_time;
    int no_output_channel = no_output;
    int status;
    char file_loc[MAX_CHARS_IN_LOCATION_STRING];

    /* Declare a wave_reader. */
    Data_Channel_Reader<int> *wave_reader;
    wave_reader = new Data_Channel_Reader<int>(wda_key);
    // DEBUG
    //    status = wave_reader->Set_Debug_On(1);

    TimeStamp request_start_time(TRUE_EPOCH_TIME, int_to_tepoch(start_time));
    TimeStamp time_of_first_sample(TRUE_EPOCH_TIME,0.0);
    int duration_seconds = 0;
    int duration_usecs = 0;
    int number_of_returned_samples = 0;

    *p_out_time = start_time;
    *p_rate = 1;
    *p_ns = 0;

    res = wave_reader->Initialize_Channel(wda_mem,
					  network, station, 
					  channel, location);
    mapLC(network, location, ASCII);

    if (res == TN_SUCCESS) {
	*p_rate = (int)wave_reader->Rate_in_Samples_per_Second();

	duration_seconds = duration;
	duration_usecs = 0;
    
	int res2;
	res2 = wave_reader->Get_Contiguous_Samples(request_start_time,
				      duration_seconds,
				      duration_usecs,
				      time_of_first_sample,
				      number_of_returned_samples,
				      retarray);

	*p_ns = number_of_returned_samples;
	if (res2 == 0) {
	    *p_out_time = 
		tepoch_to_int(time_of_first_sample.ts_as_double(TRUE_EPOCH_TIME));
	}
    }

    if (number_of_returned_samples == int(duration * sps_rate(*p_rate,1))) status = 1;
    else if (number_of_returned_samples == 0) status = 0;
    else status = -1;
    if (status != 1 && run_qdata) {
	char cmd[256];
	if (outfp) {
	    fclose (outfp);
	    unlink (outfile);
	}
	sprintf (cmd, "qdata -f %s -s %dS %s %s %s %s %s.%s.%s.%s",
		 time_to_str(start_time,1), duration, 
		 pass_n, pass_B, pass_C, pass_o,
		 station, network, channel, location);
	fprintf (stderr, "All data is not available in WDA for %s.%s.%s.%s\n", 
		 station, network, channel, location);
	system (cmd);
	return (0);
    }
    if (status_report) {
	fprintf (stdout, "%s.%s.%s.%s %d\n", station, network, channel, 
		 location, status);
    }
    if (res == TN_FAILURE) {
	fprintf (stderr, "Requested data not available in WDA for %s.%s.%s.%s\n", 
		 station, network, channel, location);
	return (res);
    }
    if (! no_output_channel) {
	if (ascii_output) {
	    for(int i=0;i<number_of_returned_samples; i++) {
		std::cout << retarray[i] << " ";
		if (0 == (i%points_per_line)) {
		    std::cout << std::endl;
		}
	    }
	    std::cout << std::endl;
	}

	else {
	    // Create miniSEED output.
	    int nblocks;
	    int nw;
	    int blksize = 4096;
	    EXT_TIME et;
	    char ms_name[256];
	    char *ms = NULL;
	    DATA_HDR *hdr = NULL;

	    if ((hdr = new_data_hdr()) == NULL) {
		fprintf (stderr, "Error allocating DATA_HDR\n");
		return (1);
	    }
	    strcpy (hdr->station_id, station);
	    strcpy (hdr->channel_id, channel);
	    strcpy (hdr->network_id, network);
	    strcpy (hdr->location_id, location);
	    mapLC(network, hdr->location_id, BINARY);
	    
	    hdr->sample_rate = *p_rate;
	    hdr->sample_rate_mult = 1;
	    hdr->begtime = hdr->hdrtime = *p_out_time;
	    hdr->data_type = STEIM2;		
	    hdr->blksize = blksize;
	    status = ms_pack_data (hdr, NULL, number_of_returned_samples, retarray,
				   &nblocks, &ms, 0, NULL);
	    if (status != number_of_returned_samples || ms == NULL) {
		fprintf (stderr, "Error creating miniSEED output for %s.%s.%s.%s, nsamples=%d, status=%d\n",
			 station, network, channel, location, 
			 number_of_returned_samples, status);
		if (ms) free (ms);
		return (1);
	    }
	    et = int_to_ext(*p_out_time);
	    if (! no_output) {
		if (outfile == NULL) {
		    strcpy(file_loc, location);
		    mapLC(network, file_loc, FILENAME);
		    sprintf (ms_name, "%s.%s.%s.%s.D.%04d.%03d.%02d%02d%02d",
			     station, network, channel, file_loc, 
			     et.year, et.doy, et.hour, et.minute, et.second);
		    if ((outfp = fopen(ms_name, "w")) == NULL) {	
			fprintf (stderr, "Error opening miniSEED file %s\n", ms_name);
			free (ms);
			return (1);
		    }
		}
		if ((nw = fwrite (ms, blksize, nblocks, outfp)) != nblocks) {
		    fprintf (stderr, "Error writing miniSEED file %s - have %d blocks, wrote %d blocks\n",
			     ms_name , nblocks, nw);
		    fclose (outfp);
		    free (ms);
		    return (1);
		}
		if (outfile == NULL) fclose (outfp);
	    }
	    free (ms);
	}
    }
    if (! ascii_output) {
	time_interval2 (number_of_returned_samples, *p_rate, 1, &seconds, &usecs);
	end_time = add_time (*p_out_time, seconds, usecs);
	fprintf (info, "%s.%s.%s.%s: rate=%d (%s to ", station, network, channel, 
		 location, *p_rate,
		 time_to_str(*p_out_time, date_fmt));
	fprintf (info, "%s) : %d points, ",
		 time_to_str(end_time, date_fmt), number_of_returned_samples);
	fprintf (info, "%.1lf msec correction (min,max,max_step = %3.1lf,%.1lf,%.1lf msec)\n",
		 0.0, 0.0, 0.0, 0.0);
    }
    return (res);
}

/************************************************************************/
/*  main program.							*/
/************************************************************************/
int main(int argc, char* argv[])
{
    INT_TIME start_time;
    INT_TIME end_time;
    INT_TIME out_time;
    INT_TIME *pt;
    int ns;
    int retarray[MAX_OUTPUT_SAMPLES];
    int duration; 
    int res;
    int rate;
    int start_flag = 0;
    int end_flag = 0;
    int span_flag = 0;
    char *span, *p, *station_list, *token;
    char **stationv = NULL;
    Channel_id *channel_id = NULL;
    int i, j, k;
    int nstation = 0;
    int nchannel = 0;
    struct timeval tp;

    /* Variables needed for getopt. */
    extern char	*optarg;
    extern int	optind, opterr;
    int c;

    cmdname = ((p = strrchr(*argv,'/')) != NULL) ? ++p : *argv;
    while ( (c = getopt(argc,argv,"hnarQBCA:f:t:s:l:o:")) != -1)
	switch (c) {
	case '?':
	case 'h':
	    print_syntax(cmdname); exit(0); break;
	case 'A':
	    wda_keyname = strdup(optarg);break;
	case 'n':	
	  ++no_output; pass_n = (char *)"-n"; break;
	case 'f':
	  if ((pt = parse_date (optarg)) == NULL) {
	      std::cout << "invalid date: " <<  optarg << std::endl;
	      exit(1);
	  }
	  start_time = *pt;
	  ++start_flag;
	  break;
	case 't':
	  if ((pt = parse_date (optarg)) == NULL) {
	      std::cout << "invalid date: " <<  optarg << std::endl;
	      exit(1);
	  }
	  end_time = *pt;
	  ++end_flag;
	  break;
	case 's':
	  span = optarg; ++span_flag; break;
        case 'r':
	  status_report = 1; break;
        case 'Q':
	  run_qdata = 1; break;
        case 'B':	/* Compatability with qdata	*/
	  pass_B = (char *)"-B";
	  break;
        case 'C':	/* Compatability with qdata	*/
	  pass_C = (char *)"-C";
	  break;
	case 'l':
	  points_per_line = atoi(optarg); break;
	case 'D':
	  /* Date display format.   */
	  if (strcmp (optarg, "j") == 0) date_fmt = JULIAN_FMT;
	  else if (strcmp (optarg, "j,") == 0) date_fmt = JULIAN_FMT_1;
	  else if (strcmp (optarg, "m") == 0) date_fmt = MONTH_FMT;
	  else if (strcmp (optarg, "m,") == 0) date_fmt = MONTH_FMT_1;
	  else {
	      std::cout <<  "invalid format for date display: " <<  optarg << std::endl;
	      exit(1);
	  }
	  break;
	case 'a':
	  ascii_output = 1; break;
	case 'o':
	  outfile = optarg; sprintf(pass_o,"-o %s",optarg); break;
	default:
	  fprintf (info, "Error: unknown option: -%c\n", c);
	  exit(1);
      }

    if (points_per_line <= 0) {
	fprintf (stderr, "points per line must be > 0\n");
	exit(1);
    }
    if (end_flag && span_flag) {
	fprintf (stderr, "span and endtime mutually exclusive\n");
	exit(1);
    }
    if (span_flag && !start_flag) {
	fprintf (stderr, "span not valid without start time\n");
	exit(1);
    }
    if (span_flag && ! valid_span(span)) {
	    fprintf (stderr, "invalid span specification: %s\n", span);
	    exit(1);
	}
    if (span_flag) {
	end_time = end_of_span (start_time, span);
	++end_flag;
    }

    if (outfile) {
	if ((outfp = fopen(outfile, "w")) == NULL) {	
	    fprintf (stderr, "Error opening miniSEED file %s\n", outfile);
	    exit(1);
	}
    }

    if (wda_keyname == NULL)
	wda_keyname = strdup(WDA_KEYNAME);

    /*	Skip over all options and their arguments.			*/
    argv = &(argv[optind]);
    argc -= optind;

    // Determine whether we have a list of SNCLs or station and channel list.
    if (argc <= 0) {
	print_syntax(cmdname);
	exit(-1);
    }
    while (argc-- > 0) {
	station_list = *(argv++); 
	while (token = strtok(station_list, ",")) { 
	    station_list = NULL;
	    stationv = (stationv == NULL) ? 
	      (char **)malloc((nstation+1)*sizeof(char	*)) : 
	      (char **)realloc((void *)stationv,(nstation+1)*sizeof(char *));
	    stationv[nstation++] = uppercase(strdup(token));
	}
    }
    channel_id = (Channel_id *)malloc((nstation+1)*sizeof(Channel_id));
    for (i=0; i<nstation; i++) {
	channel_id[i] = parse_channel_id (stationv[i]);
    } 

    if (! start_flag) {
	res = gettimeofday(&tp,NULL);

	// backup 6 minutes
	tp.tv_sec = tp.tv_sec - 360;
	start_time = int_time_from_timeval(&tp);
    }

    if (span_flag || end_flag) {
	duration = (int)(tdiff (end_time, start_time) / USECS_PER_SEC);
    }
    else {
	duration = 300;
    }

    int status = 0;
    if (status_report) info = stderr;

    // Attach to the wda.
    Generic_CDA<int>wda(std::string(wda_keyname),0);
    char *wda_mem = wda.Start_of_Memory_Area();
    key_t wda_key = wda.Get_Memory_Key();

    ChannelArray channels;
    wda.Scan_Channel_Headers(channels);

    for (i=0; i<channels.size(); i++) {
	for (k=0; k<nstation; k++) {
	    if (fnmatch(channel_id[k].station,channels[i].station,0) == 0 &&
		fnmatch(channel_id[k].network,channels[i].network,0) == 0 &&
		fnmatch(channel_id[k].channel,channels[i].seedchan,0) == 0 &&
		fnmatch(channel_id[k].location,channels[i].location,0) == 0) {

		res = get_and_output_data (wda_key, wda_mem, 
					   channels[i].station, 
					   channels[i].seedchan, 
					   channels[i].network,
					   channels[i].location, 
					   start_time, duration, &rate, 
					   &out_time, &ns, retarray);
		if (res != 0) status = res;
	    }
	}
    }

    return(status); // exit, really
}
