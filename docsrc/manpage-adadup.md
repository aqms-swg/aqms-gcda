# adadup {#man-adadup}

*adadup* was derived from Generic Continuous Data Area (gcda) DUP and Rapid Amplitude Data (RAD2). It currently has two programs: *adadup_master* and *adadup_slave*. Based on information from NCSS wiki page, on the Network Services systems, raw waveforms are processed by rad2 to estimate acceleration, velocity, displacement, and spectral response. The output of rad2 is a time series with one sample every 5 seconds for each SNCL being processed. Each sample includes each of the amplitude values, the time of the start of the sample, the time offset of each of the peaks, and some quality measures. Rad2 writes these time series to the ADA on the network service systems. They get forwarded to the real-time (RT) systems using adadup_master and adadup_slave. To export the ADA data to real-time machines, several instances of *adadup_master* run on the network services systems. To receive this data, a real-time system runs an instance of *adadup_slave* for each network services system. 
&nbsp;  

## PAGE LAST UPDATED ON

2021-03-02
&nbsp;  

## NAMES

adadup_master, adadup_slave 
&nbsp;  

## VERSION and STATUS

status: ACTIVE  
&nbsp;  

## PURPOSE
*adadup_master* runs on network service systems and sends ADA data to real-time systems.  
*adadup_slave* runs on real-time systems and receives ADA data from network service systems.
&nbsp;  

## HOW TO RUN

```
adadup_master network_service_2_realtime.cfg
adadup_slave network_service_2_realtime.cfg 
```
where network_service_2_realtime.cfg is the input configuration file that specifies global configuration parameters and network information for Master and Slave, e.g. ip address and program name, and database information and related parameters for GCDA, e.g. ADAKeyname.

For e.g. 
```
adadup_master adadup_ucbns12ucbrt.cfg
adadup_slave adadup_ucbns12ucbrt.cfg 
```
where *ucbns1* is a network service system and *ucbrt* is a real-time system. Here the configuratio name for adadup_master and adadup_slave is the same, but they have slightly different parameter values.
&nbsp;  
## CONFIGURATION FILE

Here are detailed description for the configuration files used by the adadup_master which runs on network service system and adadup_slave which runs on real-time system.

Format of the description below is 
```
<parameter name> <data type> <default value>
<description>
```
For example, a sample configuration file for **adadup_master** on *ucbns1* is shown below:

**#Global Configuration Parameters**

**Logfile** *string* &nbsp;&nbsp; /home/ncss/run/logs/adadup_ucbns12ucbrt

The whole path of log file with name.

**LoggingLevel** *integer* &nbsp;&nbsp; 1  

**ConsoleLog** *integer* &nbsp;&nbsp;0

**ProgramName** *string* &nbsp;&nbsp;adadup_master  

A unique string that looks up in the program/config_channel database tables the channels to include. 
&nbsp;

**#Network information for Master and Slave**

**SLAVE_IP_ADDRESS** *string* &nbsp;&nbsp; ucbrt.seismo.berkeley.edu

**SLAVE_IP_PORT** *integer* &nbsp;&nbsp; 45121

**START_TIMES_MODE**  *integer* &nbsp;&nbsp; 2

 This parameter allows to set up time position for the Matser program    
0  -  Master starts sending data from the oldest availavle time in GCDA            
2  -  Master will send only newest data , which comes into GCDA after the program has starte     
1  -  Master stores the current timestamp in file after successful delivery 
Next time when the Master program starts running again it reads the timestamp from a file. Each channel has it's own file(this makes life easy in multy-thread environment). If there is no file for a channel, the file will be created automatically. The other parameter ("START_TIMES_DIR" )     specifies a pathname pointed to DIRectory where those files are. Use START_TIMES_FILE if compiled with -DSTARTPOSITION_MMAP    

**#Database connection parameters**  

**Use database** 

**DBProgname** *string* &nbsp;&nbsp; SVC_ADA_UCB

The name of program in the program/config_channel database tables 

**Include** *string* &nbsp;&nbsp; commonDb.cfg

The database configuration file includes DBservice, DBUser, and DBPasswd.

**#Parameters for Generic Contiguous Data Area (GCDA)**

**ADAKEYname** *string* &nbsp;&nbsp; ADA_KEY

The key name is in gcda.cfg

**GCDA_READ_MODE** *integer* &nbsp;&nbsp; 2

This is described method how to read data from gcda.
0 - Read subroutine relies on standard GCDA library call "Get_Next_Sample"
1 -  Read by a packet of samples. This helps to avoid unnesesary shared memory locks, by locking once for a serie of samples
2 - Read by packet + follow real time.
Allows to skip waiting for sample if far enough behind realtime.
The other parameter MAX_SECONDS_BEHIND_REALTIME needs to be specified.
	
**MAX_SECONDS_BEHIND_REALTIME** *integer* &nbsp;&nbsp;180

A sample configuration file for **adadup_slave** on *ucbrt* is shown below:

**#Global Configuration Parameters**

**Logfile** *string* &nbsp;&nbsp; /home/ncss/run/logs/adadup_ucbns12ucbrt

The whole path of log file with name

**LoggingLevel** *integer* &nbsp;&nbsp; 1  

**ConsoleLog** *integer* &nbsp;&nbsp;0

**ProgramName** *string* &nbsp;&nbsp;adadup_slave  

A unique string that looks up in the program/config_channel database tables the channels to include. 

**#Network information for Master and Slave**

**SLAVE_IP_ADDRESS** *string* &nbsp;&nbsp; ucbrt.seismo.berkeley.edu

**SLAVE_IP_PORT** *integer* &nbsp;&nbsp; 45121

**MASTER_IP_ADDRESS** *string* &nbsp;&nbsp; ucbns1.seismo.berkeley.edu
  
**#Database connection parameters**  

**Use database** 

**DBProgname** *string* &nbsp;&nbsp; RT_ADA

The name of program in the program/config_channel database tables 

**Include** *string* &nbsp;&nbsp; commonDb.cfg

The database configuration file includes DBservice, DBUser, and DBPasswd.
&nbsp;  

**#Parameters for Generic Contiguous Data Area (GCDA)**

**ADAKEYname** *string* &nbsp;&nbsp; ADA_KEY

The key name is in gcda.cfg
&nbsp;

## DEPENDENCIES

The database program and config_channel tables must be populated with some channels and the gcda.cfg file must have the key mapping pointed to by the KeyName argument.  
&nbsp;    


## MAINTENANCE
&nbsp;  

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-gcda/-/issues  
&nbsp;  

## MORE INFORMATION

*  Authors : Alexei Kireev, Pete Lombard 
