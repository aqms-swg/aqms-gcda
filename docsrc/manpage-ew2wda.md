# ew2wda {#man-ew2wda}

ew2wda takes Earthworm TRACEBUF2 packets and converts them into continuous waveform packets stored in the waveform data area (wda). wda is a shared memory area used by the rad2 program to get waveforms and is a creation of the early CISN software design. The wda is created with the gcda_util makewda.


## PAGE LAST UPDATED ON

Oct 10, 2008

## NAME

ew2wda

## VERSION AND STATUS

VERSION "0.0.4 2013.01.17",   ACTIVE

## HOW TO RUN

```
ew2wda ew2wda.d
```
where ew2wda.d is the input configuration file in Earthworm .d style (param value strings).


## CONFIGURATION FILE

Config.d arguments (required):

* MyModuleId   MOD_EW2WDA   - module id of the instance of ew2wda
* RingName    WAVE_RING  - the earthworm ring id where TRACEBUF2 messages should be pulled
* LogFile     1  - if 0, no logfile is generated, if 1, then a earthworm style logfile is created
* HeartBeatInterval 30 - number of seconds to beat the heartbeat message for statmgr to monitor this module.
* GetTraceFrom INST_WILDCARD MOD_WILDCARD - allows the user to specify one or more of these lines to get particular modules or institutions tracebuf2 packets.
* ProgramName RAD_AE - the name of the program which is used to look up channel lists from the Database table program/config_channel.
* WDA_KeyName WDA_AE_KEY - the id of the WDA shared memory area where the data should be written (same one used by makewda).
* DBService dbname - the name of the database being used (for most this will be rtdb or rtdb2)
* DBUser username - the name of the database user account (for most this will be rtem)
* DBPasswd password - the password for the database user account specified by DBUser
* DBConnectionRetryInterval num - number of seconds to fall back when retrying db reconnects (not used currently)
* DBMaxConnectionRetries num - number of times to retry db connections (not used currently)

Config.d options:

* Debug  - an optional parameter to turn on debugging, no arguments


Example config file:

```
#
# This is ew2wda's parameter file
#
MyModuleId         MOD_EW2WDA_AE     # module id for this instance of ew2wda
RingName           WAVE_RING_AE      # get trace_buf2 messages here
LogFile            1              # 0 to completely turn off disk log file
HeartBeatInterval  30             # seconds between heartbeats
#Debug                            # Optional switch to turn on debugging output
#
# Specify one or more logos of messages to get from Earthworm ring.
# Message type is always TYPE_TRACEBUF2.
#
GetTraceFrom   INST_WILDCARD   MOD_WILDCARD    # TYPE_TRACEBUF2
#
ProgramName     RAD_AE                # Program key to config_channels table
WDA_KeyName     WDA_AE_KEY            # WDA shared memory key
#
# should make this more central
@wda/db_info.d
```

where wda/db_info.d contains:
```
DBService       rtdb            
DBUser          rtem
DBPasswd        actualpasswordgoes here
DBConnectionRetryInterval 5
DBMaxConnectionRetries    5
```

## DEPENDENCIES

The database program and config_channel tables must be populated with some channels and the makewda module must also have been run for this module to work. This module only runs within a working Earthworm environment 7.X or greater version.

## MAINTENANCE

Use the scang gcda utility to look at the data        

## AUTHORS

Will Kohler wrote the first version.

Pete Lombard and Paul Friberg have worked on it further.

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-gcda/-/issues

## MORE INFORMATION

See gcda manual pages for scang and makewda commands.
