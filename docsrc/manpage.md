# gcda {#man-gcda}

Generic Continuous Data Area (gcda). These comprise a set of tools to manage the waveform data area (wda) and the acceleration data area (ada).  
&nbsp;  

## PAGE LAST UPDATED ON

2021-01-15  
&nbsp;  

## NAMES

makeada, deleteada, makewda, deletewda, scang, accmon
&nbsp;  

## VERSION and STATUS

status: ACTIVE  
&nbsp;  

## PURPOSE

gcda are a handful of small C++ programs that create, monitor, and destroy the Generic Continuous Data Areas known as Waveform Data Area (wda) and the Acceleration Data Area (ada). 

*makeada* and *makewda* can be used to create the ada and the wda, respectively.  
*deleteada* and *deletewda* can be used to destroy the ada and wda, respectively, from memory.  
*scang* is a generic data area scanner and will work on both wda and ada areas.  
*accmon* stands for acceleration monitor. Displays accelerations for a stream as they happen.    
&nbsp;  

## HOW TO RUN

```
makewda makewda_ae.cfg
makeada makeada.cfg
```
where *.cfg is the input configuration file in CISN style (param value strings much like Earthworm).  

```
deletewda <key_name>
deleteada <ada_key_name>
```
where <*key_name> is the key name lookup into the GCDA_FILE to find the actual shared memory id. All _DA using modules must be stopped before this can be run. 

```
accmon <adaKeyName> <network> <station-name> <channel> <location>

scang <key_name>
```

For e.g. 
```
scang WDA_KEY 
```

will produce the following output snapshot:  

```
scang ADA_KEY
Number of channels in gcda : 1234
Time of data area scan : ADA_KEY 2008/10/16,18:52:46.8260
Sta  Chan Loc  Latency(secs)   Samp/Sec  Samps_in_Mem  Num_Invalid   Empty  Newest_Sample
CI RIN   HHZ --       14.83      0.2       120          0   0   2008/10/16,18:52:32.0008
CI RIN   HHN --        9.83      0.2       120          0   0   2008/10/16,18:52:37.0008
CI GSC   HHZ --       14.84      0.2       120          0   0   2008/10/16,18:52:31.9968
CI GSC   HHN --        9.84      0.2       120          0   0   2008/10/16,18:52:36.9968
CI GSC   HHE --       14.84      0.2       120          0   0   2008/10/16,18:52:31.9968
CI MAG   HNN --        9.84      0.2       120          0   0   2008/10/16,18:52:36.9948
CI MAG   HNZ --       14.84      0.2       120          0   0   2008/10/16,18:52:31.9948
ZY SHF7  HNZ --  32982842.86      0.2       120        120   1   2007/10/01,00:58:43.9757
ZY SHF7  HNN --  32982842.86      0.2       120        120   1   2007/10/01,00:58:43.9758
ZY SHF7  HNE --  32982842.86      0.2       120        120   1   2007/10/01,00:58:43.9759
CI MSJ   HHZ --        9.84      0.2       120          0   0   2008/10/16,18:52:36.9926
ZY SHF2  HNZ --  32982842.86      0.2       120        120   1   2007/10/01,00:58:43.9760
ZY SHF2  HNN --  32982842.86      0.2       120        120   1   2007/10/01,00:58:43.9761
ZY SHF2  HNE --  32982842.86      0.2       120        120   1   2007/10/01,00:58:43.9762
CI WES   HHN --       14.84      0.2       120          0   0   2008/10/16,18:52:31.9966
CI WES   HHZ --        9.84      0.2       120          0   0   2008/10/16,18:52:36.9966
CI WBS   HHZ --        9.84      0.2       120          0   0   2008/10/16,18:52:36.9965
CI PHL   HHN --        9.84      0.2       120          0   0   2008/10/16,18:52:36.9918
CI PER   HHN --       14.84      0.2       120          0   0   2008/10/16,18:52:31.9926
CI PER   HHE --       14.84      0.2       120          0   0   2008/10/16,18:52:31.9926
```  
&nbsp;  
## CONFIGURATION FILE

The configuration files used by the gcda tools are makeada.cfg, makewda.cfg and gcda.cfg.

Format of the description below is 
```
<parameter name> <data type> <default value>
<description>
```

makeada.cfg and makewda.cfg are similar and are described below.

**All configuration parameters are required**

**KeyName** *string* &nbsp;&nbsp; WDA_KEY  
A unique key to identify the WDA (should map to the gcda.cfg file pointed to by the GCDA_FILE environmental variable).  

**GCDAConfigName** *string* &nbsp;&nbsp;RAD  
A unique string that looks up in the program/config_channel database tables the channels to include.  

**SecondsInMemory** *integer* nbsp;&nbsp;600  
The number of seconds of continuous waveform data to store in memory.

**Include** *string*   
External file to be included, with path prefix.  Typically used to include database connection parameters stored in an external file.
  
*The makeada module has one additional argument than the makewda, and that is the sample rate since this is a derived data area:*  

**SampleRate** *float*  
The frequency of continuous waveform data to store in memory.
  
**Database connection parameters**  


**DBService** *string*   
The name of the database being used. 

**DBUser** *string*  
The name of the database user account. 

**DBPasswd** *string*   
The password for the database user account specified by DBUser

**DBConnectionRetryInterval** *integer*   
Number of seconds to fall back when retrying db reconnects.

**DBMaxConnectionRetries** *integer*  
Number of times to retry db connections. 


*A sample makewda.cfg is shown below.*

```
# Configuration file to create a WDA
# Name of key for this WDA, from gcda.cfg
KeyName         WDA_KEY

# Name for this WDA from config_channel & program DB tables
GCDAConfigName  RAD

# Seconds of data for each channel in memory
SecondsInMemory 600

# Database parameters
Include db_info.d
```

where db_info.d contains:
```
DBService       somedb            
DBUser          someuser
DBPasswd        actualpasswordgoes here
DBConnectionRetryInterval 5
DBMaxConnectionRetries    5
```
&nbsp;  
*A sample makeada.cfg is shown below.*

```
# Configuration file to create an ADA
# Name of key for this ADA, from gcda.cfg
KeyName         ADA_KEY

# Name for this ADA from config_channel & program DB tables
GCDAConfigName  ADA

# Sample rate for the ADA (in seconds)
SampleRate      0.2

# Seconds of data for each channel in memory
SecondsInMemory 600

# Database parameters
Include db_info.d
```
&nbsp;  

The gcda.cfg contains a list of key-value pairs as shown in the sample below.

```
# gcda keys
WDA_AE_KEY 2000
WDA_FJ_KEY 3000
WDA_KO_KEY 4000
WDA_PS_KEY 5000
WDA_TZ_KEY 6000

XDA_KEY 20000
ADA_KEY  30000

EDA_KEY  1800
```  
&nbsp;  

## DEPENDENCIES

The database program and config_channel tables must be populated with some channels and the gcda.cfg file must have the key mapping pointed to by the KeyName argument.  
&nbsp;    


## MAINTENANCE

*  Use the scang gcda_utility to look at the data.
*  Use the linux utils ipcs and ipcrm to delete wda and ada areas once ALL programs reading and writing from them (rad2/ampgen/trimag/ew2wda) are stopped.  
&nbsp;  

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-gcda/-/issues  
&nbsp;  

## MORE INFORMATION

*  Authors : Phil Maechling, Paul Friberg, Pete Lombard 
*  The modules that use the wda and ada are rad2/trimag/ampgen and ew2wda.
