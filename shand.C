/** 
* @file
* @ingroup group_gcda
* @brief Signal handler for gcda
*/
/***********************************************************

File Name :
	shand.C

Programmer:
	Phil Maechling

Description:
	Signal handler.
	This routine should set a terminate flag (called no_interruptions).
	When it's true, processing continues. When signalled, this flag
	should be set. Then when the program sees the flag set, it will
	exit gracefully.

Creation Date:
	17 June 1996


Usage Notes:


Modification History:


**********************************************************/
#include <csignal>
#include <iostream>
#include "RetCodes.h"
#include "shand.h"

extern int no_signals_trapped;

void sig_handler(int sig)
{
  no_signals_trapped = TN_FALSE;
  std::cout << "Signal Number : " << sig << std::endl;
  std::cout << "In signal handler" << std::endl;
  
}
