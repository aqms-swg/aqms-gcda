#
# This is ew2wda's parameter file
#
MyModuleId         MOD_EW2WDA     # module id for this instance of ew2wda 
RingName           WAVE_RING      # get trace_buf2 messages here
LogFile            1              # 0 to completely turn off disk log file
HeartBeatInterval  30             # seconds between heartbeats
#Debug			          # Optional switch to turn on debugging output
#
# Specify one or more logos of messages to get from Earthworm ring.
# Message type is always TYPE_TRACEBUF2.
#
GetTraceFrom   INST_MENLO   MOD_WILDCARD    # TYPE_TRACEBUF2
#
ProgramName	EW2WDA        # Program key to config_channels table
WDA_KeyName	WDA_KEY	      # WDA shared memory key
#
DBService	service	      
DBUser		user
DBPasswd	passwd
