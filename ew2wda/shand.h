/***********************************************************

File Name :
	shand.h

Programmer:
	Phil Maechling

Description:
	Signal handler.
	This routine should set a terminate flag (called no_interruptions).
	When it's true, processing continues. When signalled, this flag
	should be set. Then when the program sees the flag set, it will
	exit gracefully.

Creation Date:
	17 June 1996


Usage Notes:


Modification History:


**********************************************************/


void sig_handler(int sig);
