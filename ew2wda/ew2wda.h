
/***********************************************************

File Name :
        ew2wda.h

Original Author:
        Will Kohler, USGS

Description:

Creation Date:
        March 29, 2004

Modification History:

Usage Notes:

**********************************************************/


#ifndef EW2WDA_H
#define EW2WDA_H

#define ERR_MISSMSG      0     // Message missed in transport ring
#define ERR_TOOBIG       1     // Retrieved msg too large for buffer
#define ERR_NOTRACK      2     // Msg retrieved; tracking limit exceeded

#define MAXLOGO 10
#define BUF_SIZE 60000         // Maximum size of a tracebuf message

#endif
