
/***********************************************************

File Name :
        sncl.h

Original Author:
        Pete Lombard, BSL

Description:
	Class for manipulating SNCLs, in particular as keys to 
	map containers. This is similar to the Channel class,
	but with only the SNCL, not the other paraphernalia.

Creation Date:
        4 June, 2002

Modification History:


Usage Notes:

**********************************************************/

#ifndef SNCL_H
#define SNCL_H

// Various include files
#include <iostream>
#include "nscl.h"


class SNCL : public nscl
{
 private:

 public:
    char station[MAX_CHARS_IN_STATION_STRING];
    char network[MAX_CHARS_IN_NETWORK_STRING];
    char channel[MAX_CHARS_IN_CHANNEL_STRING];
    char location[MAX_CHARS_IN_LOCATION_STRING];


    SNCL();
    SNCL(const SNCL &c);
    SNCL(const char *sta, const char *net, const char *chan, 
	 const char *loc );
    ~SNCL();

    SNCL& operator=(const SNCL &c);
    friend int operator<(const SNCL &c1, const SNCL &c2);
    friend int operator==(const SNCL &c1, const SNCL &c2);
    friend ostream& operator<<(ostream &os, const SNCL &c);
};

#endif
