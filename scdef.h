/** 
* @file
* @ingroup group_gcda
* @brief Station-Channel Definition. This is a structure used to instantiate a channel in the wda.
*/
/***********************************************************

File Name :
	scdef.h

Programmer:
	Phil Maechling

Description:
	Station-Channel Definition. This is a structure used to instantiate
	a channel in the wda.

Creation Date:
	16 October 1996


Usage Notes:



Modification History:



**********************************************************/
#ifndef scdef_H
#define scdef_H

#include "seismic.h"
#include "nscl.h"

struct station_channel_definition_type
{
  char network[MAX_CHARS_IN_NETWORK_STRING];
  char station[MAX_CHARS_IN_STATION_STRING];
  char channel[MAX_CHARS_IN_CHANNEL_STRING];
  char location[MAX_CHARS_IN_LOCATION_STRING];
};

#endif
